<?php
namespace Neklo\ProductPosition\Block\Adminhtml\Category;

class ProductPosition extends \Magento\Backend\Block\Template
{
    /**
     * @var \Neklo\ProductPosition\Helper\Product
     */
    protected $_helperProduct;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        \Neklo\ProductPosition\Helper\Product $helperProduct,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Template\Context $context,
        array $data = array()
    ) {
        $this->_helperProduct = $helperProduct;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->setTemplate('category/tab/product/position.phtml');
        parent::_construct();
    }

    /**
     * @return bool
     */
//    public function canShow(){
//        return $this->_helperProduct->isEnabled();
//    }

    /**
     * @return int
     */
    public function getColumnCount(){
        return $this->_helperProduct->getColumnCount();
    }

    /**
     * @return int
     */
    public function getRowCount(){
        return $this->_helperProduct->getRowCount();
    }

    /**
     * @return string
     */
    public function getCollectionJson()
    {
        return $this->_helperProduct->getCollectionJson();
    }

    /**
     * @return string
     */
    public function getSortedProductsPositionJson()
    {
        return $this->_helperProduct->getSortedProductsPositionJson();
    }

    /**
     * @return string
     */
    public function getAttachedProductsJson()
    {
        return $this->_helperProduct->getAttachedProductsJson();
    }

    /**
     * @return string
     */
    public function getCollectionSize()
    {
        return $this->_helperProduct->getCollection()->getSize();
    }

    /**
     * @return string
     */
    public function getNextPageUrl()
    {
        $params = array();
        if ($this->getCategory() && $this->getCategory()->getId()) {
            $params['id'] = $this->getCategory()->getId();
        }
        return $this->getUrl('neklo_productposition/ajax/page', $params);
    }

    /**
     * @return \Magento\Catalog\Model\Category
     */
    public function getCategory()
    {
        $category = $this->_coreRegistry->registry('current_category');
        return $category;
    }

}