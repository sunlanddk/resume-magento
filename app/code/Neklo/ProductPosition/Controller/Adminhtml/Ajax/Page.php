<?php

namespace Neklo\ProductPosition\Controller\Adminhtml\Ajax;

class Page extends \Magento\Backend\App\Action
{
    /**
     * @var \Neklo\ProductPosition\Helper\Product
     */
    protected $_helperProduct;

    protected $_resultJsonFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Neklo\ProductPosition\Helper\Product $helperProduct,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->_helperProduct = $helperProduct;
        $this->_resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $count = $this->getRequest()->getParam('count', 20);
        $collectionArray = $this->_helperProduct->getCollectionJson($page, $count, true);

        return  $this->_resultJsonFactory->create()->setData($collectionArray);
    }

    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Catalog::categories');
    }
}