<?php
namespace Neklo\ProductPosition\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
//    CONST GENERAL_ENABLE = 'neklo_productposition/general/enable';
    CONST COLUMN_COUNT = 'neklo_productposition/grid/column_count';
    CONST ROW_COUNT = 'neklo_productposition/grid/row_count';
    CONST HIDE_DISABLED = 'neklo_productposition/grid/hide_disabled';
    CONST HIDE_OOS = 'neklo_productposition/grid/hide_oos';

//    /**
//     * @param \Magento\Framework\App\Helper\Context $context
//     */
//    public function __construct(
//        \Magento\Framework\App\Helper\Context $context
//    ) {
//        parent::__construct($context);
//    }

//    public function isEnabled(){
//        return $this->scopeConfig->getValue(self::GENERAL_ENABLE);
//    }

    public function getColumnCount(){
        return $this->scopeConfig->getValue(self::COLUMN_COUNT);
    }

    public function getRowCount(){
        return $this->scopeConfig->getValue(self::ROW_COUNT);
    }

    public function hideDisabledProducts(){
        return $this->scopeConfig->getValue(self::HIDE_DISABLED);
    }

    public function hideOutOfStockProducts(){
        return $this->scopeConfig->getValue(self::HIDE_OOS);
    }

}