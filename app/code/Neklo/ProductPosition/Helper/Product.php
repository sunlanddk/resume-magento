<?php
namespace Neklo\ProductPosition\Helper;

class Product extends Data
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_pricingHelper;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $_stockFilter;

    /**
     * @var \Neklo\ProductPosition\Model\ResourceModel\Category
     */
    protected $_categoryFactory;

    /**
     * Product collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    const IMAGE_WIDTH = 100;
    const NO_SELECTION = 'no_selection';

    protected $_jsonMap = array(
        'entity_id',
        'name',
        'sku',
        'price',
        'position',
    );

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\CatalogInventory\Helper\Stock $stockFilter
     * @param \Neklo\ProductPosition\Model\ResourceModel\Category $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        \Neklo\ProductPosition\Model\ResourceModel\Category $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_pricingHelper = $pricingHelper;
        $this->_imageHelper = $imageHelper;
        $this->_stockFilter = $stockFilter;
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @return string
     */
    public function getAttachedProductsJson()
    {
        $products = $this->_categoryFactory->getAttachedProducts($this->getCategory());
        if (count($products) === 0) {
            return '{}';
        }
        return $this->_jsonEncoder->encode($products);
    }

    /**
     * @return string
     */
    public function getSortedProductsPositionJson()
    {
        $products = $this->_categoryFactory->getSortedProductsPosition($this->getCategory());
        if (count($products) === 0) {
            return '{}';
        }
        $productIdList = array_keys($products);
        $productPositionList = range(1, count($productIdList));
        $products = array_combine($productIdList, $productPositionList);
        return $this->_jsonEncoder->encode($products);
    }

    /**
     * @param int $page
     * @param int $count
     *
     * @return array
     */
    public function getCollectionJson($page = 1, $count = 20, $asArray = false)
    {
        $productDataList = array();
        $collection = $this->getCollection($page, $count);
        $startPosition = $count * $page - $count + 1;
        foreach ($collection as $product) {
            $productData = $product->toArray($this->_jsonMap);
            //unset($productData['stock_item']);
            $productData['image'] = $this->resizeImage($product);
            $productData['price'] = $this->_pricingHelper->currencyByStore($productData['price'], $this->getRequestStoreId(), true, false);
            $productData['position'] = $startPosition;
            $productData['attached'] = var_export(!!$product->getIsAttached(), true);
            $productDataList[] = $productData;
            $startPosition++;
        }
        if ($asArray)
            return $productDataList;
        else
            return $this->_jsonEncoder->encode($productDataList);
    }

    /**
     * @param int $page
     * @param int $count
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getCollection($page = 1, $count = 20)
    {
        /* @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->getProductCollection();
        $collection->setPageSize($count);
        if ($collection->getLastPageNumber() >= $page) {
            $collection->setCurPage($page);
            $collection->setOrder('position', 'ASC');
            $collection->setOrder('entity_id', 'DESC');
        } else {
            $collection->addFieldToFilter('entity_id', 0);
        }
        return $collection;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollection()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_productCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('thumbnail')
            ->joinField(
                "position",
                "catalog_category_product",
                "position",
                "product_id = entity_id",
                "category_id = {$this->getRequestCategoryId()}",
                "inner"
            )
            ->addAttributeToFilter(
                'visibility',
                array(
                    'in' => array(
                        \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG,
                        \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                    )
                )
            )
        ;

        if ($this->hideDisabledProducts()) {
            $collection->addAttributeToFilter(
                'status',
                \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
            );
        }
        if ($this->hideOutOfStockProducts()) {
            $this->_stockFilter->addInStockFilterToCollection($collection);
        }

        $collection->getSelect()->joinLeft(
            array('product_status' => \Neklo\ProductPosition\Model\ResourceModel\Product\Status::TABLE_NAME ),
            'e.entity_id = product_status.product_id AND product_status.category_id = ' . (int)$this->getRequestCategoryId(),
            array('is_attached' => 'IF(product_status.is_attached, product_status.is_attached, 0)')
        );
        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return string
     */
    public function resizeImage($product)
    {
        $imageHelper = $this->_imageHelper->init($product, 'product_thumbnail_image');
        $imageHelper
            ->constrainOnly(false)
            ->keepAspectRatio(false)
            ->keepFrame(false)
        ;
        $resizedImage = (string)$imageHelper->resize(self::IMAGE_WIDTH)->getUrl();

        return $resizedImage;
    }

    /**
     * @return int
     */
    public function getRequestCategoryId()
    {
        return (int)$this->_getRequest()->getParam('id', 0);
    }

    /**
     * @return int
     */
    public function getRequestStoreId()
    {
        return (int)$this->_getRequest()->getParam('store', 0);
    }

    /**
     * @return \Magento\Catalog\Model\Category
     */
    public function getCategory()
    {
        $category = $this->_coreRegistry->registry('current_category');
        return $category;
    }
}