<?php

namespace Neklo\ProductPosition\Model\Product;

use Magento\Framework\Model\AbstractModel;

class Status extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Neklo\ProductPosition\Model\ResourceModel\Product\Status');
    }
}