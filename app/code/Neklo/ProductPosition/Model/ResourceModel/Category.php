<?php

namespace Neklo\ProductPosition\Model\ResourceModel;

class Category extends \Magento\Catalog\Model\ResourceModel\Category
{
    /**
     * Get sorted positions of associated to category products
     *
     * @param \Magento\Catalog\Model\Category $category
     *
     * @return array
     */
    public function getSortedProductsPosition($category)
    {
        $select = $this->getConnection()->select()
            ->from($this->getCategoryProductTable(), array('product_id', 'position'))
            ->where('category_id = ' . (int)$category->getId())
            ->order(
                array(
                    'position ' . \Magento\Framework\DB\Select::SQL_ASC,
                    'product_id ' . \Magento\Framework\DB\Select::SQL_DESC,
                )
            )
        ;
//        var_dump($select->__toString());die();
        $sortedProductPosition = $this->getConnection()->fetchPairs($select);
        return $sortedProductPosition;
    }

    /**
     * Get attached products for category products
     *
     * @param \Magento\Catalog\Model\Category $category
     *
     * @return array
     */
    public function getAttachedProducts($category)
    {
        $select = $this->getConnection()->select()
            ->from(
                array('category_product' => $this->getCategoryProductTable()),
                array()
            )
            ->joinLeft(
                array('product_status' => \Neklo\ProductPosition\Model\ResourceModel\Product\Status::TABLE_NAME ),
                'category_product.product_id = product_status.product_id',
                array()
            )
            ->where('category_product.category_id = ' . (int)$category->getId())
            ->columns(
                array(
                    'product_id'  => 'category_product.product_id',
                    'is_attached' => 'IF(product_status.is_attached, product_status.is_attached, 0)',
                )
            )
        ;
        $attachedProductList = $this->getConnection()->fetchPairs($select);
        return $attachedProductList;
    }
}