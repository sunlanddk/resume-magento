<?php

namespace Neklo\ProductPosition\Model\ResourceModel\Product;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Status extends AbstractDb
{
    const TABLE_NAME = 'neklo_productposition_product_status';
    
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, 'entity_id');
    }

    public function updateProductPositions($categoryId, $attachedProductList, $categoryProductList)
    {
        $adapter = $this->getConnection();
        $adapter->delete($this->getMainTable(), array('category_id = ?' => $categoryId));

        $productIdListForInsert = array_diff_key($categoryProductList, $attachedProductList);
        $productIdListForDelete = array_diff_key($attachedProductList, $categoryProductList);
        $productIdListForUpdate = array_diff_key($attachedProductList, $productIdListForDelete);

        $insertData = $productIdListForUpdate;
        if (count($productIdListForInsert)) {
            foreach ($productIdListForInsert as $key => $value) {
                $insertData[$key] = 0;
            }
        }
        ksort($insertData);

        if (count($insertData)) {
            $data = array();
            foreach ($insertData as $productId => $isAttached) {
                $data[] = array(
                    'category_id' => (int)$categoryId,
                    'product_id'  => (int)$productId,
                    'is_attached' => (int)$isAttached,
                );
            }
            $adapter->insertMultiple($this->getMainTable(), $data);
        }
    }

}