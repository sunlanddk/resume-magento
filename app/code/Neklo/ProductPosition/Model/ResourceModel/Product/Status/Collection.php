<?php

namespace Neklo\ProductPosition\Model\ResourceModel\Product\Status;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            'Neklo\ProductPosition\Model\Product\Status',
            'Neklo\ProductPosition\Model\ResourceModel\Product\Status'
        );
    }
}