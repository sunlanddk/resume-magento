<?php

namespace Neklo\ProductPosition\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class CategoryPrepareSave implements ObserverInterface
{
    /**
     * Parser
     *
     * @var \Neklo\ProductPosition\Helper\Parser
     */
    protected $_parser;

    /**
     * @var \Neklo\ProductPosition\Model\ResourceModel\Product\Status
     */
    protected $_statusResource;

    private $_logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Neklo\ProductPosition\Model\ResourceModel\Product\Status $statusResource,
        \Neklo\ProductPosition\Helper\Parser $parser
    ) {
        $this->_logger = $logger;
        $this->_statusResource = $statusResource;
        $this->_parser = $parser;
    }

    public function execute(Observer $observer)
    {
        $category = $observer->getCategory();
        $postedProducts = $observer->getRequest()->getParam('category_products', false);
        if ($postedProducts === false)
            return $this;

        $postedProducts = $this->_parser->parseJsonStr($postedProducts);
        $attachedProducts = $this->_parser->parseJsonStr($observer->getRequest()->getParam('attached_category_products', false));

        try {
            $this->_statusResource->updateProductPositions($category->getId(), $attachedProducts, $postedProducts);
        } catch (\Throwable $t) {
            // Executed only in PHP 7, will not match in PHP 5.x
            $this->_logger->critical($t);
        } catch (\Exception $e) {
            // Executed only in PHP 5.x, will not be reached in PHP 7
            $this->_logger->critical($e);
        }

    }

}