<?php
namespace Neklo\ProductPosition\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Neklo\ProductPosition\Model\ResourceModel\Product\Status;

class ProductAfterSave implements ObserverInterface
{
    /**
     *
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;
    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $_msgs;

    /**
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param Status $statusResource
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Status $statusResource,
        \Magento\Framework\Message\ManagerInterface $msgs
    ) {
        $this->_logger = $logger;
        $this->_statusResource = $statusResource;
        $this->_msgs = $msgs;
    }

    /**
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /* @var $resource \Magento\Framework\App\ResourceConnection */
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_category_product');

        // SELECT DATA
        $sql = "SELECT * FROM " . $tableName;
        $result = $connection->fetchAll($sql);

        /* @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getProduct();
        $categories = $product->getCategoryIds();

        if (is_array($categories) && count($categories)) {
            try {
                foreach ($categories as $categoryId) {
                    $sql = $connection->select()
                        ->from($tableName)
                        ->where('category_id = ?', $categoryId)
                        ->order('position ASC')
                        ->order('product_id DESC');
                    $result = $connection->fetchAll($sql);
                    $data = [];
                    $key = 0;
                    foreach ($result as $pos) {
                        $pos['position'] = $key++;
                        $eid = $pos['entity_id'];
                        unset($pos['entity_id']);
                        $connection->update('catalog_category_product', $pos, 'entity_id='.$eid);
                    }

                }
            } catch (\Throwable $t) {
                // Executed only in PHP 7, will not match in PHP 5.x
                $this->_logger->critical($t);
                $this->_msgs->addErrorMessage($t->getMessage());
            } catch (\Exception $e) {
                // Executed only in PHP 5.x, will not be reached in PHP 7
                $this->_logger->critical($e);
                $this->_msgs->addErrorMessage($e->getMessage());
            }
        } else {
            return;
        }
    }
}
