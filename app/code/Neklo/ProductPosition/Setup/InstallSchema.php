<?php

namespace Neklo\ProductPosition\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable( \Neklo\ProductPosition\Model\ResourceModel\Product\Status::TABLE_NAME )
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [ 'identity' => true, 'nullable' => false, 'unsigned' => true , 'primary' => true, 'auto_increment' => true ],
            'ID'
        )->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [ 'nullable' => false, 'unsigned' => true ],
            'Category ID'
        )->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [ 'nullable' => false, 'unsigned' => true ],
            'Product ID'
        )->addColumn(
            'is_attached',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [ 'default' => 0, 'nullable' => false, 'unsigned' => true ],
            'Is Attached'
        )->setComment(
            'Neklo Product Position Table'
        );

        $installer->getConnection()->createTable( $table );

        $installer->endSetup();
    }
}