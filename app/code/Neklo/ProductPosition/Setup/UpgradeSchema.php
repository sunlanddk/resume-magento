<?php
namespace Neklo\ProductPosition\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Catalog\Model\ResourceModel\Product;
use Neklo\ProductPosition\Model\ResourceModel\Product\Status;

/**
 * Upgrade the Neklo_ProductPosition module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $this->addForeignKeys($setup);
        }

        $setup->endSetup();
    }


    /**
     *
     * @param SchemaSetupInterface $setup
     * @return void
     */
    protected function addForeignKeys(SchemaSetupInterface $setup)
    {
        /**
         * Add foreign keys again
         */
        //product key
        $setup->getConnection()->addForeignKey(
            $setup->getFkName(
                Status::TABLE_NAME,
                'product_id',
                'catalog_product_entity',
                'entity_id'
            ),
            $setup->getTable(Status::TABLE_NAME),
            'product_id',
            $setup->getTable('catalog_product_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        //product key
        $setup->getConnection()->addForeignKey(
            $setup->getFkName(
                Status::TABLE_NAME,
                'category_id',
                'catalog_category_entity',
                'entity_id'
            ),
            $setup->getTable(Status::TABLE_NAME),
            'category_id',
            $setup->getTable('catalog_category_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
    }

}
