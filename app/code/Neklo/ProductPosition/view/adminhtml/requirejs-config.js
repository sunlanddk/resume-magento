/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        'NEKLOvendor': 'Neklo_ProductPosition/js/vendor-min',
        'NEKLOmodule': 'Neklo_ProductPosition/js/module-min',
    }
};