<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace WhitePixel\Prismic\Block;
use \WhitePixel\Prismic\Helper\Data;

class Content extends \Magento\Framework\View\Element\Template{

  protected $storeManager;
  protected $page;
  private $content;

  public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Cms\Model\Page $page,
    \Magento\Catalog\Model\Product $product,
    Data $helper,
    array $data = []
  ) {
    $this->helper = $helper;
    $this->storeManager = $storeManager;
    $this->product = $product;
    $this->page = $page;
    parent::__construct($context, $data);
  }
  /**
   * Retrieve Pricmic content
   *
   * @return content
   */
  public function getPrismicContent($uid, $lang, $preview = false)
  {
// die();
    $this->content = $this->helper->getPrismicPage($uid, $lang, $preview);

    if($this->content->type == 'front_page'){
      return $this->getFrontPage();
    }
    if($this->content->type == 'collection_overview'){
      return $this->getCollectionFrontPage();
    }
    if($this->content->type == 'collection'){
      return $this->getCollectionPage($uid);
    }
    if($this->content->type == 'front_page_version_2'){
      return $this->getFrontPageNew($lang);
    }
    if($this->content->type == 'collection_page_layout'){
      return $this->getCollectionView($lang);
    }
    if($this->content->type == 'page'){
      return $this->getPageView($lang);
    }

    // foreach ($contentElements as $contentElement) {
    //   if($contentElement->slice_type)
    // }
    return "oops nothing found";
  }
  
  public function getStoreCode()
  {
    return $this->storeManager->getStore()->getCode();
  }
  public function getPageIdentifier()
  {
    return $this->page->getIdentifier();
  }
  private function getFrontPage(){
    // $html = '<div class="fixed-fix"></div>';
    $html = '';
    foreach ($this->content->data->body as $slice) {
      switch ($slice->slice_type) {
        case 'full_width_image_sticky':
          $html .= $this->getFullWidthImageStickyHtml($slice);
          break;
        case 'image_gallery_5':
          $html .= $this->getImageGallery5($slice);
          break;
        case 'image_gallery_video':
          $html .= $this->getImageGalleryVideo($slice);
          break;
        case 'sign_up_loyalty':
          $html .= $this->getSignUpLoyalty($slice);
          break;
        case 'newsletter':
          $html .= $this->getNewsletter($slice);
          break;
        case 'gallery_slider':
          $html .= $this->getGallerySlider($slice);
          break;
        case 'instagram':
          $html .= $this->getInstagram($slice);
          break;
      }
    }
    return $html;
  }

  private function getProductTitle($Id){
    $product = $this->product->load($Id);
    return $product->getName();
  }

  private function getProductLink($Id){
    if($Id == "A"){
      return "/";
    }
    $product = $this->product->load($Id);
    $url = $product->getProductUrl();
    $this->product->clearInstance();
    return $url;
    //return $product->getData()['url_key'];
  }

  
  private function getCollectionView($lang){
    $slices = $this->content->data;
    $html = '';

    

      $html .= '<section class="collectionPageLayout">';

      // Hero section
      $html .= $this->getBgHerosectionCol($slices->body[0]->primary, $slices->main_text_color);

      //Product section
      // $html .= $this->getProductCollectionSection($slices->body[1]->primary, $slices->body[2]->primary, $slices->body[3]->primary, $slices->main_text_color);
      $html .= $this->getProductCollectionSection($slices->body, $slices->body, $slices->body, $slices->main_text_color);

      $html .= '<div id="change"></div>';

      //Text section
      $html .= $this->getCollectionInfo($slices->body);

      //Full image fixed
      $html .= $this->getFixedImage($slices->body);

      //Bottom section
      // $html .= $this->getBottomSection($slices->body);

      $html .= '</section>';
    $html .= '</section>';
    

    $textColor = $slices->main_text_color;
    $color = $this->content->data->menu_color_primary;
    // $textColor = $this->content->data->menu_color_secondary;

    $colorsecond = $this->content->data->menu_color_secondary;
    // $textColorsecond = $this->content->data->menu_color_secondary;

    $html .= "<style>
    header .headerContent .menuContainer .headerContainer .block-search .block-title{color:".$color."; border-color:".$color."}
    .collectionPageLayout .product_collection .content_collection h4{color:".$color.";}
    body.page-layout-1column-prismic-new.changeColor header .headerContent .menuContainer .headerContainer .block-search .block-title{color:".$colorsecond."; border-color:".$colorsecond."}
    .changeColor header .header.content .mobileToggle{color:".$colorsecond."}header .header.content .mobileToggle{color:".$color."}
    .page-layout-1column-prismic-new main .headline h1{-webkit-text-stroke-color: ".$textColor.";} .page-layout-1column-prismic-new main .headline .headContainer .subtitleBox .subtitle h4{color: ".$colorsecond.";} .page-layout-1column-prismic-new main .hero .arrow-wrapper .arrow{border-color: ".$textColor.";} header:not(.showhead) .headerContent .menuContainer nav ul li{color: ".$color.";} header:not(.showhead) .headerContent .menuContainer nav ul li a:hover{border-bottom: solid 2px ".$color.";} header:not(.showhead) .headerContent .menuContainer nav ul li.active a{border-bottom: solid 2px ".$color.";} body:not(.changeColor) header:not(.showhead) .openCartLink {color: ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openCartLink:not(.mobileOpenCartLink):hover {border-bottom: solid 2px ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openLoginLink {color: ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openLoginLink:hover {border-bottom: solid 2px ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openSearcLink {color: ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openSearcLink:hover {border-bottom: solid 2px ".$color." !important;}body.page-layout-1column-prismic-new.changeColor header .headerContent .menuContainer nav ul li{color:".$colorsecond."}body.page-layout-1column-prismic-new.changeColor header .headerContent .menuContainer nav ul li:hover a{border-color:".$colorsecond."}body.page-layout-1column-prismic-new.changeColor header .openCartLink, body.page-layout-1column-prismic-new.changeColor header .instagramLink, body.page-layout-1column-prismic-new.changeColor header .storeSelecter{color:".$colorsecond."!important}body.page-layout-1column-prismic-new.changeColor header .openCartLink:hover, body.page-layout-1column-prismic-new.changeColor header .instagramLink:hover, body.page-layout-1column-prismic-new.changeColor header .storeSelecter:hover{border-color:".$colorsecond."!important}</style>";

    $html .= "<style>body.page-layout-1column-prismic-new main{margin-bottom:0}footer{display:none !important;}.collectionPageLayout footer{display:none}.collectionPageLayout .heroSection_collection,.collectionPageLayout .hero_collection,.collectionPageLayout .container_collection{z-index:2}.collectionPageLayout .parallaxEl{width:100%;height:auto;overflow:hidden;float:left; position:relative;z-index:999;}.collectionPageLayout .container_collection{display:table;height:100%}.collectionPageLayout .container_collection .heroSection_collection{height:auto;display:table-cell;vertical-align:middle;position:relative;z-index:0}.collectionPageLayout .container_collection .heroSection_collection .hero_collection{position:fixed;vertical-align:middle;width:100%}.collectionPageLayout .container_collection .heroSection_collection .hero_collection h1{font-size:98px;letter-spacing:7px; text-transform:uppercase;}.collectionPageLayout .container_collection .heroSection_collection .hero_collection .subtitle h4{letter-spacing:2px !important}.collectionPageLayout .space_collection{padding-bottom:24%}.collectionPageLayout .product_collection{position:relative}.collectionPageLayout .product_collection .content_collection{margin:0 auto;float:none;width:48%;height:auto;position:relative}.collectionPageLayout .product_collection .content_collection h4{transform:rotate(-90deg);transform-origin:left bottom 0;float:right;font-family:'Oswald', sans-serif;font-size:14px;font-weight:500;position:absolute;margin:0;right:-15px;left:calc(100% + 20px);bottom:0;width:100%;letter-spacing:1px;text-transform:uppercase}.collectionPageLayout .product_collection .content_collection img{position:relative;z-index:10}.collectionPageLayout div#change{width:100%;position:relative;height:auto;float:left}.collectionPageLayout .text-container{width:100%;height:auto;float:left;position:relative;z-index:10}.collectionPageLayout .text-container .text-container-inner{padding-left:8.333%;padding-right:8.333%;padding-bottom:calc(8.333% *4)}.collectionPageLayout .text-container .text-container-inner p{font-size:41px;line-height:61px;font-family:'Libre Baskerville', serif;color:#041fe2}.collectionPageLayout .full_collection{width:100%;position:relative;z-index:9;margin-bottom:250px}.collectionPageLayout .full_collection .content_collection_full{width:100%;margin:0 auto;height:auto;position:relative;text-align:center}.collectionPageLayout .full_collection .content_collection_full h1{font-size:100px;color:#041fe2;text-transform:uppercase;font-family:'Oswald', sans-serif;font-weight:400;line-height:120px;z-index:1}.collectionPageLayout .full_collection_parallax{height:0;width:100%;float:left;z-index:3}.collectionPageLayout .active{position:fixed;bottom:0;z-index:1}.collectionPageLayout .after-collection{width:100%;height:250px;background:#041fe2;float:left;z-index:-1;left:0;bottom:0;position:fixed;text-align:center;overflow:hidden}.collectionPageLayout .after-collection.active{z-index:1}.collectionPageLayout .after-collection h2{color:transparent;-webkit-text-stroke-width:2px;-webkit-text-stroke-color:#fbf9f7;font-size:98px;letter-spacing:7px;font-family:'Oswald', sans-serif;font-weight:700;width:100%;height:auto;margin:0 auto;line-height:250px;text-transform:uppercase;font-weight:700 !important;-moz-transition:0.5s color;-o-transition:0.5s color;-webkit-transition:0.5s color;transition:0.5s color}.collectionPageLayout .after-collection h2:hover{color:#fbf9f7}@media only screen and (max-width: 1199px){.collectionPageLayout .after-collection h2{font-size:67px;letter-spacing:3px}}@media only screen and (min-width: 768px) and (max-width: 1000px){.collectionPageLayout .container_collection{display:block}.collectionPageLayout .product_collection .content_collection{width:calc(100% - 200px)}.collectionPageLayout .product_collection .content_collection h4{margin:15px 0;right:0;bottom:0;position:relative;float:none;transform:none;left:0;font-size:16px}.collectionPageLayout .space_collection{padding-bottom:40%}.collectionPageLayout .full_collection{width:100%}.collectionPageLayout .full_collection img{width:100% !important}.collectionPageLayout .full_collection .active{left:0;width:calc(100% + 160px);margin-left:-80px}.collectionPageLayout .hero_collection{left:0;top:0}}@media only screen and (max-width: 767px){.page-layout-1column-prismic-new .collectionPageLayout{padding:0}.page-layout-1column-prismic-new .collectionPageLayout .container_collection .heroSection_collection .hero_collection h1{font-size:48px;letter-spacing:4px;-webkit-text-stroke-width:1px}.page-layout-1column-prismic-new .collectionPageLayout .after-collection h2{font-size:47px;letter-spacing:2px}.page-layout-1column-prismic-new .collectionPageLayout .container_collection{display:block;padding:0 0}.page-layout-1column-prismic-new .collectionPageLayout .hero_collection{left:0;top:0}.page-layout-1column-prismic-new .collectionPageLayout .space_collection{padding-bottom:40%}.page-layout-1column-prismic-new .collectionPageLayout .product{top:0}.page-layout-1column-prismic-new .collectionPageLayout .headline h1{font-size:48px;letter-spacing:4px;-webkit-text-stroke-width:1px}.page-layout-1column-prismic-new .collectionPageLayout .subtitle{padding:0}.page-layout-1column-prismic-new .collectionPageLayout .subtitle h4{font-size:10px !important}.page-layout-1column-prismic-new .collectionPageLayout .product_collection .content_collection{width:100%}.page-layout-1column-prismic-new .collectionPageLayout .product_collection .content_collection h4{margin:15px 0;right:0;bottom:0;position:relative;float:none;transform:none;left:0;font-size:12px}.page-layout-1column-prismic-new .collectionPageLayout .after-collection{overflow:hidden;height:200px;display:block}.page-layout-1column-prismic-new .collectionPageLayout .after-collection h2{font-size:28px;line-height:200px;letter-spacing:2;-webkit-text-stroke-width:1px}.page-layout-1column-prismic-new .collectionPageLayout .text-container .text-container-inner{padding-left:0;padding-right:0}.page-layout-1column-prismic-new .collectionPageLayout .text-container .text-container-inner p{font-size:19px;line-height:39px}.page-layout-1column-prismic-new .collectionPageLayout .product_inner .full_collection .parallax_pull{width:100%;left:0}.page-layout-1column-prismic-new .collectionPageLayout .full_collection{width:calc(100% + 60px);margin-bottom:200px}.page-layout-1column-prismic-new .collectionPageLayout .full_collection.active{left:0;width:100%;margin-left:0 !important}}

</style>";

    return $html;
  }  
  
  private function getPageView($lang){

  // return var_dump($this->content->data);
  $content = $this->content->data;
  $imageBackgroundDesktop = $content->background_image->url;
  $imageBackgroundMobil = $content->background_image->Mobile->url;

  $html = '<section class="prismic-page">';
  $html .= '<style>body.page-layout-1column-prismic-new .page-wrapper #maincontent .columns .column.main{padding:0;} .prismic-page{background: url('.$imageBackgroundDesktop.');}@media all and (max-width:768px) { .prismic-page{background: url('.$imageBackgroundMobil.');} } </style>';
    foreach ($content->body as $slice) {
      switch ($slice->slice_type) {
        case 'text_':
          $html .= '<section class="prismic-page-text">';
            foreach ($slice->primary->wysiwyg as $wysiwygBlock) {
              if($wysiwygBlock->type == "heading1"){
                $html .= "<h1 style='color:".$slice->primary->color_picker."' class='".$slice->primary->heading_scale."'>".$wysiwygBlock->text."</h1>";
              }elseif($wysiwygBlock->type == "heading2"){
                $html .= "<h2 class='".$slice->primary->heading_scale."'>".$wysiwygBlock->text."</h2>";
              }elseif($wysiwygBlock->type == "paragraph"){
                $string = $wysiwygBlock->text != "" ? $wysiwygBlock->text : "&nbsp;";
                $link = "";
                if(count($wysiwygBlock->spans) > 0){
                  if($wysiwygBlock->spans[0]->type == "hyperlink"){
                    $link = $wysiwygBlock->spans[0]->data->url;
                  }
                }
                if($link != ""){
                  $html .= '<p><a href="'.$link.'">'.$string.'</a></p>';  
                }else{
                  $html .= "<p>".$string."</p>";
                }
              }
            }
          $html .= '</section>'; 

          break;
        case 'image':
          $html .= '<section class="prismic-page-image">';
              $image = $slice->primary->image->url;
              $mobileImage = $slice->primary->image->Mobile->url;
              $html .= '<picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="" src="'.$image.'" alt=""></picture>';
          $html .= '</section>'; 
          break;
       
        case 'accordion':
          $html .= '<section class="prismic-page-accordion">';
            $html .= "<section class='accordion' id='accordion'><h2>".$slice->primary->title[0]->text."</h2></section>";
              $html .= "<div class='accordion-inner'>";
                foreach ($slice->primary->wysiwyg as $wysiwygBlock) {
                  if($wysiwygBlock->type == "heading1"){
                    $html .= "<h2>".$wysiwygBlock->text."</h2>";
                  }elseif($wysiwygBlock->type == "paragraph"){
                    $string = $wysiwygBlock->text != "" ? $wysiwygBlock->text : "&nbsp;";
                    // $html .= "<div class='accordion-inner'>";
                    $html .= "<p>".$string."</p>";
                    // $html .= "</div>";
                  }
                }
            $html .= "</div>";
          $html .= '</section>'; 
        break;
        
      }
    }
    // $image = $content->background_image->url;
    // $mobileImage = $content->background_image->MOBILE->url;
    // $html .= '<section class="backgroundImage"><picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-front" src="'.$image.'" alt=""></picture></section>';

    $html .= '</section>';
    return $html;
  }

  private function getFrontPageNew($lang){
    $html = '';

    // return var_dump($this->content->data);
    // $textColor = $slices->main_text_color;
    $color = $this->content->data->menu_color;
    $textColor = $this->content->data->text_color;

    $html .= "<style>
    body:not(.changeColor) header .headerContent .menuContainer .headerContainer .block-search .block-title{color:".$color.";}
    .page-layout-1column-prismic-new .vertical_text{color:".$color." !important}
    body:not(.changeColor) header .headerContent .menuContainer .headerContainer .block-search .block-title:hover {border-bottom: solid 2px ".$color." !important;}
    body:not(.changeColor) header .header.content .mobileToggle{color:".$color."}.page-layout-1column-prismic-new main .bigtitle h2{-webkit-text-stroke-color:".$textColor."}.page-layout-1column-prismic-new main .bigtitle h2:hover{color:".$textColor."}.page-layout-1column-prismic-new main .headContainer:hover h1{color:".$textColor.";}.page-layout-1column-prismic-new main .headline h1{-webkit-text-stroke-color: ".$textColor.";} .page-layout-1column-prismic-new main .headline .headContainer .subtitleBox .subtitle h4{color: ".$textColor.";} .page-layout-1column-prismic-new main .hero .arrow-wrapper .arrow{border-color: ".$textColor.";} header:not(.showhead) .headerContent .menuContainer nav ul li{color: ".$color.";} header:not(.showhead) .headerContent .menuContainer nav ul li a:hover{border-bottom: solid 2px ".$color.";} header:not(.showhead) .headerContent .menuContainer nav ul li.active a{border-bottom: solid 2px ".$color.";} body:not(.changeColor) header:not(.showhead) .openCartLink {color: ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openCartLink:not(.mobileOpenCartLink):hover {border-bottom: solid 2px ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openLoginLink {color: ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openLoginLink:hover {border-bottom: solid 2px ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openSearcLink {color: ".$color." !important;} body:not(.changeColor) header:not(.showhead) .openSearcLink:hover {border-bottom: solid 2px ".$color." !important;}</style>";



    // Hero section
    $html .= $this->getBgHerosection($this->content->data);

    $html .= '<section class="product">';
    //Product section
    $html .= $this->getProductSection($this->content->data->body1);

    //Newsletter section
    $html .= $this->getNewsletterSection($this->content->data->body3);

    $html .= '</section>';

    //Instagram section
    $html .= $this->getInstagramSection($this->content->data->body5, $lang);

    $html .= '</section>';


    return $html;

    // foreach ($this->content->data->body as $slice) {
    //   switch ($slice->slice_type) {
    //     case 'full_width_image_sticky':
    //       $html .= $this->getFullWidthImageStickyHtml($slice);
    //       break;
    //     case 'image_gallery_5':
    //       $html .= $this->getImageGallery5($slice);
    //       break;
    //     case 'image_gallery_video':
    //       $html .= $this->getImageGalleryVideo($slice);
    //       break;
    //     case 'sign_up_loyalty':
    //       $html .= $this->getSignUpLoyalty($slice);
    //       break;
    //     case 'newsletter':
    //       $html .= $this->getNewsletter($slice);
    //       break;
    //     case 'gallery_slider':
    //       $html .= $this->getGallerySlider($slice);
    //       break;
    //     case 'instagram':
    //       $html .= $this->getInstagram($slice);
    //       break;
    //   }
    // }
    // return $html;
  } 


  private function getBottomSection($slice){
    $html = '';
    foreach ($slice as $key => $value) {
      if($value->slice_type == 'full_width_container'){
        $html = '<div class="after-collection" style="background: '.$value->primary->container_background_color.' !important;">';
          $html .= '<div>';
            $html .= '<a href="'.$value->primary->collection_full_width_link[0]->text.'"><h2>'.$value->primary->collection_full_width_title[0]->text.'</h2></a>';
          $html .= '</div>';
        $html .= '</div>';

        $html .= '<style>.after-collection h2{color: transparent; -webkit-text-stroke-color: '.$value->primary->container_text_color.' !important;} .after-collection h2:hover{color: '.$value->primary->container_text_color.' !important;}</style>';   
      }
    }
    // $html = '<div class="after-collection" style="background: '.$slice->container_background_color.' !important;">';
    //   $html .= '<div>';
    //     $html .= '<a href="'.$slice->collection_full_width_link[0]->text.'"><h2>'.$slice->collection_full_width_title[0]->text.'</h2></a>';
    //   $html .= '</div>';
    // $html .= '</div>';

    // $html .= '<style>.after-collection h2{color: transparent; -webkit-text-stroke-color: '.$slice->container_text_color.' !important;} .after-collection h2:hover{color: '.$slice->container_text_color.' !important;}</style>';

    return $html;
  }

  private function getFixedImage($slice){
    $html = '';
    foreach ($slice as $key => $value) {
      if($value->slice_type == 'full_width_product'){
        $mobileImage = $value->primary->collection_full_width_image->Mobile->url;
        $image = $value->primary->collection_full_width_image->url;

        $html .= '<div class="full_collection_parallax"></div>';
        $html .= '<div class="product_inner full_collection parallax_pull">';
          $html .= '<a href="'.$this->getProductLink($value->primary->collection_full_width_link[0]->text).'" class="content_collection_full">';
            $html .= '<picture><source media="(max-width: 768px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="" src="'.$image.'" alt=""></picture>';
          $html .= '</a>  ';
        $html .= '</div>';
      }
    }
    // $mobileImage = $slice->collection_full_width_image->Mobile->url;
    // $image = $slice->collection_full_width_image->url;

    // $html .= '<div class="full_collection_parallax"></div>';
    // $html .= '<div class="product_inner full_collection parallax_pull">';
    //   $html .= '<div class="content_collection_full">';
    //     $html .= '<picture><source media="(max-width: 768px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="" src="'.$image.'" alt=""></picture>';
    //   $html .= '</div>  ';
    // $html .= '</div>';

    return $html;
  }
  
  private function getCollectionInfo($slice){
    $html = '';

    foreach ($slice as $key => $value) {
      if($value->slice_type == 'text'){
        $html .= '<div class="parallaxEl"></div>';
        $html .= '<div class="text-container durum">';
          $html .= '<div class="text-container-inner">';
            foreach ($value->primary->collection_text_field as $paragraph) {
              $string = $paragraph->text != "" ? $paragraph->text : "&nbsp;";
              $html .= '<p class="text-container-inner-p" style="color: '.$value->primary->text_color.' !important;">'.$string.'</p>';
            }
          $html .= '</div>';
        $html .= '</div>';        
      }
    }
    // foreach ($slice->primary->wysiwyg as $wysiwygBlock) {
    //           if($wysiwygBlock->type == "heading1"){
    //             $html .= "<h1 style='color:".$slice->primary->color_picker."' class='".$slice->primary->heading_scale."'>".$wysiwygBlock->text."</h1>";
    //           }elseif($wysiwygBlock->type == "heading2"){
    //             $html .= "<h2>".$wysiwygBlock->text."</h2>";
    //           }elseif($wysiwygBlock->type == "paragraph"){
    //             $string = $wysiwygBlock->text != "" ? $wysiwygBlock->text : "&nbsp;";
    //             $link = "";
    //             if(count($wysiwygBlock->spans) > 0){
    //               if($wysiwygBlock->spans[0]->type == "hyperlink"){
    //                 $link = $wysiwygBlock->spans[0]->data->url;
    //               }
    //             }
    //             if($link != ""){
    //               $html .= '<p><a href="'.$link.'">'.$string.'</a></p>';  
    //             }else{
    //               $html .= "<p>".$string."</p>";
    //             }
    //           }
    //         }
    // $html = '';

    // $html .= '<div class="parallaxEl"></div>';
    // $html .= '<div class="text-container durum">';
    //   $html .= '<div class="text-container-inner">';
    //     $html .= '<p class="text-container-inner-p" style="color: '.$slice->text_color.' !important;">'.$slice->collection_text_field[0]->text.'</p>';
    //   $html .= '</div>';
    // $html .= '</div>';

    return $html;
  }

  private function getProductCollectionSection($slice, $slice1, $slice2, $textcolor){

    $html = '<section class="product product_collection">';

    foreach ($slice as $key => $value) {
      if($value->slice_type == 'product'){
        $html .= '<div class="product_inner">';
          $html .= '<div class="space_collection"></div>';
          $html .= '<div class="content_collection">';
      //                           var_dump($value);
      // die();
            $html .= '<a href="'.$this->getProductLink($value->primary->link[0]->text).'"><img src="'.$value->primary->collection_product_image->url.'" alt=""></a>';
            $html .= '<h4 class="vertical_text" style="color: '.$textcolor.' !important">'.$this->getProductTitle($value->primary->collection_product_title[0]->text).'</h4>';
          $html .= '</div>';
        $html .= '</div>';
      }
    }
    // $html = '<section class="product product_collection">';
    //   $html .= '<div class="product_inner">';
    //     $html .= '<div class="space_collection"></div>';
    //     $html .= '<div class="content_collection">';
    //       $html .= '<a href="'.$this->getProductLink($slice->link[0]->text).'"><img src="'.$slice->collection_product_image->url.'" alt=""></a>';
    //       $html .= '<h4 class="vertical_text" style="color: '.$textcolor.' !important">'.$this->getProductTitle($slice->collection_product_title[0]->text).'</h4>';
    //     $html .= '</div>';
    //   $html .= '</div>';

    //   $html .= '<div class="product_inner">';
    //   $html .= '<div class="space_collection"></div>';
    //     $html .= '<div class="content_collection">';
    //       $html .= '<a href="'.$this->getProductLink($slice1->link[0]->text).'"><img src="'.$slice1->collection_product_image->url.'" alt=""></a>';
    //       $html .= '<h4 class="vertical_text" style="color: '.$textcolor.' !important">'.$this->getProductTitle($slice1->collection_product_title[0]->text).'</h4>';
    //     $html .= '</div>';
    //   $html .= '</div>';

    //   $html .= '<div class="product_inner">';
    //   $html .= '<div class="space_collection"></div>';
    //     $html .= '<div class="content_collection">';
    //       $html .= '<a href="'.$this->getProductLink($slice2->link[0]->text).'"><img src="'.$slice2->collection_product_image->url.'" alt=""></a>';
    //       $html .= '<h4 class="vertical_text" style="color: '.$textcolor.' !important">'.$this->getProductTitle($slice2->collection_product_title[0]->text).'</h4>';
    //     $html .= '</div>';
    //   $html .= '</div>';

    $html .= '</section>';

    return $html;
  }

  private function getBgHerosectionCol($slice, $textcolor){
    $image = $slice->image->url;
    $mobileImage = $slice->image->Mobile->url;
    $title = $slice->collection_page_title[0]->text;
    $subtitle = $slice->collection_page_subtitle[0]->text;
    $subtitle_link = $slice->collection_page_title_link[0]->text;

    // $html = '<section class="backgroundImage" >
    //           <picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-front lazy" src="'.$image.'" alt=""></picture>
    //         </section>
    //         <section class="background" style="background: '.$slice->background_color.';"></section>';

    $html = '<section class="backgroundImage" >
              <picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-front lazy" src="'.$image.'" alt=""></picture>
            </section>
            <section class="background" style="background: '.$slice->background_color.';"></section>';
    $html = '<section class="backgroundImage" >
              <div class="imgCover"></div>
              <style>
                .backgroundImage .imgCover{
                  background: url("'.$image.'") no-repeat center bottom;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;
                  background-size: cover;
                  position: absolute;
                  left: 0;
                  top: 0;
                  width: 100%;
                  height: 100%;
                }
                @media all and (max-width: 767px){
                  .backgroundImage .imgCover{
                    background: url("'.$mobileImage.'") no-repeat center bottom;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                  }
                }
              </style>
            </section>
            <section class="background" style="background: '.$slice->background_color.';"></section>';

    $html .= '<section class="container container_collection">';

    $html .= '<section class="heroSection heroSection_collection">
                <div class="hero hero_collection">
                    <div class="text">
                      <div class="headline">
                        <div class="headContainer">
                            <h1>'.$title.'</h1>
                          <div class="subtitleBox">
                            <div class="subtitle">
                                <h4>'.$subtitle.'</h4>
                            </div>
                          </div>
                        </div>
                      </div>   
                      <div class="arrow-wrapper"><div class="arrow"></div></div> 
                    </div>
                </div>
              </section>';
    $html .= '<style>.backgroundImage{height: 100vh !important;}.content_collection h4{color: '.$textcolor.' !important;}.headContainer h4{color:'.$textcolor.' !important;}.headContainer h1{color: transparent; -webkit-text-stroke-color: '.$textcolor.' !important;}.headContainer:hover h1{color: '.$textcolor.' !important;}</style>';
    // $html .= '<div class="parallaxEl"></div>';
    return $html;
  }

  
  private function getInstagramSection($slices, $lang){
    $html = '<section class="instagram">';
      $html .= '<div class="space half"></div>';
      $html .= '<div class="ig">';
        $html .= '<div class="sticky-wrapper">';
          $html .= '<div class="title-wrapper">';
            $html .= '<div class="title">';
              if($lang == 'da-dk'){
                $html .= '<a target="_blank" href="https://www.instagram.com/resumecph/"><h2>Følg os på Instagram</h2></a>';
              }
              else{
                $html .= '<a target="_blank" href="https://www.instagram.com/resumecph/"><h2>Follow us on Instagram</h2></a>'; 
              }
            $html .= '</div>';
            $html .= '<div class="follow">';
              $html .= '<h2><a target="_blank" href="https://www.instagram.com/resumecph/">@Resumecph</a></h2>';
            $html .= '</div>';
          $html .= '</div>';
          $html .= '<div class="space half"></div>';
          $count = 1;
          foreach ($slices as $slice) {
            $html .= '<div class="image-row">';
              $html .= '<div class="space"></div>'; 
                if($count%2 === 0){
                  $html .= '<div class="image-container two">';
                }
                else{
                  $html .= '<div class="image-container one">';
                }
                $html .= '<img src="'.$slice->primary->instagram_image->url.'" alt="">';
              $html .= '</div>';
            $html .= '</div>';
            $count ++;
          }
          $html .= '<div class="spaceBottom"></div>';  
        $html .= '</div>';
      $html .= '</div>';
      $html .= '<div class="spaceBottom"></div>';  
    $html .= '</section>';

    return $html;
  }

  private function getNewsletterSection($slices){
    $html = '<section class="newsletter">';
      $html .= '<div class="space"></div>';
      $html .= '<div class="ig">';
        $html .= '<div class="sticky-wrapper">';
          $html .= '<div class="content8 title-wrapper">';
            $html .= '<div class="content-box title">';
              $html .= "<p>Wanna get all the news before everyone else? Sign up for our newsletter here</p>";
              $html .= '<form action="https://resume.us4.list-manage.com/subscribe/" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-container" target="_blank" novalidate>';
                  $html .= '<div class="mc-field-group">';
                    $html .= '<input type="email" value="" name="EMAIL" placeholder="Email" class="required email form-input" id="mce-EMAIL">';
                    $html .= '<input type="hidden" value="46a8bd460b72c372723cc5ff0" name="u">';
                    $html .= '<input type="hidden" value="2245739783" name="id">';
                  $html .= '</div>';
                  $html .= '<div id="mce-responses" class="clear">';
                  $html .= '</div><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->';
                  $html .= '<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_46a8bd460b72c372723cc5ff0_2245739783" tabindex="-1" value=""></div>';
                  $html .= '<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">';
              $html .= '</form>';
            $html .= '</div>';
          $html .= '</div>';
          $count = 1;
          foreach ($slices as $slice) {
            $html .= '<div class="image-row">';
              if($count > 1){
                $html .= '<div class="space"></div>'; 
              }
              $html .= '<div class="image-container one">';
                $html .= '<img src="'.$slice->primary->newsletter_images->url.'" alt="">';
              $html .= '</div>';
            $html .= '</div>';
            $count ++;
          }
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</section>';

    return $html;
  }

  private function getProductSection($slices){
    // $image = $slice->image->url;
    // $mobileImage = $slice->image->Mobile->url;
    // $title = $slice->title[0]->text;
    // $subtitle = $slice->subtitle[0]->text;
    // $subtitle_link = $slice->link->url;
    $html = '';

    $count = 1;
    foreach ($slices as $slice) {
        if($count === 1){
          $numb = '';
        }
        else{
          $numb = $count;
        }
        switch ($slice->slice_type) {
          case 'product_container':
            $html .= '<div class="product_inner">';
              $html .= '<div class="space"></div>';
              $html .= '<div class="content'.$numb.'">';
                  if($slice->primary->product_container_productid != ''){
                    $html .= '<a href="'.$this->getProductLink($slice->primary->product_container_productid).'"><img src="'.$slice->primary->product_container_image->url.'" alt=""></a>';
                  }
                  else{
                    $html .= '<img src="'.$slice->primary->product_container_image->url.'" alt="">'; 
                  }
                  $html .= '<h4 class="vertical_text">'.$this->getProductTitle($slice->primary->product_container_productid).'</h4>';
              $html .= '</div>';
              $count ++;
            $html .= '</div>';
            break;
          case 'page_container':
            $html .= '<div class="bigtitle">';
              $html .= '<div class="spaceBig"></div>';
              $html .= '<h2>'.$slice->primary->page_container_pagetitle[0]->text.'</h2>';
              $html .= '<div class="spaceBig"></div>';
            $html .= '</div>';
            break;
          case 'video_product_container':
            $html .= '<div class="product_inner">';
              // $html .= '<div class="spaceBig"></div>';
              $html .= '<div class="content'.$numb.'">';
                $html .= '<a href="'.$this->getProductLink($slice->primary->video_product_container_productid).'">';
                  if(isset($slice->primary->video_product_background_link->url)):
                  $html .= '<video class="video" muted="muted" loop="loop" autoplay="autoplay" playsinline="playsinline">';
                  $html .= '<source src="'.$slice->primary->video_product_background_link->url.'" type="video/mp4">';
                  $html .= 'Your browser does not support HTML5 video.';
                  $html .= '</video>';
                  $html .= '<img src="'.$slice->primary->video_product_container_image->url.'" alt="">';
                  else:
                  $html .= '<img style="width: 100%;position: relative;z-index: 10;margin: 0;padding: 0;vertical-align: top;overflow: hidden;float: left; max-width: 100%;left: 0;top: 0;" src="'.$slice->primary->video_product_container_image->url.'" alt="">';
                  $html .= '<h4 class="vertical_text">'.$this->getProductTitle($slice->primary->video_product_container_productid).'</h4>';
                  endif;
                  //$html .= '<canvas id="canvas" class="canvas"></canvas>';                    
                  //$html .= '<h4 class="vertical_text">'.$this->getProductTitle($slice->primary->video_product_container_productid).'</h4>';
                $html .= '</a>';
              $html .= '</div>';
            $html .= '</div>';
            $count ++;
            break;
          case 'category_container':
            $html .= '<div class="product_inner">';
              $html .= '<div class="space"></div>';
              $html .= '<div class="content'.$numb.'">';
              $html .= '</div>';
            $html .= '</div>';
            $count ++;
            break;
          case 'custom_link_container':
            $html .= '<div class="product_inner">';
              $html .= '<div class="space"></div>';
              $html .= '<div class="content'.$numb.'">';
              $html .= '</div>';
            $html .= '</div>';
            $count ++;
            break;
        }
    }

    return $html;

    $html = '<section class="backgroundImage" >
              <picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-front lazy" src="'.$image.'" alt=""></picture>
            </section>
            <section class="background" >
            </section>';
    $html .= '<section class="containerSlideFix"></div>';        
    $html .= '<section class="container">';

    $html .= '<section class="heroSection">
                <div class="hero">
                  <a href="'.$subtitle_link.'">
                    <div class="text">
                      <div class="headline">
                        <h1>'.$title.'</h1>
                      </div>    
                    </div>
                    <div class="subtitleBox">
                      <div class="subtitle">
                        <h4>'.$subtitle.'</h4>
                      </div>
                    </div>
                  </a>
                </div>
              </section>';
    return $html;
  }

  private function getBgHerosection($slice){
    $image = $slice->image->url;
    $mobileImage = $slice->image->Mobile->url;
    $title = $slice->title[0]->text;
    $subtitle = $slice->subtitle[0]->text;
    $subtitle_link = $slice->link1[0]->text;

    // $html = '<section class="backgroundImage" >
    //           <picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-front lazy" src="'.$image.'" alt=""></picture>
    //         </section>
    //         <section class="background" style="background: #fbf9f7;"></section>';
    $html = '<section class="backgroundImage" >
              <picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-front lazy" src="'.$image.'" alt=""></picture>
            </section>
            <section class="background" style="background: #fbf9f7;"></section>';
    $html = '<section class="backgroundImage" >
              <div class="imgCover"></div>
              <style>
                .backgroundImage .imgCover{
                  background: url("'.$image.'") no-repeat center bottom;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;
                  background-size: cover;
                  position: absolute;
                  left: 0;
                  top: 0;
                  width: 100%;
                  height: 100%;
                }
                @media all and (max-width: 767px){
                  .backgroundImage .imgCover{
                    background: url("'.$mobileImage.'") no-repeat center bottom;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                  }
                }
              </style>
            </section>
            <section class="background" style="background: #fbf9f7;"></section>';
    $html .= '<section class="containerSlideFix"></section>';        
    $html .= '<section class="container">';

    $html .= '<section class="heroSection">
                <div class="hero">
                  <div class="text">
                    <div class="headline">
                      <div class="headContainer">
                        <a href="'.$subtitle_link.'">
                          <h1>'.$title.'</h1>
                        </a>
                        <div class="subtitleBox">
                          <div class="subtitle">
                            <a href="'.$subtitle_link.'">
                              <h4>'.$subtitle.'</h4>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>    
                  </div>
                  <div class="arrow-wrapper"><div class="arrow"></div></div>
                </div>
              </section>
              <style>.backgroundImage{height: 100vh !important;}</style>';

    return $html;
  }

  private function getFullWidthImageStickyHtml($slice){
    $image = $slice->primary->image->url;
    $mobileImage = $slice->primary->mobile_image->url;
    $title = $slice->primary->title[0]->text;
    $subtitle = $slice->primary->subtitle[0]->text;
    $subtitle_link = $slice->primary->subtitle_link[0]->text;
    $html = '<div class="full-width-image-sticky wrapper">';
    $html .= '<div class="title-wrapper">';
    $html .= '<div class="title"><h1>'.$title.'</h1></div>';
    // $html .= '<div class="title"><h1 style="color:#041fe2">'.$title.'</h1></div>';
    $html .= '<div class="subtitle"><a href="'.$subtitle_link.'"><h2>'.$subtitle.'</h2></a></div>';
    // $html .= '<div class="subtitle" style="color:#041fe2"><a style="color:#041fe2" href="'.$subtitle_link.'"><h2 style="color:#041fe2">'.$subtitle.'</h2></a></div>';
    $html .= '</div>';
    $html .= '<div class="imageContainer"><picture><source media="(max-width: 767px)" srcset="'.$mobileImage.' 1x, '.$mobileImage.' 2x, '.$mobileImage.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="cover-full-width lazy" src="'.$image.'" alt=""></picture></div>';
    $html .= '</div>';
    return $html;
  }
  private function getImageGallery5($slice){

    $image1 = $slice->primary->image_1->url;
    $linkTop1 = $slice->primary->image_1_top_clothing_link[0]->text;
    $linkBottom1 = $slice->primary->image_1_bottom_clothing_link[0]->text;
    $nameTop1 = $slice->primary->image_1_top_clothing_name[0]->text;
    $nameBottom1 = $slice->primary->image_1_bottom_clothing_name[0]->text;
    $image2 = $slice->primary->image_2->url;
    $linkTop2 = $slice->primary->image_2_top_clothing_link[0]->text;
    $linkBottom2 = $slice->primary->image_2_bottom_clothing_link[0]->text;
    $nameTop2 = $slice->primary->image_2_top_clothing_name[0]->text;
    $nameBottom2 = $slice->primary->image_2_bottom_clothing_name[0]->text;
    $image3 = $slice->primary->image_3->url;
    $image4 = $slice->primary->image_4->url;
    $linkTop4 = $slice->primary->image_4_top_clothing_link[0]->text;
    $linkBottom4 = $slice->primary->image_4_bottom_clothing_link[0]->text;
    $nameTop4 = $slice->primary->image_4_top_clothing_name[0]->text;
    $nameBottom4 = $slice->primary->image_4_bottom_clothing_name[0]->text;
    $image5 = $slice->primary->image_5->url;
    $linkTop5 = $slice->primary->image_5_top_clothing_link[0]->text;
    $linkBottom5 = $slice->primary->image_5_bottom_clothing_link[0]->text;
    $nameTop5 = $slice->primary->image_5_top_clothing_name[0]->text;
    $nameBottom5 = $slice->primary->image_5_bottom_clothing_name[0]->text;
    $html = '<div class="image-gallery-5 wrapper">';
      $html .= '<div class="image-row right">';
        $html .= '<div class="image-container one">';
          $full = "";
          if($nameBottom1 == ""){
            $full = "full";
          }
          $html .= '<a href="'.$linkTop1.'" class="image-text-top '.$full.'">';
          $html .= '<h2>'.$nameTop1.'</h2>';
          $html .= '</a>';
          if($nameBottom1 != ""){
            $html .= '<a href="'.$linkBottom1.'" class="image-text-bottom">';
            $html .= '<h2>'.$nameBottom1.'</h2>';
            $html .= '</a>';
          }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img class="move" src="'.$image1.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
      $html .= '<div class="image-row">';
        $html .= '<div class="image-container two">';
          $full = "";
          if($nameBottom2 == ""){
            $full = "full";
          }
          $html .= '<a href="'.$linkTop2.'" class="image-text-top '.$full.'">';
          $html .= '<h2>'.$nameTop2.'</h2>';
          $html .= '</a>';
          if($nameBottom2 != ""){
            $html .= '<a href="'.$linkBottom2.'" class="image-text-bottom">';
            $html .= '<h2>'.$nameBottom2.'</h2>';
            $html .= '</a>';
          }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><source srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><img class="move2" src="'.$image2.'" alt=""></picture>';
        $html .= '</div>';
        $html .= '<div class="image-container three">';
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image3.' 1x, '.$image3.' 2x, '.$image3.' 3x"><source srcset="'.$image3.' 1x, '.$image3.' 2x, '.$image3.' 3x"><img class="move4" src="'.$image3.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
      $html .= '<div class="image-row right">';
        $html .= '<div class="image-container four">';
          $full = "";
          if($nameBottom4 == ""){
            $full = "full";
          }
          $html .= '<a href="'.$linkTop4.'" class="image-text-top '.$full.'">';
          $html .= '<h2>'.$nameTop4.'</h2>';
          $html .= '</a>';
          if($nameBottom4 != ""){
            $html .= '<a href="'.$linkBottom4.'" class="image-text-bottom">';
            $html .= '<h2>'.$nameBottom4.'</h2>';
            $html .= '</a>';
          }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image4.' 1x, '.$image4.' 2x, '.$image4.' 3x"><source srcset="'.$image4.' 1x, '.$image4.' 2x, '.$image4.' 3x"><img class="move2" src="'.$image4.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
      $html .= '<div class="image-row last">';
        $html .= '<div class="image-container five">';
          $full = "";
          if($nameBottom5 == ""){
            $full = "full";
          }
          $html .= '<a href="'.$linkTop5.'" class="image-text-top '.$full.'">';
          $html .= '<h2>'.$nameTop5.'</h2>';
          $html .= '</a>';
          if($nameBottom5 != ""){
            $html .= '<a href="'.$linkBottom5.'" class="image-text-bottom">';
            $html .= '<h2>'.$nameBottom5.'</h2>';
            $html .= '</a>';
          }    
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image5.' 1x, '.$image5.' 2x, '.$image5.' 3x"><source srcset="'.$image5.' 1x, '.$image5.' 2x, '.$image5.' 3x"><img class="move3" src="'.$image5.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';
    return $html;

    // return var_dump($slice);
  }
  private function getImageGalleryVideo($slice){
    $video = $slice->primary->video->url;
    $image1 = $slice->primary->image_1->url;
    $imageText1 = $slice->primary->image_1_clothing_name[0]->text;
    $imageLink1 = $slice->primary->image_1_clothing_link[0]->text;
    // $image2 = $slice->primary->image_2->url;
    // $image3 = $slice->primary->image_3->url;
    $html = '<div class="image-gallery-video">';
    $html .= '<div class="image-column">';
    $html .= '<div class="image-container">';
    $html .= '<div class="image-container-inner">';
    $full = "full";
    $html .= '<a href="'.$imageLink1.'" class="image-text-top '.$full.'">';
    $html .= '<h2>'.$imageText1.'</h2>';
    $html .= '</a>';
    $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img class="lazy" src="'.$image1.'" alt=""></picture>';
    // $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><source srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><img class="lazy" src="'.$image2.'" alt=""></picture>';
    // $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image3.' 1x, '.$image3.' 2x, '.$image3.' 3x"><source srcset="'.$image3.' 1x, '.$image3.' 2x, '.$image3.' 3x"><img class="lazy" src="'.$image3.'" alt=""></picture>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<video class="video" muted="muted" loop="loop" autoplay="autoplay" playsinline="playsinline">';
    $html .= '<source src="'.$video.'" type="video/mp4">';
    $html .= 'Your browser does not support HTML5 video.';
    $html .= '</video>';
    $html .= '<canvas id="canvas" class="canvas"></canvas>';
    $html .= '</div>';
    return $html;
    // return var_dump($slice);
  }
  private function getSignUpLoyalty($slice){

    // return var_dump($slice);
  }
  private function getNewsletter($slice){
    $image1 = $slice->primary->image_1->url;
    $image2 = $slice->primary->image_2->url;
    $image3 = $slice->primary->image_3->url;
    $html = '<div class="newsletter">';
      $html .= '<div class="left">';
        $html .= '<div class="title-wrapper">';
          $html .= '<div class="subtitle"><h3>Signup for Our</h3></div>';
          $html .= '<div class="title"><h2>Newsletter</h2></div>';
          $html .= '<div class="form-wrapper">';
            $html .= '<form action="https://resume.us4.list-manage.com/subscribe/" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>';
              $html .= '<div class="mc-field-group">';
              $html .= '<input type="email" value="" name="EMAIL" placeholder="Email" class="required email" id="mce-EMAIL">';
              $html .= '<input type="hidden" value="46a8bd460b72c372723cc5ff0" name="u">';
              $html .= '<input type="hidden" value="2245739783" name="id">';
              $html .= '</div>';
              $html .= '<div id="mce-responses" class="clear">';
              $html .= '</div><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->';
                $html .= '<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_46a8bd460b72c372723cc5ff0_2245739783" tabindex="-1" value=""></div>';
                $html .= '<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">';
            $html .= '</form>';
          $html .= '</div>';
        $html .= '</div>';
      $html .= '</div>';
      $html .= '<div class="right">';
        $html .= '<div class="image-wrapper">';
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img width="1200" height="1800" class="cover-newsletter" src="'.$image1.'" alt=""></picture>';
        $html .= '</div>';
        $html .= '<div class="image-wrapper">';
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><source srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><img width="1200" height="1800" class="cover-newsletter" src="'.$image2.'" alt=""></picture>';
        $html .= '</div>';
        $html .= '<div class="image-wrapper">';
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image3.' 1x, '.$image3.' 2x, '.$image3.' 3x"><source srcset="'.$image3.' 1x, '.$image3.' 2x, '.$image3.' 3x"><img width="1200" height="1800" class="cover-newsletter" src="'.$image3.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';
    
    return $html;
  }
  private function getGallerySlider($slice){

    // return var_dump($slice);
  }
  private function getInstagram($slice){

    $title = $slice->primary->title[0]->text;
    $url = $slice->primary->link_instagram_account->url;
    $instagramLogo = $slice->primary->image_instagram_name->url;
    $instagramLogoHover = $slice->primary->image_instagram_name_hover->url;
    $image1 = $slice->primary->image_1->url;
    $image2 = $slice->primary->image_2->url;
    $html = '<div class="instagram">';
      $html .= '<div class="sticky-wrapper">';
        $html .= '<div class="title-wrapper">';
          $html .= '<div class="title"><h2>'.$title.'</h2></div>';
            $html .= '<a href = '.$url.' class="instagram-logo-wrapper" target="_blank">';
              $html .= '<picture><source media="(max-width: 767px)" srcset="'.$instagramLogo.' 1x, '.$instagramLogo.' 2x, '.$instagramLogo.' 3x"><source srcset="'.$instagramLogo.' 1x, '.$instagramLogo.' 2x, '.$instagramLogo.' 3x"><img class="instagram-logo" src="'.$instagramLogo.'" alt=""></picture>';
              $html .= '<picture><source media="(max-width: 767px)" srcset="'.$instagramLogoHover.' 1x, '.$instagramLogoHover.' 2x, '.$instagramLogoHover.' 3x"><source srcset="'.$instagramLogoHover.' 1x, '.$instagramLogoHover.' 2x, '.$instagramLogoHover.' 3x"><img class="instagram-logo-hover" src="'.$instagramLogoHover.'" alt=""></picture>';
        $html .= '</a>';
        $html .= '</div>';
        $html .= '<div class="image-row">';
          $html .= '<div class="image-container one">';
            $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img class="" src="'.$image1.'" alt=""></picture>';
          $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="image-row">';
          $html .= '<div class="image-container two">';
            $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><source srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><img class="" src="'.$image2.'" alt=""></picture>';
          $html .= '</div>';
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';
    
    return $html;
  }
  private function getCollectionFrontPage(){
    $html = '<div class="fixed-fix"></div>';
    // foreach ($this->content->data->body as $slice) {
    //   switch ($slice->slice_type) {
    //     case 'full_width_image_sticky':
    //       $html .= $this->getFullWidthImageStickyHtml($slice);
    //       break;
    //     case 'image_gallery_5':
    //       $html .= $this->getImageGallery5($slice);
    //       break;
    //     case 'image_gallery_video':
    //       $html .= $this->getImageGalleryVideo($slice);
    //       break;
    //     case 'sign_up_loyalty':
    //       $html .= $this->getSignUpLoyalty($slice);
    //       break;
    //     case 'newsletter':
    //       $html .= $this->getNewsletter($slice);
    //       break;
    //     case 'gallery_slider':
    //       $html .= $this->getGallerySlider($slice);
    //       break;
    //     case 'instagram':
    //       $html .= $this->getInstagram($slice);
    //       break;
    //   }
    // }
    return $html;
  }
  private function getCollectionPage($uid){
    $html = '';
    // var_dump($this->content->data->body);
    foreach ($this->content->data->body as $slice) {
      switch ($slice->slice_type) {
        case 'collection_intro':
          $html .= $this->getCollectionIntro($slice);
          break;
        case 'single_image':
          $html .= $this->getCollectionSingleImage($slice);
          break;
        case 'full_width_image':
          $html .= $this->getCollectionFullWidthImage($slice);
          break;
        case 'full_with_2_images':
          $html .= $this->getCollectionFullWidthImage2($slice);
          break;
      }
    }
    if($uid = 'about'){
      $html .= '<style>header .mobileToggle{background: url(https://www.resumecph.dk/pub/static/version1568729530/frontend/WhitePixel/resume/da_DK/images/menu_h.png) no-repeat; background-size: 100% auto;}header .openCartLink, header .instagramLink, header .storeSelecter{color: white;}</style>';
    }
    return $html;
  }
  private function getCollectionIntro($slice){

    $image = $slice->primary->image->url;
    $collection = $slice->primary->collection[0]->text;
    $collectionName = $slice->primary->collection_name[0]->text;
    $color = $slice->primary->farve;
    $text = $slice->primary->tekst[0]->text;
    $html = '<div class="collection-intro-wrapper">';
      $html .= '<div class="collection-intro" style="background-color:'.$color.';">';
        $html .= '<div class="title-wrapper">';
          $html .= '<div class="title"><h1>'.$collectionName.'</h1></div>';
          $html .= '<div class="subtitle"><h2>'.$collection.'</h2></div>';
        $html .= '</div>';
        $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><source srcset="'.$image.' 1x, '.$image.' 2x, '.$image.' 3x"><img class="intro-image lazy" src="'.$image.'" alt=""></picture>';      
      $html .= '</div>';
      $html .= '<div class="collection-intro-spacer"></div>';
      $html .= '<div class="collection-intro-text">';
      $html .= '<p>'.$text.'</p>';
      $html .= '</div>';
    $html .= '</div>';

    return $html;
  }
  private function getCollectionSingleImage($slice){
    // return var_dump($slice);

    $image1 = $slice->primary->image->url;
    // $linkTop1 = $slice->primary->top_clothing_link[0]->text;
    // $linkBottom1 = $slice->primary->bottom_clothing_link[0]->text;
    // $nameTop1 = $slice->primary->top_clothing_name[0]->text;
    // $nameBottom1 = $slice->primary->bottom_clothing_name[0]->text;
    $position = $slice->primary->position;
    $html = '<div class="single-image wrapper '.$position.'">';
      $html .= '<div class="image-row">';
        $html .= '<div class="image-container one">';
          // $full = "";
          // if($nameBottom1 == ""){
          //   $full = "full";
          // }
          // $html .= '<a href="'.$linkTop1.'" class="image-text-top '.$full.'">';
          // $html .= '<h2>'.$nameTop1.'</h2>';
          // $html .= '</a>';
          // if($nameBottom1 != ""){
          //   $html .= '<a href="'.$linkBottom1.'" class="image-text-bottom">';
          //   $html .= '<h2>'.$nameBottom1.'</h2>';
          //   $html .= '</a>';
          // }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img class="lazy" src="'.$image1.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';
      
    return $html;
  }
  private function getCollectionFullWidthImage($slice){
    $image1 = $slice->primary->image->url;
    $mobileImage1 = $slice->primary->image_mobile->url;
    // $linkTop1 = $slice->primary->top_clothing_link[0]->text;
    // $linkBottom1 = $slice->primary->bottom_clothing_link[0]->text;
    // $nameTop1 = $slice->primary->top_clothing_name[0]->text;
    // $nameBottom1 = $slice->primary->bottom_clothing_name[0]->text;
    $html = '<div class="full-width-image wrapper">';
      $html .= '<div class="image-row">';
        $html .= '<div class="image-container one">';
          // $full = "";
          // if($nameBottom1 == ""){
          //   $full = "full";
          // }
          // $html .= '<a href="'.$linkTop1.'" class="image-text-top '.$full.'">';
          // $html .= '<h2>'.$nameTop1.'</h2>';
          // $html .= '</a>';
          // if($nameBottom1 != ""){
          //   $html .= '<a href="'.$linkBottom1.'" class="image-text-bottom">';
          //   $html .= '<h2>'.$nameBottom1.'</h2>';
          //   $html .= '</a>';
          // }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$mobileImage1.' 1x, '.$mobileImage1.' 2x, '.$mobileImage1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img class="lazy" src="'.$image1.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
  private function getCollectionFullWidthImage2($slice){
    $image1 = $slice->primary->image_1->url;
    $mobileImage1 = $slice->primary->image_1_mobile->url;
    // $linkTop1 = $slice->primary->image_1_top_clothing_link[0]->text;
    // $linkBottom1 = $slice->primary->image_1_bottom_clothing_link[0]->text;
    // $nameTop1 = $slice->primary->image_1_top_clothing_name[0]->text;
    // $nameBottom1 = $slice->primary->image_1_bottom_clothing_name[0]->text;
    $image2 = $slice->primary->image_2->url;
    $mobileImage2 = $slice->primary->image_2_mobile->url;
    // $linkTop2 = $slice->primary->image_1_top_clothing_link[0]->text;
    // $linkBottom2 = $slice->primary->image_2_bottom_clothing_link[0]->text;
    // $nameTop2 = $slice->primary->image_2_top_clothing_name[0]->text;
    // $nameBottom2 = $slice->primary->image_2_bottom_clothing_name[0]->text;
    $html = '<div class="full-width-image-2 wrapper">';
      $html .= '<div class="image-row">';
        $html .= '<div class="image-container one">';
          // $full = "";
          // if($nameBottom1 == ""){
          //   $full = "full";
          // }
          // $html .= '<a href="'.$linkTop1.'" class="image-text-top '.$full.'">';
          // $html .= '<h2>'.$nameTop1.'</h2>';
          // $html .= '</a>';
          // if($nameBottom1 != ""){
          //   $html .= '<a href="'.$linkBottom1.'" class="image-text-bottom">';
          //   $html .= '<h2>'.$nameBottom1.'</h2>';
          //   $html .= '</a>';
          // }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$mobileImage1.' 1x, '.$mobileImage1.' 2x, '.$mobileImage1.' 3x"><source srcset="'.$image1.' 1x, '.$image1.' 2x, '.$image1.' 3x"><img class="lazy" src="'.$image1.'" alt=""></picture>';
        $html .= '</div>';
        $html .= '<div class="image-container two">';
          // $full = "";
          // if($nameBottom2 == ""){
          //   $full = "full";
          // }
          // $html .= '<a href="'.$linkTop2.'" class="image-text-top '.$full.'">';
          // $html .= '<h2>'.$nameTop2.'</h2>';
          // $html .= '</a>';
          // if($nameBottom2 != ""){
          //   $html .= '<a href="'.$linkBottom2.'" class="image-text-bottom">';
          //   $html .= '<h2>'.$nameBottom2.'</h2>';
          //   $html .= '</a>';
          // }
          $html .= '<picture><source media="(max-width: 767px)" srcset="'.$mobileImage2.' 1x, '.$mobileImage2.' 2x, '.$mobileImage2.' 3x"><source srcset="'.$image2.' 1x, '.$image2.' 2x, '.$image2.' 3x"><img class="lazy" src="'.$image2.'" alt=""></picture>';
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
}
