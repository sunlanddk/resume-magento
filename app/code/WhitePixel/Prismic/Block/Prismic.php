<?php

namespace WhitePixel\Prismic\Block;
use \WhitePixel\Prismic\Helper\Data;

class Prismic extends \Magento\Framework\View\Element\Template
{
  protected $page;
  private $content;

  public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    Data $helper,
    array $data = []
  ) {
    $this->helper = $helper;
    parent::__construct($context, $data);
  }
  public function _prepareLayout()
  {
    return parent::_prepareLayout();
  }
  /**
   * Retrieve Pricmic content
   *
   * @return content
   */
  public function getPrismicContent($uid, $lang, $preview = false)
  {
    $this->content = $this->helper->getPrismicPage($uid, $lang, $preview);

    if($this->content->type == 'front_page'){
      return $this->getFrontPage();
    }

    // foreach ($contentElements as $contentElement) {
    //   if($contentElement->slice_type)
    // }
    return "oops nothing found";
  }
  private function getFrontPage(){
    $html = "";
    foreach ($this->content->data->body as $slice) {
      
      switch ($slice->slice_type) {
        case 'full_width_image_sticky':
          $html .= $this->getFullWidthImageStickyHtml($slice);
          break;
        default:
          $html .= "<div>".$slice->slice_type."</div>";
          break;
      }
    }
    return $html;
  }
  private function getFullWidthImageStickyHtml($slice){
    $image = $slice->primary->image->url;
    $mobileImage = $slice->primary->mobile_image->url;
    $title = $slice->primary->title[0]->text;
    $subtitle = $slice->primary->subtitle[0]->text;
    $subtitle_link = $slice->primary->subtitle_link->url;
    $html = '<div class="full-width-image-sticky wrapper">';
    $html .= '<div class="title"><h1>'.$title.'</h1></div>';
    $html .= '<div class="subtitle"><a href="'.$subtitle_link.'">'.$subtitle.'</a></div>';
    $html .= '<div class="image"><img src="'.$image.'" /></div>';
    $html .= '<div class="mobileImage"><img src="'.$mobileImage.'" /></div>';
    $html .= '</div>';
    return $html;
  }

}