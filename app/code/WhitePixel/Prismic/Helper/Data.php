<?php

namespace WhitePixel\Prismic\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Prismic\Api;
use \Prismic\LinkResolver;
use \Prismic\Predicates;
use \Prismic\Dom\Link;
use \Prismic\Dom\RichText;
use \Prismic\Dom\Date;

class Data extends AbstractHelper
{
    public function getPreviewRef() {
      $cookieNames = [
          str_replace(['.',' '], '_', Api::PREVIEW_COOKIE),
          Api::PREVIEW_COOKIE,
      ];
      foreach ($cookieNames as $cookieName) {
          if (isset($_COOKIE[$cookieName])) {
              return $_COOKIE[$cookieName];
          }
      }
      return null;
    }
    public function getPrismicPage($uid, $lang = 'da-dk', $preview = false)
    {

      $options = [
          'ref' => false,
          'prismic.api'   => "https://resumecph.cdn.prismic.io/api/v2",
          'prismic.token' => "MC5YWElka2hFQUFDWUFJZEFy.Ju-_ve-_vWvvv73vv71d77-9Uu-_ve-_vQTvv73vv71rfu-_ve-_vUBT77-977-9BjlyJQh277-9Uxfvv70",
      ];

      $api = Api::get($options['prismic.api'], $options['prismic.token']);


      if($preview){
        $previewRef = $this->getPreviewRef();
        $ref = $previewRef ? $previewRef : $api->master()->getRef();
        $options = [ 'lang' => $lang, 'ref' => $ref];
        $document = $api->getByUID('front_page', $uid, $options);
        // $response = $api->query(Predicates::at('document.type', 'header_image_scaleable'),[ 'ref' => $ref ]);
        return $document;
      }else{
        $options = [ 'lang' => $lang, 'ref' => $api->master()->getRef()];
        $document = $api->getByUID('collection', $uid, $options);
        if($document == NULL){
          $document = $api->getByUID('front_page', $uid, $options);  
        }
        // if($document == NULL){
        //   $document = $api->getByUID('collection_overview', $uid, $options);
        // }
        if($document == NULL){
          $document = $api->getByUID('collection_page_layout', $uid, $options);
        }
        if($document == NULL){
          $document = $api->getByUID('front_page_version_2', $uid, $options);
        }
        if($document == NULL){
          $document = $api->getByUID('page', $uid, $options);
        }
        return $document;
      }
      

      // SAVE FOR PREVIEW

      // $previewRef = $this->getPreviewRef();
      // $ref = $previewRef ? $previewRef : $api->master()->getRef();

      // $response = $api->query(Predicates::at('document.type', 'header_image_scaleable'),[ 'ref' => $ref ]);

      // return $response->results[0];

  }

}
