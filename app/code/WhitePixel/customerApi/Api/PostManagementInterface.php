<?php 
namespace WhitePixel\customerApi\Api;
 
 
interface PostManagementInterface {


	/**
	 * POST for Post api
	 * @return string
	 */
	
	public function getPost();

	/**
	 * GET for Post api
	 * @param string $param
	 * @return string
	 */
	
	public function getCustomer($param);
}