<?php
/**
 * @package     
 * @version
 **/

namespace WhitePixel\customerApi\Helper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ResourceConnection;

class Data extends \Magento\Framework\App\Helper\AbstractHelper 
{

    protected $_searchCriteriaBuilder;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $sqlResource
     */
    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\App\ResourceConnection $sqlResource,
        \Magento\Framework\App\Helper\Context $context
    ) 
    {
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sqlResource = $sqlResource;
        parent::__construct($context);
    }

    public function getCustomerData($Id) 
    {
        
        /*
        CREATE TABLE `turndigital_2`.`customer_club_data` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `bg_color` VARCHAR(40) NOT NULL , `stroke_color` VARCHAR(40) NOT NULL , `content_color` VARCHAR(40) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
        */


        $connection = $this->sqlResource->getConnection();
        $fields = array('bg_color', 'stroke_color', 'content_color');
        $sql = $connection->select()
                  // ->from('customer_club_data') // to select all fields
                  ->from('customer_club_data', $fields) // to select some particular fields                  
                  ->where('user_id = ?', $Id); // adding WHERE condition with AND
                     
        $result = $connection->fetchAll($sql); 
        return $result;
    }
    public function updateCustomerData($post) 
    {
        $connection = $this->sqlResource->getConnection();
        $sql = "UPDATE customer_club_data SET bg_color = :bgcolor, stroke_color = :strokecolor, content_color = :contentcolor WHERE user_id = :userid";
        $binds = array(
            'userid' => $post['userid'],
            'bgcolor' => $post['bgcolor'],
            'strokecolor' => $post['strokecolor'],
            'contentcolor' => $post['contentcolor']
        );
        $test = $connection->query($sql, $binds);
        return true;
    }

    public function insertCustomerData($post) 
    {
            
        $connection = $this->sqlResource->getConnection();
        $sql = "INSERT INTO customer_club_data (user_id, bg_color, stroke_color, content_color) VALUES (:userid, :bgcolor, :strokecolor, :contentcolor)";
        $binds = array(
            'userid' => $post['userid'],
            'bgcolor' => $post['bgcolor'],
            'strokecolor' => $post['strokecolor'],
            'contentcolor' => $post['contentcolor']
        );
        $test = $connection->query($sql, $binds);
        return true;


        /*
            $id = 1; // table row id to update
            $sql = "UPDATE " . $tableName . " SET name = 'Your Name', email = 'your-email@example.com' WHERE id = " . $id;
            $connection->query($sql);
        */

        
        return $post;
    }
}