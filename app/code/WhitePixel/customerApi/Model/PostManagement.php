<?php 
/**
 * @package     
 * @version
 **/

namespace WhitePixel\customerApi\Model;

use \WhitePixel\customerApi\Helper\Data;
 
class PostManagement 
{

    public function __construct( Data $customerHelper) 
    {
        $this->customerHelper = $customerHelper;
    }

	/**
	 * {@inheritdoc}
	 */
	public function getPost()
	{
		$postData = file_get_contents('php://input');
		$post = json_decode($postData, true);
		
		if(isset($post['userid']) === true && isset($post['bgcolor']) === true && isset($post['strokecolor']) === true && isset($post['contentcolor']) === true){
			$user = $this->getCustomer($post['userid']);
			if($user === false){
				$customerdata = $this->customerHelper->insertCustomerData($post);
				return true;
			}
			$customerdata = $this->customerHelper->updateCustomerData($post);
			return true;
		}

		return false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCustomer($param)
	{
		$customerdata = $this->customerHelper->getCustomerData($param);
		if(empty($customerdata) === true){
			return false;
		}

		return json_encode($customerdata[0]);
	}
}