<?php
namespace WhitePixel\frontendHelper\Block;



class Categoriesmenu extends \Magento\Framework\View\Element\Template
{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_categoryHelper = $categoryHelper;
        $this->_storeManager = $storeManager;
        $this->_categoryFactory = $categoryFactory;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }


    public function getMenuCategories(){
        $currentCategory = $this->_registry->registry('current_category');
        $category = $this->_categoryFactory->create()->load(3)->setStore($this->_storeManager->getStore());
        $subCats = $category->getChildrenCategories();

        echo '<div class="catMenu"><ul>';
        $active = '';
        if($currentCategory->getId() === $category->getId()){
            $active = 'active';
        }
        $allcategoryproduct = $this->_categoryFactory->create()->load(3)->getProductCollection()->addAttributeToSelect('*');
        echo '<li class="'.$active.'"><a href="'.$category->getUrl().'">'.$category->getName().'</a><span>'.$allcategoryproduct->count().'</span></li>';
            foreach($subCats as $category){
                $allcategoryproduct = $this->_categoryFactory->create()->load($category->getId())->getProductCollection()->addAttributeToSelect('*');

                if($allcategoryproduct->count() > 0){
                    $active = '';
                    if($currentCategory->getId() === $category->getId()){
                        $active = 'active';
                    }
                    echo '<li class="'.$active.'"><a href="'.$category->getUrl().'">'.$category->getName().'</a><span>'.$allcategoryproduct->count().'</span></li>';
                }
            }
        echo '</ul></div>';
	}

}