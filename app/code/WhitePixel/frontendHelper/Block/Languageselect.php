<?php
namespace WhitePixel\frontendHelper\Block;



class Languageselect extends \Magento\Framework\View\Element\Template
{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Catalog\Helper\Image $image,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
        $this->_categoryHelper = $categoryHelper;
        $this->_storeManager = $storeManager;
        $this->_categoryFactory = $categoryFactory;
        $this->_registry = $registry;
        $this->_productloader = $_productloader;
        $this->_image = $image;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_request = $request;
        parent::__construct($context, $data);
    }

    public function randomProducts($_max, $_amount){
        $array = array();
        while(count($array) !== $_amount){
            $rand = rand(1,$_max);
            $array[$rand] = $rand;
        }
        return $array;
    }


    public function getStoresSelecter(){
        $allStores = $this->_storeManager->getStores();
        $options = array();
        $currentStore = $this->_storeManager->getStore()->getId();
        $baseUrl = $this->_storeManager->getStore(0)->getBaseUrl();
        foreach ($allStores as $key => $value) {
            if($key == $currentStore) continue;
            echo '<div class="lang store'.$value['code'].'"><a href="'.$baseUrl.$value['code'].'">'.$value['code'].'</a></div>';
        }
            
        // $currentProduct = $this->_registry->registry('current_product');
        // $categoryIds = $currentProduct->getCategoryIds();
        // foreach ($categoryIds as $key => $value) {
        //     if($value !== 3){
        //         $products = $this->_categoryFactory->create()->load($value)->getProductCollection()->addAttributeToSelect('*');
        //         $productCount = $products->count();
        //         $randomProducts = $this->randomProducts($productCount, 7);
        //         $count = 1;
        //         echo '<div class="relatedContainer">';
        //             foreach ($products as $key => $producttemp) {
        //                 $product = $this->_productloader->create()->load($producttemp->getId())->setStore($this->_storeManager->getStore());
        //                 if ($product->getTypeId() == 'configurable') {
        //                     if(isset($randomProducts[$count]) === true and $product->getId() !== $currentProduct->getId() and $count > 7){
                                
        //                         $productImageLoaded = $this->_image
        //                             ->init($product, "category_page_grid")
        //                             ->constrainOnly(false)
        //                             ->keepAspectRatio(true)
        //                             ->keepTransparency(true)
        //                             ->keepFrame(true)
        //                             ->resize(750, 1125);

        //                         echo '<div class="relatedProduct">';
        //                             echo '<a href="'.$product->getProductUrl().'">';
        //                                 echo '<img src="'.$productImageLoaded->getUrl().'" />';
        //                                 echo '<p class="relName">'.$product->getName().'</p>';
        //                                 $currency = $this->_storeManagerInterface->getStore()->getCurrentCurrencyCode();
        //                                 $basePrice = $product->getPriceInfo()->getPrice('regular_price');
        //                                 $regularPrice = $basePrice->getMinRegularAmount()->getValue();
        //                                 $specialPrice = $product->getFinalPrice();
        //                                 if($regularPrice != $specialPrice){
        //                                     echo '<p class="priceContainer"><span class="oldPrice">'.$regularPrice.' '.$currency.'</span>'.$specialPrice.' '.$currency.'</p>';
        //                                 }
        //                                 else{
        //                                     echo '<p class="priceContainer">'.$specialPrice.' '.$currency.'</p>';
        //                                 }
        //                             echo '</a>';
        //                         echo '</div>';
        //                     }
        //                     $count ++;
        //                 }
        //             }
        //         echo '</div>';
        //         return;
        //     }
        // }
        // echo 'alex2';
	}

}