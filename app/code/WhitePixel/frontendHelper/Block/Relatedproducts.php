<?php
namespace WhitePixel\frontendHelper\Block;



class Relatedproducts extends \Magento\Framework\View\Element\Template
{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Catalog\Helper\Image $image,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        array $data = []
    ) {
        $this->_categoryHelper = $categoryHelper;
        $this->_storeManager = $storeManager;
        $this->_categoryFactory = $categoryFactory;
        $this->_registry = $registry;
        $this->_productloader = $_productloader;
        $this->_image = $image;
        $this->_storeManagerInterface = $storeManagerInterface;
        parent::__construct($context, $data);
    }

    public function randomProducts($_max, $_amount){
        $array = array();
        while(count($array) !== $_amount){
            $rand = rand(1,$_max);
            $array[$rand] = $rand;
        }
        return $array;
    }


    public function getRelatedProductsFromCat($_productId){
        $currentProduct = $this->_registry->registry('current_product');
        $categoryIds = $currentProduct->getCategoryIds();
        foreach ($categoryIds as $key => $value) {
            if((int)$value !== 3){

                $products = $this->_categoryFactory->create()->load($value)->getProductCollection()->addAttributeToFilter('type_id', 'configurable');
                $productCount = $products->count();
                if($productCount < 2){
                    return;
                }
                if($productCount > 10){
                    $productCount = 10;
                }
                $max = ( 7 > $productCount ? $productCount : 7);
                $randomProducts = $this->randomProducts($productCount, $max);
                $count = 0;
                $maxcount = 0;
                echo '<div class="relatedContainer">';
                    foreach ($products as $key => $producttemp) {

                        $product = $this->_productloader->create()->load($producttemp->getId())->setStore($this->_storeManager->getStore());
                        if ($product->getTypeId() == 'configurable') {
                            if(isset($randomProducts[$count]) === true && $maxcount < ($max - 1) && $product->getId() !== $currentProduct->getId()){
                                
                                $productImageLoaded = $this->_image
                                    ->init($product, "category_page_grid")
                                    ->constrainOnly(false)
                                    ->keepAspectRatio(true)
                                    ->keepTransparency(true)
                                    ->keepFrame(true)
                                    ->resize(300, 450);

                                echo '<div class="relatedProduct">';
                                    echo '<a href="'.$product->getProductUrl().'">';
                                        echo '<div class="infoOverlay"></div>';
                                        echo '<img src="'.$productImageLoaded->getUrl().'" />';
                                        echo '<div class="inforel">';
                                            echo '<p class="relName">'.$product->getName().'</p>';
                                            $currency = $this->_storeManagerInterface->getStore()->getCurrentCurrencyCode();
                                            $basePrice = $product->getPriceInfo()->getPrice('regular_price');
                                            $regularPrice = $basePrice->getMinRegularAmount()->getValue();
                                            $specialPrice = $product->getFinalPrice();
                                            if($regularPrice != $specialPrice){
                                                echo '<p class="priceContainer"><span class="oldPrice">'.$regularPrice.' '.$currency.'</span>'.$specialPrice.' '.$currency.'</p>';
                                            }
                                            else{
                                                echo '<p class="priceContainer">'.$specialPrice.' '.$currency.'</p>';
                                            }
                                        echo '</div>';
                                    echo '</a>';
                                echo '</div>';
                                $maxcount ++;
                            }
                            $count ++;
                        }
                    }
                    $count = 0;
                    $maxcount = 0;
                    foreach ($products as $key => $producttemp) {

                        $product = $this->_productloader->create()->load($producttemp->getId())->setStore($this->_storeManager->getStore());
                        if ($product->getTypeId() == 'configurable') {
                            if(isset($randomProducts[$count]) === true && $maxcount < ($max - 1) && $product->getId() !== $currentProduct->getId()){
                                
                                $productImageLoaded = $this->_image
                                    ->init($product, "category_page_grid")
                                    ->constrainOnly(false)
                                    ->keepAspectRatio(true)
                                    ->keepTransparency(true)
                                    ->keepFrame(true)
                                    ->resize(300, 450);

                                echo '<div class="relatedProduct">';
                                    echo '<a href="'.$product->getProductUrl().'">';
                                        echo '<div class="infoOverlay"></div>';
                                        echo '<img src="'.$productImageLoaded->getUrl().'" />';
                                        echo '<div class="inforel">';
                                            echo '<p class="relName">'.$product->getName().'</p>';
                                            $currency = $this->_storeManagerInterface->getStore()->getCurrentCurrencyCode();
                                            $basePrice = $product->getPriceInfo()->getPrice('regular_price');
                                            $regularPrice = $basePrice->getMinRegularAmount()->getValue();
                                            $specialPrice = $product->getFinalPrice();
                                            if($regularPrice != $specialPrice){
                                                echo '<p class="priceContainer"><span class="oldPrice">'.$regularPrice.' '.$currency.'</span>'.$specialPrice.' '.$currency.'</p>';
                                            }
                                            else{
                                                echo '<p class="priceContainer">'.$specialPrice.' '.$currency.'</p>';
                                            }
                                        echo '</div>';
                                    echo '</a>';
                                echo '</div>';
                                $maxcount ++;
                            }
                            $count ++;
                        }
                    }
                echo '</div>';
                return;
            }
        }
        return;
	}

}