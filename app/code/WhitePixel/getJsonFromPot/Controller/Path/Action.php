<?php
/**
* @package     
* @version
**/

namespace WhitePixel\getJsonFromPot\Controller\Path;

class Action extends \Magento\Framework\App\Action\Action 
{
	
    protected $_context;
    protected $_pageFactory;
    protected $_jsonEncoder;
    protected $_orderHelper;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\EncoderInterface $encoder,
        \WhitePixel\getJsonFromPot\Helper\OrderData $orderHelper,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_context = $context;
        $this->_pageFactory = $pageFactory;
        $this->_orderHelper = $orderHelper; 
		    $this->_jsonEncoder = $encoder;        
        parent::__construct($context);
    }
    
	/**
	 * Takes the place of the M1 indexAction. 
	 * Now, every action has an execute
	 *
	 ***/
    public function execute() 
    {    	
        // $json_params = json_decode(file_get_contents("php://input"));
        $token = 'ylb0q43hoygoehyxf1oyquhjpb1kdh7x';
        $data = ['response'=>null];
        if(isset($_GET['frompot'])){
            //$potObject = json_encode($json_params->fromPot);

            $data = false;
            $remoteurl = 'http://b2b.resumecph.dk/resume';
            $url = 'https://resumecph.dk';
            $apiUrl = 'https://resumecph.dk';
            // $url = "http://turndigital.prod41.magentohotel.dk/resumecph";
            // $apiUrl = "http://turndigital.prod41.magentohotel.dk/resumecph";
            $rootfolder = '/var/www/resumecph.dk/public_html/';
            // $rootfolder = '/var/www/turndigital.dk/public_html/resumecph/';
            if($_GET['request'] == 'complete_order'){

                $data = array('success' => 'Send');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;


                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                // $helperExport = $objectManager->create('WhitePixel\orderCompleteNew\Helper\Data');
                $orderInterface = $objectManager->get('\Magento\Sales\Api\Data\OrderInterface');
                $orderInfo = $json_params->fromPot->data;
                $articles = $orderInfo->Articles;


                $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')
                  ->getCollection()
                  ->addAttributeToSelect('*')
                  ->addFieldToFilter('is_in_pot',['eq' => $orderInfo->potId])
                  ->load();

                foreach($orderDatamodel as $_orderData){
                  $_order = $orderInterface->load($_orderData->getId());

                  if ($_order->canShip()) {
                      // Initialize the order shipment object
                       $convertOrderToShip = $objectManager->create('Magento\Sales\Model\Convert\Order');
                       $shipment = $convertOrderToShip->toShipment($_order);
                      // Loop through order items
                      foreach ($_order->getAllItems() AS $orderItem) {
                          // Check if order item has qty to ship or is virtual
                          if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                              continue;
                          }
                          $continue = true;
                          $qty = 0;
                          $sku = $orderItem->getSku();
                          foreach ($articles as $key => $value) {
                              if($value->VariantCode == $sku){
                                  $qty = $value->Quantity;
                                  $continue = false;
                              }    
                          }
                          if($continue){ continue; }
                          if($qty == 0){ continue; }

                          // $qtyShipped = $orderItem->getQtyToShip();
                          // Create shipment item with qty
                          $shipmentItem = $convertOrderToShip->itemToShipmentItem($orderItem)->setQty($qty);
                          // Add shipment item to shipment
                          $shipment->addItem($shipmentItem);
                      }

                      // // Register shipment
                      $shipment->register();
                      $shipment->getOrder()->setIsInProcess(true);

                      try {
                          // Save created shipment and order
                          $shipment->save();
                          $shipment->getOrder()->save();

                          // Send email
                          //$objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                          //    ->notify($shipment);
                          //$shipment->save();

                          $totalQty = 0;
                          if ($_order->canInvoice()) {
                              $convertOrderToInvoice = $objectManager->create('Magento\Sales\Model\Convert\Order');
                              $invoice = '';
                              $invoice = $convertOrderToInvoice->toInvoice($_order);
                              foreach ($_order->getAllItems() as $_orderItem) {
                                  
                                  $continue = true;
                                  $qty = 0;
                                  $sku = $_orderItem->getSku();
                                  foreach ($articles as $key => $value) {
                                      if($value->VariantCode == $sku){
                                          $qty = $value->Quantity;
                                          $continue = false;
                                      }    
                                  }

                                  if($continue){ 
                                    // echo $sku.' not sold';
                                    // echo '<br />';
                                    continue; 
                                  }
                                  if($qty == 0){ 
                                    // echo $sku.' not sold = 0';
                                    // echo '<br />';
                                    continue; 
                                  }

                                  $invoiceItem = $convertOrderToInvoice->itemToInvoiceItem($_orderItem)->setQty($qty);

                                  $totalQty += $qty;
                                  $invoice->addItem($invoiceItem);
                              }

                              $invoice->setTotalQty($totalQty);
                              $invoice->collectTotals();
                              $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                              $invoice->register();

                              try {
                                  // Save created shipment and order
                                  // $invoice->setState(\Magento\Sales\Model\Order\Invoice::STATE_PAID);
                                  $invoice->save();
                                  $invoice->getOrder()->save();
                                  
                                  // Send email
                                  // $objectManager->create('Magento\Sales\Model\Order\Email\Sender\InvoiceSender')
                                  //    ->send($invoice);

                              } catch (\Exception $e) {
                                 echo "invoice Not Created". $e->getMessage(); exit;
                              }

                          }

                      } catch (\Exception $e) {
                         echo "Shipment Not Created". $e->getMessage(); exit;
                      }

                      //echo "Shipment Succesfully Generated for order: #".$orderId.'<br /><br /><br />';
                      $count ++;
                      if($_order->getStatus() !== 'complete'){
                          $_order->cancel()->save();
                      }
                      $_order->setIsInPot($potId);
                      $_order->save();

                  }
                  
                  
                }

                $data = array('success' => 'Send');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;
            }
            if($_GET['request'] == 'create_season'){
                $ch = curl_init($remoteurl."/sfmodule/sw/api/season/list/".$_GET['data']);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // I need to set my user id in the curl to get access the server
                // curl_setopt($ch, CURLOPT_COOKIE, "UID=1513152036");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                $result = curl_exec($ch);
                $fromPotJson = $result;

                $myfile = fopen($rootfolder."td_inc/seasoncreate.json", "w");
                fwrite($myfile, $fromPotJson);
                fclose($myfile);
                $data = array('success' => 'Send');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;
            }
            if($_GET['request'] == 'delete_article'){
                $data = array('success' => 'deleted');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;
                $ch = curl_init($remoteurl."/sfmodule/sw/api/product/list/".$json_params->fromPot->data);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // I need to set my user id in the curl to get access the server
                // curl_setopt($ch, CURLOPT_COOKIE, "UID=1513152036");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                $result = curl_exec($ch);
                $fromPotJson = $result;

                // $userData = array("username" => "aro@whitepixel.dk", "password" => "fredeE%%1875");
                //   $ch = curl_init($url."/rest/V1/integration/admin/token");
                //   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                //   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
                //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //   curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
                //   $token = curl_exec($ch);
                //   curl_close($ch);

                $productArray = json_decode($fromPotJson);

                 foreach ($productArray as $key => $value) {
                  $confSku = (int)$key;
                  foreach ($value->colors as $color => $arrayvalue) {
                    $colorKey = $color;
                    foreach ($arrayvalue->sizes as $sizeId => $sizevalue) {
                      $simpleSku = $sizevalue->sku;
                      $ch = curl_init($url."/index.php/rest/V1/products/".$simpleSku);
                      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                      $result = curl_exec($ch);
                      $result = json_decode($result);
                    }
                    $ch = curl_init($url."/index.php/rest/V1/products/".$confSku.'_'.$colorKey);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                    $result = curl_exec($ch);
                    curl_close($ch);
                  }
                }

                $myfile = fopen($rootfolder."td_inc/deletearticlelog.json", "a");
                fwrite($myfile, 'At: '.date().' -- '. $fromPotJson . '
                    ');
                fclose($myfile);

                $data = array('success' => 'deleted');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;
            }
            if($_GET['request']== 'delete_season'){
              $data = array('success' => 'deleted');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;
                $ch = curl_init($remoteurl."/sfmodule/sw/api/season/list/".$json_params->fromPot->data);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // I need to set my user id in the curl to get access the server
                // curl_setopt($ch, CURLOPT_COOKIE, "UID=1513152036");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                $result = curl_exec($ch);
                $fromPotJson = $result;

                $myfile = fopen($rootfolder."td_inc/deleteseason.json", "w");
                fwrite($myfile, $fromPotJson);
                fclose($myfile);
                $data = true;
            }
            if($_GET['request'] == 'create_article'){
                $data = array('success' => 'deleted');
                $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
                return;
                //////////////////////////////////////////// Start of create_article ////////////////////////////////////////

                $time = time();
                $dateTime =  date("d/m/y - h:i:s",$time);
                $starttime = microtime(true);
                $time = time();
                $dateTime =  date("d/m/y - h:i:s",$time);

                // $userData = array("username" => "api", "password" => "resumeapi12345");
                // $userData = array("username" => "aro@whitepixel.dk", "password" => "alexX%%1875");
                // $ch = curl_init($apiUrl."/rest/V1/integration/admin/token");
                // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
                // $token = curl_exec($ch);
                // // echo '<br />';
                // // echo PHP_EOL;

                // $tokenObj = json_decode($token);
                // if(isset($tokenObj->message)){
                //     $myfile = fopen("/var/www/resumecph.dk/public_html/td_inc/cronImport.log", "a");
                //     fwrite($myfile, ' Token cant be set... 
                //             ');
                //     fclose($myfile);
                //     die();
                // }


                $ch = curl_init($remoteurl."/sfmodule/sw/api/product/list/".$json_params->fromPot->data);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // I need to set my user id in the curl to get access the server
                // curl_setopt($ch, CURLOPT_COOKIE, "UID=1513152036");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                $result = curl_exec($ch);
                
                $fromPotJson = $result;
                $productArray = json_decode($fromPotJson);
                $statusReturn = array(
                    'success' => array(),
                    'error'   => array(),
                );

                // Get list of attributes from magento
                $ch = curl_init($apiUrl."/rest/all/V1/products/attribute-sets/4/attributes");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                $attrListJson = curl_exec($ch);
                $attributeList = json_decode($attrListJson);
                curl_close($ch);

                $sizesArray = array();
                $colorsArray = array();

                // Loop through and get sizes and colors that are not in magento yet.
                foreach ($productArray as $key => $value){
                    $colors = $value->colors;
                    foreach ($colors as $color => $arrayvalue){
                        $colorsArray[$color] = array('id'=>$color, 'name'=>$arrayvalue->colorName->default);
                        $sizes = $arrayvalue->sizes;
                        foreach ($sizes as $sizeId => $sizevalue){
                            $sizesArray[$sizeId] = array('id'=>$sizeId, 'name'=>$sizeId);
                        }
                    }
                }

                /// Create size if not in magento
                foreach ($sizesArray as $ckey => $cvalue) {
                    // Get size from pot and magento 
                    $sizeLabel = strtolower($ckey);
                    // check if color exists if not create color in magento
                    foreach($attributeList as $attrGroup){
                        if($attrGroup->attribute_code == 'size'){
                            $attributes = $attrGroup->options;
                            $exists = 'false';
                            foreach ($attributes as $key => $val) {
                                if ((string) $val->label == (string) $sizeLabel) {
                                    $exists = 'true';
                                    // echo 'Denne størrelse findes allerede: '. $sizeLabel;
                                    // echo PHP_EOL;
                                }
                            }
                            if($exists == 'false'){
                                // echo 'Denne størrelse findes ikke: '. $sizeLabel;
                                // echo PHP_EOL;
                                $attrOption = array(
                                'label' => $sizeLabel,
                                );
                                $attrOption = json_encode(array('option' => $attrOption));              
                                $ch = curl_init($apiUrl."/index.php/rest/V1/products/attributes/size/options");
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $attrOption);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                                $result = curl_exec($ch);
                                $result = json_decode($result);
                                curl_close($ch);
                                if($result == 1){
                                   // echo $sizeLabel . ' has been created succesfully';
                                    // echo PHP_EOL;
                                    // echo PHP_EOL;
                                } 
                            }
                        }
                    }    
                }

                /// Create color if not in magento
                foreach ($colorsArray as $ckey => $cvalue) {
                    $colorId = $ckey;

                    // check if color exists if not create color in magento
                    foreach($attributeList as $attrGroup){
                        if($attrGroup->attribute_code == 'color'){
                            $attributes = $attrGroup->options;

                            $exists = 'false';
                            foreach ($attributes as $key => $val) {
                                // if ((string) $val->label == (string) $colorId) {
                                if ((string) $val->label == (string) $cvalue['name']) {
                                    $exists = 'true';
                                    // echo 'Denne farve findes allerede: '. $cvalue['name'];
                                    // echo PHP_EOL;
                                }
                            }
                            if($exists == 'false'){
                                $colArr = array();
                                array_push($colArr, array('store_id' => 0, 'label' => $cvalue['name'] ) );

                                $attrOption = array(
                                    'label' => utf8_decode($colorId),
                                    'store_labels'   => $colArr,
                                 );
                                // echo 'Denne farve findes ikke: '. $cvalue['name'];
                                // echo PHP_EOL;

                                $attrOption = json_encode(array('option' => $attrOption));              
                                $ch = curl_init($apiUrl."/index.php/rest/all/V1/products/attributes/color/options");
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $attrOption);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                                $result = curl_exec($ch);
                                $result = json_decode($result);

                                // print_r($result);
                                curl_close($ch);
                                if($result == 1){
                                    // echo $cvalue['name'] . ' has been created succesfully';
                                    // echo PHP_EOL;
                                    // echo PHP_EOL;
                                } 
                            }
                        }
                    } 
                }

                // Get new list of attributes from magento, with the newly added attributes
                $ch = curl_init($apiUrl."/rest/all/V1/products/attribute-sets/4/attributes");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                $attrListJson = curl_exec($ch);
                $attributeList = json_decode($attrListJson);
                curl_close($ch);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                function createSimpleProduct($_storeCode, $_name, $_sku, $_currency, $_size, $_datetime, $_qty, $_isinstock, $_apiUrl, $_token, $_seasonId, $_collectionmemberid, $_status){
                    $simpleProduct = array(
                    'sku'               => $_sku,
                    'name'              => $_name,
                    'visibility'        => 1,
                    'type_id'           => 'simple',
                    'price'             => $_currency,
                    'status'            => $_status,
                    'attribute_set_id'  => 4,
                    'custom_attributes' => array(
                        array( 'attribute_code' => 'size', 'value' => $_size ),
                        array( 'attribute_code' => 'new_product', 'value' => $_datetime ),
                        array( 'attribute_code' => 'season_id', 'value' => $_seasonId ),
                        array( 'attribute_code' => 'colmemberid', 'value' =>  $_collectionmemberid),
                    ),
                    'extensionAttributes' => array(
                        'stockItem'   => array('qty' => $_qty, "isInStock" => $_isinstock, "manageStock" => true,)
                    ),
                );

                    $simpleProduct = json_encode(array('product' => $simpleProduct));

                    $ch = curl_init($_apiUrl."/index.php/rest/".$_storeCode."/V1/products");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $simpleProduct);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $_token));
                    $result = curl_exec($ch);
                    $result = json_decode($result);
                    return $result->id;
                }

                function createConfigurableProuct($_sku, $_name, $_currency, $_url, $_color, $_dateTime, $_sizeOption, $_simpleProductIds, $_apiUrl, $_token, $_seasonId, $_description, $_storeCode, $_collectionmemberid, $_status = 1){

                    $confProductArray = array(
                         'sku'               => $_sku,
                         'name'              => $_name,
                         'visibility'        => 1, /*'catalog',*/
                         'type_id'           => 'configurable',
                         'price'             => $_currency,
                         'status'            => $_status,
                         'attribute_set_id'  => 9,
                         'custom_attributes' => array(
                             array( 'attribute_code' => 'has_options', 'value' => 1 ),
                             array( 'attribute_code' => 'url_key', 'value' => $_url ),
                             array( 'attribute_code' => 'color', 'value' => $_color ),
                             array( 'attribute_code' => 'new_product', 'value' => $_dateTime ),
                             array( 'attribute_code' => 'category_ids', 'value' => [3] ),
                             array( 'attribute_code' => 'season_id', 'value' =>  $_seasonId),
                             array( 'attribute_code' => 'colmemberid', 'value' =>  $_collectionmemberid),
                             array( 'attribute_code' => 'description', 'value' => $_description ),
                           ),
                         'extensionAttributes' => array(
                           'stockItem'   => array("isInStock" => true),
                           'configurable_product_options'   => array(
                             'values' => array(
                               'attribute_id'  => 138,
                               'label'         => 'Size',
                               'position'      => 0,
                               'values'        => $_sizeOption,
                             ),
                           ),
                           'configurable_product_links'    => $_simpleProductIds,
                         ),
                       );
                       $confProductArray = json_encode(array('product' => $confProductArray));
                       
                        $ch = curl_init($_apiUrl."/index.php/rest/".$_storeCode."/V1/products");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $confProductArray);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $_token));
                        $result = curl_exec($ch);
                }

                foreach ($productArray as $key => $value) {
                    $confSku = (int)$key;
                    $colors = $value->colors;
                    $name = $value->name->default;
                    $description = '';
                    foreach ($colors as $color => $arrayvalue) {
                        $colorKey = $color;
                        $colorName = $arrayvalue->colorName->default;
                        $seasonId = $arrayvalue->season_id;
                        $statusReturn['success']['season'] = $season_id;
                        $collectionmemberid = $arrayvalue->collection_member_id;
                        $ch = curl_init($apiUrl."/rest/V1/products/".$confSku."-".$colorKey."");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                        $result = curl_exec($ch);
                        $result = json_decode($result);
                        // curl_close($ch);
                        if(isset($result->sku)){
                        // if(false){
                            // echo 'Denne findes allerede: '. $confSku . '-' . $colorKey;
                            // echo PHP_EOL;
                        }
                        else{
                            //echo "'".$confSku . '-' . $colorKey."', ";
                            // echo 'Denne findes ikke allerede: '. $confSku . '-' . $colorKey;
                            // echo PHP_EOL;
                            
                            $simpleProductIds = array();
                            $sizes = $arrayvalue->sizes;
                            foreach ($sizes as $sizeId => $sizevalue) {

                                // Get the id of the size from magento
                                foreach($attributeList as $attrGroup){
                                    if($attrGroup->attribute_code == 'size'){
                                        $attributes = $attrGroup->options;
                                        foreach ($attributes as $key => $val) {
                                            if($key !== 0){
                                                $sizeId = strtolower($sizeId);
                                                if ((string) $val->label == (string) $sizeId) {
                                                    $size = $val->value;
                                                }
                                            }
                                        }
                                    }
                                }
                                $qty = $sizevalue->QTY;
                                $isInStock = false;
                                if($qty > 0){
                                    $isInStock = true;
                                }

                                // if(isset($sizevalue->DKK)){
                                //     $resultId = createSimpleProduct('da', $name, $sizevalue->sku, $sizevalue->DKK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId);
                                //     array_push($simpleProductIds, $resultId);
                                // }
                                // if(isset($sizevalue->EUR)){
                                //     createSimpleProduct('eu', $name, $sizevalue->sku, $sizevalue->EUR, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId);
                                // }
                                // if(isset($sizevalue->NOK)){
                                //     createSimpleProduct('no', $name, $sizevalue->sku, $sizevalue->NOK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId);
                                // }
                                // if(isset($sizevalue->USD)){
                                //     createSimpleProduct('us', $name, $sizevalue->sku, $sizevalue->USD, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId);
                                // }
                                // // Skal ændres til gpb men er ikke i db lige nu
                                // if(isset($sizevalue->GBP)){
                                //     createSimpleProduct('uk', $name, $sizevalue->sku, $sizevalue->GBP, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId);
                                // } 
                                $resultId = createSimpleProduct('all', $name, $sizevalue->sku, $sizevalue->DKK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                            array_push($simpleProductIds, $resultId);

                            if(isset($sizevalue->DKK)){
                                createSimpleProduct('da', $name, $sizevalue->sku, $sizevalue->DKK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                                
                            }
                            else{
                                createSimpleProduct('da', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
                            }
                            if(isset($sizevalue->EUR)){
                                createSimpleProduct('eu', $name, $sizevalue->sku, $sizevalue->EUR, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                            }
                            else{
                                createSimpleProduct('eu', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
                            }
                            if(isset($sizevalue->NOK)){
                                createSimpleProduct('no', $name, $sizevalue->sku, $sizevalue->NOK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                            }
                            else{
                                createSimpleProduct('no', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
                            }
                            if(isset($sizevalue->USD)){
                                createSimpleProduct('us', $name, $sizevalue->sku, $sizevalue->USD, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                            }
                            else{
                                createSimpleProduct('us', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
                            }
                            if(isset($sizevalue->SEK)){
                                createSimpleProduct('se', $name, $sizevalue->sku, $sizevalue->SEK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                            }
                            else{
                                createSimpleProduct('se', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
                            }
                            if(isset($sizevalue->GBP)){
                                createSimpleProduct('uk', $name, $sizevalue->sku, $sizevalue->GBP, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
                            }
                            else{
                                createSimpleProduct('uk', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0); 
                            }  
                                
                            }

                            $testArray = array($colorKey);
                            foreach($testArray as $array){
                               $colorId = $array;
                               foreach($attributeList as $attrGroup){
                                    if($attrGroup->attribute_code == 'color'){
                                        $attributes = $attrGroup->options;
                                        foreach ($attributes as $key => $val) {
                                            if($key !== 0){
                                                // if ($val->label == $colorId) {
                                                if ($val->label == $colorName) {
                                                    $color = $val->value;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            $sizeOption = array();
                            foreach($attributeList as $attrGroup){
                                if($attrGroup->attribute_code == 'size'){
                                    $options = $attrGroup->options;
                                    foreach ($options as $option) {
                                        if($option->value != ''){
                                            array_push($sizeOption, array('value_index' => $option->value));
                                        } 
                                    }
                                }
                            }

                            // createConfigurableProuct($confSku.'-'.$colorKey, $name, $sizevalue->DKK, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId);
                            
                            if(isset($value->description->uk) === true){
                                $description = $value->description->uk;
                            }
                            createConfigurableProuct($confSku.'-'.$colorKey, $name, $sizevalue->DKK, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'all', $collectionmemberid);
                            $daName = $name;
                            $dadescription = $description;
                            if(isset($value->name->da)){
                                $daName = $value->name->da.' da';
                            }
                            if(isset($value->description->da)){
                                $dadescription = $value->description->da.' da';
                            }

                            if(isset($lastPrices->DKK)){
                                createConfigurableProuct($confSku.'-'.$colorKey, $daName, $sizevalue->DKK, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $dadescription, 'da', $collectionmemberid);
                            }
                            else{
                                createConfigurableProuct($confSku.'-'.$colorKey, $daName, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $dadescription, 'da', $collectionmemberid, 0);
                            }

                            if(isset($lastPrices->EUR) === false){
                                createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'eu', $collectionmemberid, 0);
                            }
                            if(isset($lastPrices->NOK) === false){
                                createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'no', $collectionmemberid, 0);
                            }
                            if(isset($lastPrices->USD) === false){
                                createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'us', $collectionmemberid, 0);
                            }
                            if(isset($lastPrices->SEK) === false){
                                createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'se', $collectionmemberid, 0);
                            }
                            if(isset($lastPrices->GBP) === false){
                                createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'uk', $collectionmemberid, 0);
                            }
                            $statusReturn['success']['collectionmembers'][$collectionmemberid] = 1;
                            // echo $confSku.'-'.$colorKey .' Has been craeted !!!! ####';
                            // echo PHP_EOL;
                            // echo PHP_EOL;
                            // echo PHP_EOL;
                        }
                    }
                }


                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                $endtime = microtime(true);
                $timediff = $endtime - $starttime;

                $newStatus = json_encode($statusReturn);
                $ch = curl_init("http://b2b.resumecph.dk/resume/sfmodule/sw/api/update/status/collectionmember");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $newStatus);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                $result = curl_exec($ch);
                curl_close($ch);
                

                $data = true;
                //////////////////////////////////////////// End of create_article ////////////////////////////////////////
            }
            if($data == false){
                $data = 'Not allowed request';
            }
        }
        else{
            $data = array('error' => 'Not allowed request');
        }

        $this->getResponse()->representJson($this->_jsonEncoder->encode($data));
    	return;
    }
}
