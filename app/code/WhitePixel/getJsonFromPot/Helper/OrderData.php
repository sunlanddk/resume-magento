<?php
/**
 * @package     
 * @version
 **/

namespace WhitePixel\getJsonFromPot\Helper;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class OrderData extends \Magento\Framework\App\Helper\AbstractHelper 
{

    protected $_orderRepositoryInterface;
    protected $_searchCriteriaBuilder;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
    	\Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\App\Helper\Context $context
    ) 
    {
        $this->_orderRepositoryInterface = $orderRepositoryInterface;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context);
    }

    public function createProducts($potObject) 
    {

        // $products = json_decode($orderId);
        // $data = [];

        // $count = 0;
        // foreach ($products as $product) {
        //     $count ++;
        //     $data['items'][$product] = $count; 
        // }
        // $this->_searchCriteriaBuilder->addFilter('increment_id', $orderId);
        // $orders = $this->_orderRepositoryInterface->getList(
        //     $this->_searchCriteriaBuilder->create()
        // )->getItems();
        // if (count($orders)) {
        //     $order = reset($orders);
        //     $data = $order->getData();
        //     foreach($order->getItems() as $item) {
        //         $data['items'][$item->getItemId()] = $item->getData();
        //     }
        // }
        // return $data;
        // return $products;
        return $potObject;
    }
}