<?php
namespace WhitePixel\orderComplete\Cron;
use \Psr\Log\LoggerInterface;

class Orderexport {

/**
   * Write to system.log
   *
   * @return void
   */

    public function execute() {
      $time = time();
      $dateTime =  date("d/m/y - h:i:s",$time);

      // $myfile = fopen("/var/www/resumecph.dk/public_html/td_inc/orderexport.log", "w");
      //  fwrite($myfile, $dateTime.' orderexport kører
      //   ');
      //  fclose($myfile);
      	
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $helperExport = $objectManager->create('WhitePixel\orderComplete\Helper\Data');

    	$orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')
    		->getCollection()
    		->addAttributeToSelect('*')
    		->addFieldToFilter('status',['eq' => 'Processing'])
    		->addFieldToFilter('is_in_pot',['eq' => '0'])
    		->load();

  		foreach($orderDatamodel as $orderData){
  			$orderId = $orderData->getId();
  			$helperExport->orderExport($orderId);
  		}
    }
}
