<?php
namespace WhitePixel\orderComplete\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function getPOTApiUrl()
    {
        return $this->scopeConfig->getValue(
            'bygreencotton_settings/general/pot_url',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Export Order to POT
     *
     * @param int $orderId
     */
    public function orderExport($orderId)
    {
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();

        $url = 'http://b2b.resumecph.dk/resume/sfmodule/sw/api/orders/new';
        $headers = array(
            "Content-Type: application/json;charset=\"utf-8\"",
            "Accept: application/json",
        );
        $curl = $objectManager->create('\Magento\Framework\HTTP\Client\Curl');
        $curlApi = $objectManager->create('\Magento\Framework\HTTP\Client\Curl');
        $curl->setHeaders($headers);

        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);

        /** @var \Magento\Store\Model\Store $orderStore */
        $orderStore = $objectManager->create('Magento\Store\Model\Store');
        $orderStore->load($order->getStoreId());

        $orderHistory = $objectManager->create('\Magento\Sales\Model\Order\Status\HistoryFactory')
            ->create()
            ->load($order->getId(), 'parent_id');

        $billing = $order->getBillingAddress();
        $shipping = $order->getShippingAddress();
        $allShippingMethods = $objectManager->create('\Magento\Shipping\Model\Config\Source\Allmethods')->toOptionArray();
        $currentMethod = array();
        foreach ($allShippingMethods as $methodName => $methodData) {
            $position = strpos($order->getShippingMethod(), $methodName);
            if($position !== false &&  $position == 0) {
                if(empty($currentMethod) || $currentMethod['code'] == '') {
                    $currentMethod['label'] = $methodData['label'];
                    $currentMethod['code'] = $methodName;
                } else {
                    if (strlen($currentMethod['code']) < strlen($methodName)) {
                        $currentMethod['label'] = $methodData['label'];
                        $currentMethod['code'] = $methodName;
                    }
                }
            }
        }
        $shippingArray = $shipping->getStreet();
        $shippingStreet = '';
        $billingArray = $billing->getStreet();
        $billingStreet = '';
        if($shippingArray && is_array($shippingArray)) {
            foreach ($shippingArray as $shippingVal) {
                $shippingStreet .=  $shippingVal . ' ';
            }
        } else {
            $shippingStreet = $shippingArray;
        }

        if($billingArray && is_array($billingArray)) {
            foreach ($billingArray as $billingVal) {
                $billingStreet .= $billingVal . ' ';
            }
        } else {
            $billingStreet = $billingArray;
        }
        $paymentInformation = $order->getPayment()->getAdditionalInformation();

        $orderDiscount = 0;
        $articlesDiscount = 0;
        if ($order->getAppliedRuleIds() != '') {
            $ruleIds = explode(",", $order->getAppliedRuleIds());
            foreach ($ruleIds as $ruleId) {
              $rule = $objectManager->create('Magento\SalesRule\Model\Rule');
              $rule->load($ruleId);
              if ($rule->getSimpleAction() == 'by_percent') {
                  $articlesDiscount = $rule->getDiscountAmount();
              } else {
                  $orderDiscount += $rule->getDiscountAmount();
              }
            }
        }

        $seasonId = 0;
        $orderProducts = array();
        $productPrice;
        $vat = 0;
        foreach ($order->getItems() as $product) {
            if ($product->getProductType() == 'virtual' || $product->getProductType() == 'simple') {
                $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getProductId());
                
                $orderProducts[] = array(
                    'VariantCode' => $product->getSku(),
                    'Price' => (float)$productPrice,
                    'Quantity' => (int)$product->getQtyOrdered(),
                    'Discount' => $articlesDiscount,
                    'CollectionMemberId' => $_product->getColmemberid(),
                    'Instructions' => '',
                    'Season' => $_product->getSeasonId(),
                );
                if($seasonId == 0){
                    $seasonId = $_product->getSeasonId();
                }
            }
            else{
                $productPrice = $product->getPriceInclTax();
                $vat = (int)$product->getTaxPercent();
            }
        }

        if($seasonId == 0){
            $seasonId = 1;
        }

        if($orderDiscount > 0){
          $orderProducts[] = array(
              'VariantCode' => '9999999000002', #GET FROM KEN
              'Price' => -$orderDiscount,
              'Quantity' => 1,
              'Discount' => 0,
              'Instructions' => '',
              'Description' => 'Coupon Rabat',
              'Season' => $seasonId
          );
        }
        if($order->getShippingAmount() > 0){
            $orderProducts[] = array(
                'VariantCode' => '9999999000001', #GET FROM KEN
                'Price' => number_format(($order->getShippingAmount()*((100+$vat)/100)),2),
                'Quantity' => 1,
                'Discount' => 0,
                'Instructions' => '',
                'Description' => $order->getShippingDescription(),
                'Season' => $seasonId
            );
        }
        /// 04/06/2016 12:00:00 Changed 'OrderId' => $orderId TO == 'OrderId' => $order->getIncrementId(),


        if(is_array($shippingStreet) === true){
            $shippingStreet = $shippingStreet[0];
        }
        
        $orderData = array(
            'Name' => $shipping->getFirstName() . ' ' . $shipping->getLastName(),
            'Address1' => $shippingStreet,
            'Address2' => '',
            'ZIP' => $shipping->getPostCode(),
            'City' => $shipping->getCity(),
            'CountryId' =>  $this->getDataPOT('country', $shipping->getCountryId(), $curlApi, $vat),
            'CarrierId' => $this->getDataPOT('carrier', $currentMethod['label'], $curlApi),
            'CurrencyId' => $this->getDataPOT('currency', $order->getOrderCurrencyCode(), $curlApi),
            'StoreCode' => $orderStore->getCode(),
            'Phone' => $shipping->getTelephone(),
            'Comment' => str_replace('ORDER COMMENT: ', '', $orderHistory->getComment()),
            'OrderId' => $order->getIncrementId(),
            'Discount' => '',
            'Season' => $seasonId,
            'shippingCost' => '',
            'shippingLabel' => '',
            'Articles' => $orderProducts
        );

        try {
            $curl->post($url,$orderData);
            $result = json_decode($curl->getBody());

            if (isset($result->orderId)) {
                // $order->setData('pot_order_id', $result->orderId);
                // $_order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
                // if ($order->getStatus() != 'processing' || $order->getState() != 'processing') {
                //     $order->setStatus('processing');
                //     $order->setState('processing');
                // }
                $order->setIsInPot($result->orderId);
                $order->save();
            }

        } catch (Exception $e) {
            die('Connection problem.');
        }
    }

    public function getDataPOT($apiName, $foundName, $curlApi, $_vat = null){
        $urlApi = 'http://94.143.10.160/resume/sfmodule/sw/api/list/' . $apiName;
        $headerss = array(
            "Content-Type: application/json;charset=\"utf-8\"",
            "Accept: application/json",
        );
        $curlApi->setHeaders($headerss);
        $curlApi->get($urlApi);
        $data = json_decode($curlApi->getBody(), true);

        if($apiName == 'country'){
            $newKey = false;
            foreach ($data as $key => $value) {
                if($value['isocode'] == $foundName){
                    if($value['vatpercentage'] === $_vat){
                        $newKey = $key;
                    }
                }
            }
            $key = $newKey;
        }
        else{
            $key = array_search($foundName, array_column($data,'name'));
        }

        try {
            if ($key !== false) {
                return $data[$key]['id'];
            } else {
                return false;
            }
        } catch (Exception $e) {
            // $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/order_export.log", "a");
            //              fwrite($myfile, date("d-m-Y h:i:s") .' Error export '. $e . '
            //              ');
            //              fclose($myfile);
            die('Connection problem.');
        }
    }
}
