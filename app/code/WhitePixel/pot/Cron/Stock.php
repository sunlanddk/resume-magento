<?php
namespace WhitePixel\pot\Cron;
use \Psr\Log\LoggerInterface;

class Stock {

/**
   * Write to system.log
   *
   * @return void
   */

    public function execute() {
    	return;
    	$time = time();
			$dateTime =  date("dmy",$time);
			$userData = array("username" => "api", "password" => "api12345");
			$ch = curl_init("http://resumecph.prod34.magentohotel.dk/rest/V1/integration/admin/token");
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
			echo $token = curl_exec($ch);


				$ch = curl_init("http://b2b.resumecph.dk/resume/sfmodule/sw/api/product/list/all");
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
				$result = curl_exec($ch);
				$result = json_decode($result);
			 $productArray = $result;

			  $productArray = get_object_vars($productArray);
			  $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/potimport.log", "a");
			             fwrite($myfile, date("d-m-Y h:i:s") .' New import '.'
			             ');
			             fclose($myfile);
			 foreach ($productArray as $key => $value) {
			   $confSku = (int)$key;
			   $colors = $value;
			   foreach ($colors as $color => $arrayvalue) {
			     $colorKey = $color;
			     $ch = curl_init("http://resumecph.prod34.magentohotel.dk/rest/V1/products/".$confSku."-".$colorKey."");
			     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			     curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
			     $result = curl_exec($ch);
			     $result = json_decode($result);
			     // curl_close($ch);
			     if(isset($result->sku)){
			        // echo 'Denne findes allerede: '. $confSku . '-' . $colorKey;
			        // echo '<br />';
			     	$myfile = fopen("/var/www/resumecph.dk/public_html/var/log/potimport.log", "a");
			             fwrite($myfile, date("d-m-Y h:i:s") .' Denne findes allere '  .$confSku . '-' . $colorKey . '
			             ');
			             fclose($myfile);
			     }
			     else{
			     	$myfile = fopen("/var/www/resumecph.dk/public_html/var/log/potimport.log", "a");
			             fwrite($myfile, date("d-m-Y h:i:s") .' Denne findes ikke allerede ' .$confSku . '-' . $colorKey . '
			             ');
			             fclose($myfile);
			       // echo 'Denne findes ikke allerede: '. $confSku . '-' . $colorKey;
			       // echo '<br />';
			       // echo '<br />';
			       $simpleProductIds = array();
			       $sizes = $arrayvalue;
			       foreach ($sizes as $sizeId => $sizevalue) {
			         $name = $sizevalue->Name;

			         if(strtolower($sizeId) == 'xs')
			           $size = 4;
			         if(strtolower($sizeId) == 's')
			           $size = 5;
			         if(strtolower($sizeId) == 'm')
			           $size = 6;
			         if(strtolower($sizeId) == 'l')
			           $size = 7;
			         if(strtolower($sizeId) == 'xl')
			           $size = 11;
			         if($sizeId == '34')
			           $size = 19;
			         if($sizeId == '36')
			           $size = 20;
			         if($sizeId == '38')
			           $size = 21;
			         if($sizeId == '40')
			           $size = 22;
			         if($sizeId == '42')
			           $size = 23;
								if($sizeId == 'O/S')
									$size = 27;

			         $qty = $sizevalue->QTY;
			         $isInStock = false;
			         if($qty > 0){
			           $isInStock = true;
			         }

			         $simpleProduct = array(
			             'sku'               => $sizevalue->EAN,
			             'name'              => $name,
			             'visibility'        => 1,
			             'type_id'           => 'simple',
			             'price'             => $sizevalue->DKK,
			             'status'            => 1,
			             'attribute_set_id'  => 4,
			             'custom_attributes' => array(
			                 // array( 'attribute_code' => 'category_ids', 'value' => ["4","5"] ),
			                 array( 'attribute_code' => 'size', 'value' => $size ),
			                 array( 'attribute_code' => 'new_product', 'value' => $dateTime ),
			               ),
			             'extensionAttributes' => array(
			               'stockItem'   => array('qty' => $sizevalue->QTY, "isInStock" => $isInStock, "manageStock" => true,)
			             ),
			         );

			         $simpleProduct = json_encode(array('product' => $simpleProduct));

			          $ch = curl_init("http://resumecph.prod34.magentohotel.dk/index.php/rest/V1/products");
			          curl_setopt($ch, CURLOPT_POSTFIELDS, $simpleProduct);
			          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			          curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
			          $result = curl_exec($ch);
			          $result = json_decode($result);
			         

			         array_push($simpleProductIds, $result->id);

			         $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/potimport.log", "a");
			             fwrite($myfile, date("d-m-Y h:i:s") .' '.$confSku . '-' . $colorKey . ' Denne er nu oprettet
			             ');
			             fclose($myfile);
			       }


			        $testArray = array($colorKey);
			         foreach($testArray as $array){
			           
			           $colorId = $array;
			           $ch = curl_init("http://resumecph.prod34.magentohotel.dk/rest/V1/products/attribute-sets/4/attributes");
			           curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			           curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
			           $attrListJson = curl_exec($ch);
			           $attributeList = json_decode($attrListJson);

			           // check if color exists if not create color in magento
			           foreach($attributeList as $attrGroup){
			             if($attrGroup->attribute_code == 'color'){
			               $attributes = $attrGroup->options;

			               $exists = 'false';
			               foreach ($attributes as $key => $val) {
			                 if ($val->label == $colorId) {
			                   $exists = 'true';
			                 }
			               }
			               if($exists == 'false'){
			                 $attrOption = array(
			                   'label' => $colorId,
			                 );
			                 $attrOption = json_encode(array('option' => $attrOption));              
			                 $ch = curl_init("http://resumecph.prod34.magentohotel.dk/index.php/rest/V1/products/attributes/color/options");
			                 curl_setopt($ch, CURLOPT_POSTFIELDS, $attrOption);
			                 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			                 curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
			                 $result = curl_exec($ch);
			                 $result = json_decode($result);
			                 if($result == 1){
			                   // echo $colorId . 'has been created succesfully';
			                   // echo '<br />';              
			                 } 
			               }
			             }
			           }

			           // Get the id of the color from magento
			           $ch = curl_init("http://resumecph.prod34.magentohotel.dk/rest/V1/products/attribute-sets/4/attributes");
			           curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			           curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
			           $attrListJson = curl_exec($ch);
			           $attributeList = json_decode($attrListJson);

			           foreach($attributeList as $attrGroup){
			             if($attrGroup->attribute_code == 'color'){
			               $attributes = $attrGroup->options;
			               foreach ($attributes as $key => $val) {
			                 if($key !== 0){
			                   if ($val->label == $colorId) {
			                     $color = $val->value;
			                   }
			                 }
			               }
			             }
			           }
			         }


			       $confProductArray = array(
			         'sku'               => $confSku.'-'.$colorKey,
			         'name'              => $name,
			         'visibility'        => 1, /*'catalog',*/
			         'type_id'           => 'configurable',
			         'price'             => $value->DKK,
			         'status'            => 1,
			         'attribute_set_id'  => 4,
			         'custom_attributes' => array(
			             array( 'attribute_code' => 'has_options', 'value' => 1 ),
			             array( 'attribute_code' => 'url_key', 'value' => $name.'-'.$confSku.'-'.$colorKey ),
			             array( 'attribute_code' => 'color', 'value' => $color ),
			             array( 'attribute_code' => 'new_product', 'value' => $dateTime ),
			           ),
			         'extensionAttributes' => array(
			           'stockItem'   => array("isInStock" => true),
			           'configurable_product_options'   => array(
			             'values' => array(
			               'attribute_id'  => 185,
			               'label'         => 'Size',
			               'position'      => 0,
			               'values'        => array(
			                 array( 'value_index' => 4),
			                 array( 'value_index' => 5),
			                 array( 'value_index' => 6),
			                 array( 'value_index' => 7),
			                 array( 'value_index' => 11),
			                 array( 'value_index' => 19),
			                 array( 'value_index' => 20),
			                 array( 'value_index' => 21),
			                 array( 'value_index' => 22),
			                 array( 'value_index' => 23),
												array( 'value_index' => 27),
			               ),
			             ),
			           ),
			           'configurable_product_links'    => $simpleProductIds,
			         ),
			       );

			       $confProductArray = json_encode(array('product' => $confProductArray));
							
							$myfile = fopen("/var/www/resumecph.dk/public_html/var/log/potimport.log", "a");
			             fwrite($myfile, date("d-m-Y h:i:s") .' '. $confSku.'-'.$colorKey . ' : ' . implode(", ",$simpleProductIds) . '
			             ');
			             fclose($myfile);
			       
			        $ch = curl_init("http://resumecph.prod34.magentohotel.dk/index.php/rest/V1/products");
			        curl_setopt($ch, CURLOPT_POSTFIELDS, $confProductArray);
			        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
			        // print_r($confProductArray);
			        $result = curl_exec($ch);
			     }
			   }
			 }
    }
}
