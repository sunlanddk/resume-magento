<?php
namespace WhitePixel\pot\Cron;
use \Psr\Log\LoggerInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;

class Test {
    protected $logger;
    protected $_cacheTypeList;
    protected $_cacheFrontendPool;
    protected $_indexerFactory;
    protected $_indexerCollectionFactory;
    protected $_sourceItemsSaveInterface;
    protected $_sourceItemFactory;

    public function __construct(
      LoggerInterface $logger, 
      \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, 
      \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool, 
      \Magento\Indexer\Model\IndexerFactory $indexerFactory, 
      \Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory, 
      SourceItemsSaveInterface $sourceItemsSaveInterface,
      SourceItemInterfaceFactory $sourceItemFactory
    ) {
        $this->logger = $logger;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_indexerFactory = $indexerFactory;
        $this->_indexerCollectionFactory = $indexerCollectionFactory;
        $this->_sourceItemsSaveInterface = $sourceItemsSaveInterface;
        $this->_sourceItemFactory = $sourceItemFactory;
    }

/**
   * Write to system.log
   *
   * @return void
   */
	
		public function reindex(){
			$indexerIds = array('catalogrule_rule', 'catalogrule_product');
			foreach ($indexerIds as $indexerId) {
					$indexer = $this->_indexerFactory->create();
					$indexer->load($indexerId);
					$indexer->reindexAll();
			}

			 $types = array('layout','block_html','collections','reflection','db_ddl','eav','config_integration','config_integration_api','full_page','translate','config_webservice');
			 foreach ($types as $type) {
					 $this->_cacheTypeList->cleanType($type);
			 }
			 foreach ($this->_cacheFrontendPool as $cacheFrontend) {
					 $cacheFrontend->getBackend()->clean();
//						 $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/stock.log", "a");
//						 fwrite($myfile, 'Cache Works');
//						 fclose($myfile);
			 }
		}


    private function updateStock($sku, $qty){
      $sourceItem = $this->_sourceItemFactory->create();
      $sourceItem->setSourceCode('default');
      $sourceItem->setSku($sku);
      $sourceItem->setQuantity($qty);
      $sourceItem->setStatus(1);

      try {
        $this->_sourceItemsSaveInterface->execute([$sourceItem]);     
      } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
        $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/stock.log", "a");
        fwrite($myfile, date("d-m-Y H:i:s") .' '. $sku . ': Error  ' . "\r\n");
        fclose($myfile);
      }


      $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/stock.log", "a");
      fwrite($myfile, date("d-m-Y H:i:s") .' '. $sku . ': ' . $qty . ' -  is in stock: '. 1 . "\r\n");
      fclose($myfile);
    }

    public function execute() {
        $time = time();
        $dateTime =  date("d/m/y - h:i:s",$time);
        // $myfile = fopen("/var/www/resumecph.dk/public_html/td_inc/stock.log", "a");
        //      fwrite($myfile, $dateTime.' stock kører
        //       ');
        //      fclose($myfile);


          $ch = curl_init("http://b2b.resumecph.dk/resume/sfmodule/sw/api/season/list/all");
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
          $result = curl_exec($ch);
          $result = json_decode($result);
          
          $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
          $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
          $stockinterface = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');

          $updatePriceArray = array();
          //get prices and map them to array with product sku and correct storeview code
          foreach ($result as $conf => $confValue) {
             // $cont = $key
             //$confValue = $value
            $colors = $confValue->colors;
              foreach ($colors as $colorKey => $colorValue) {
                // $colorKey = $key
                //$colorValue = $value
                $sizes = $colorValue->sizes;
                foreach ($sizes as $sizeKey => $sizeValue) {
                    // $sizeKey = $key
                    //$sizeValue = $value
                    $isInStock = 1;
                    if($sizeValue->QTY == 0){
                        $isInStock = 0;
                    }
                    if($sizeValue->QTY < 0){
                      $sizeValue->QTY = 0;
                    }

                    try {
                      $productObj = $productRepository->get($sizeValue->sku);
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                      continue;
                    }

                    if((int)$stockinterface->getStockQty($productObj->getId()) != (int)$sizeValue->QTY){
                      $this->updateStock($sizeValue->sku, $sizeValue->QTY);
                    }
                    
                    // \Magento\Framework\Exception\NoSuchEntityException
                    // try {
                    //   $productSimpID = $productObj->getId();
                    //   $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productSimpID);
                    //   if((int)$stockinterface->getStockQty($_product->getId()) != (int)$sizeValue->QTY){
                    //     $this->updateStock($sizeValue->sku, $sizeValue->QTY);
                    //     // $_product->setStockData(['qty' => $sizeValue->QTY, 'is_in_stock' => $isInStock]);
                    //     // $_product->setQuantityAndStockStatus(['qty' => $sizeValue->QTY, 'is_in_stock' => $isInStock]);
                    //     // $_product->save();
                    //     // echo $productSimpID.' - '.$sizeValue->QTY;
                    //     // echo PHP_EOL;
                    //   }
                    // } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
                    //   continue;
                    // }
                }
              }
          }


          // $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
          // $stockRepo = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');

          // $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
          // $collection = $productCollection->setFlag('has_stock_status_filter', true)->load();

          // foreach ($collection as $_product) {
          //   $product = $objectManager->create('Magento\Catalog\Model\Product')->load($_product->getId());  
            
          //   if($product->getTypeId() == 'configurable' ){
          //     $isInStock = $stockRepo->get($product->getId())->getIsInStock();
          //     $_children = $product->getTypeInstance()->getUsedProducts($product);
          //     $stock = 0;
          //     foreach ($_children as $child){
          //       //echo "Here are your child Product Ids ".$child->getID() . " | " . $StockState->getStockQty($child->getId(), $child->getStore()->getWebsiteId());;
          //       $stock += $StockState->getStockQty($child->getId(), $child->getStore()->getWebsiteId());
          //     }
          //     if ($stock == 0 && $isInStock == true){
          //       try {
          //         $_product->setUrlKey($product->getUrlKey());
          //         $_product->setStockData(['qty' => 0, 'is_in_stock' => false]);
          //         $_product->save();
          //       } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
          //         // echo $e;
          //         //echo "set out of stock " . $product->getUrlKey() . " | " . $product->getName() . " | " . $product->getSku() . " | " . $isInStock;
          //         //echo PHP_EOL;  
          //       }
          //     }
          //     if($stock > 0 && $isInStock == false){
          //       try {
          //         $_product->setUrlKey($product->getUrlKey());
          //         $_product->setStockData(['qty' => 0, 'is_in_stock' => true]);
          //         $_product->save();
          //       } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
          //         // echo $e;
          //         //echo "set out of stock " . $product->getUrlKey() . " | " . $product->getName() . " | " . $product->getSku() . " | " . $isInStock;
          //         //echo PHP_EOL;  
          //       }
          //     }
          //   }
          // }
          

          $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/stock.log", "a");
          fwrite($myfile, date("d-m-Y h:i:s") .' stock kørt'. "\r\n");
          fclose($myfile);
         //   $this->reindex();
    }
}
