var config = {
    paths: {
        slick: 'slick/slick',
        carouFredSel: 'fred/carouFredSel',
        jqueryui: 'jqueryui/jqueryui'
    },
    shim: {
     slick: {
         deps: ['jquery']
     },
     carouFredSel: {
         deps: ['jquery']
     },
     jqueryui: {
         deps: ['jquery']
     },
   }
};
