require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
  $ = jQuery;
  var supportsTouch = false;
  var itemsWidth = 0;
 

  function checkForScreenSize(){
    if($(window).width() > 767){
      return 'desktop';
    }
    return 'mobile';
  }

  function loginFormShow(){
    $('.customer-account-login .column.main, .customer-account-create .column.main').css('min-height', $(window).height());
    $('.showLogin').unbind().click(function(){
      $('.block-new-customer').hide();
      $('.block-customer-login').addClass('active');
    });
  }

  function showLogin(){
    $('.customer-account-login, .customer-account-create').css('opacity', 1);
  }

  $(window).scroll(function(){
  });

  jQuery(document).ready(function(){
    supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
    loginFormShow();
    showLogin();
  });

  jQuery(window).resize(function(){
    loginFormShow();
  });


});
