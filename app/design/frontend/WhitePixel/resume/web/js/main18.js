require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
  $ = jQuery;
  var supportsTouch = false;
  var itemsWidth = 0;
  var selected = false;
  var el;
  function mobileToggle(){
    $('.mobileToggle').click(function(){
      $('.headerContent').addClass('active').delay(2).queue(function(){
        $('header').addClass('active');
        $('header').addClass('showhead');
        setTimeout(
        function() 
        {
          $('body').css('top', -$(window).scrollTop());
          $('body').addClass('scroll-lock');
          $('.headerContent').dequeue();
        }, 600);
        
      });
      // $('#maincontent').addClass('overflowHidden');
      // $('.headerContent').addClass('active');
      // $('header').toggleClass('active');
      // $('header .bgoverlay').addClass('active');
    });
    $('.closeMenu').click(function(){
      $('header').removeClass('active');
      $('header').removeClass('showhead');
      $('.headerContent').dequeue();
      $('body').removeClass('scroll-lock');
      var top = $('body').css('top');
      $(window).scrollTop(-1*(top.replace("px","")));
      $('body').css('top', '');

    });    
    $('.goBack').click(function(){
      $(this).parent().parent().removeClass('active');
    });
  }

  function coverImg(){
    var windowH = $(window).height();
    var pageHeader = $(window).height();
    var minHeight = windowH - pageHeader;
    $("img.cover").each(function (){
      $(this).css("max-width", "none");
      var containerH = $('.imageItem').height();
      var containerW = $('.imageItem').width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log(counter + ": " + (imageXY > containerXY));
      // console.log(counter + ": " + (imageXY < containerXY));

      if(imageXY >= containerXY){
        $(this).height(containerH - 150);
        $(this).width('auto');
  //       $(this).height(containerH);
  //       $(this).width('auto');
        var wWidth = containerW;
        var iWidth = $(this).width();
  //       $(this).css("marginLeft", (wWidth-iWidth)/2 );
  //       $(this).css("marginTop", 75 );
      }
      if(imageXY < containerXY){
  //       $(this).width($(window).width());
  //       $(this).height(containerW / imageXY);
        $(this).width('auto');
        $(this).height(containerH - 200);
        var wHeight = $(this).width();
        var iHeight = containerH - 200;
  //       $(this).css("marginTop", 100 );
  //       $(this).css("marginLeft", (containerW-wHeight)/2 );
      };
    });
  }
  function coverImgFullWidth(){
    $("img.cover-full-width").each(function (){
      $(this).css("max-width", "none");
      var containerH = $('.imageContainer').parent().height();
      var containerW = $('.imageContainer').parent().width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log("containerXY " + containerXY);
      // console.log("imageXY " + imageXY);

      if(imageXY >= containerXY){
        $(this).height(containerH);
        $(this).width('auto');
        var wWidth = $(this).width();;
        $(this).css("marginTop", 0 );
        $(this).css("marginLeft", (containerW-wWidth)/2 );
      }
      if(imageXY < containerXY){
        $(this).height('auto');
        $(this).width(containerW);
        var wHeight = $(this).height();
    //     $(this).css("marginTop", (containerH-wHeight)/2 );
        $(this).css("marginLeft", 0 );

      };
    });
  }

  function coverImgFullWidthFront(){
    $("img.cover-front").each(function (){
      $(this).css("max-width", "none");
      var containerH = $(this).parent().parent().height();
      var containerW = $(this).parent().parent().width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log("containerXY " + containerXY);
      // console.log("imageXY " + imageXY);

      if(imageXY >= containerXY){
        $(this).height(containerH);
        $(this).width('auto');
        var wWidth = $(this).width();
        $(this).css("marginTop", 0);
        $(this).css("marginLeft", (containerW-wWidth)/2 );
      }
      if(imageXY < containerXY){
        $(this).height('auto');
        $(this).width(containerW);
        var wHeight = $(this).height();
        $(this).css("marginTop", (containerH - wHeight));
        $(this).css("marginLeft", 0 );

      };
    });
  }

  function coverImgNewsletter(){
    $("img.cover-newsletter").each(function (){
      $(this).css("max-width", "none");
      var containerH = $(this).parent().parent().height();
      var containerW = $(this).parent().parent().width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log("containerXY " + containerXY);
      // console.log("imageXY " + imageXY);

      if(imageXY >= containerXY){
        $(this).height(containerH);
        $(this).width('auto');
        var wWidth = $(this).width();;
        $(this).css("marginTop", 0 );
        $(this).css("marginLeft", (containerW-wWidth)/2 );
      }
      if(imageXY < containerXY){
        $(this).height('auto');
        $(this).width(containerW);
        var wHeight = $(this).height();
    //     $(this).css("marginTop", (containerH-wHeight)/2 );
        $(this).css("marginLeft", 0 );

      };
    });
  }

  function openSearch(){
    $('.block-search .block-title').click(function(){
      $('body').css('overflow', 'hidden');
      $('.block-search').toggleClass('active');
      $('.bgoverlay').toggleClass('active');
      $('.block-search #search').focus();
      $('.closeSearch').addClass('active');
    });

    $('.closeSearch').click(function(){
      $('.block-search').removeClass('active');
      $('.closeSearch').removeClass('active');
      $('.bgoverlay').removeClass('active');
      $('body').css('overflow', 'auto');
    });
  }

  function showSwatches(){
    $('.swatch-attribute-label, .swatch-attribute-selected-option').unbind().click(function(){
      $('.product-add-form').addClass('active');
      $('.swatch-attribute-label').addClass('active');
    });
  }

  function addToCartFromSinglePage(){
    // if(getTotalsInCart() > 1){
    //   $('.navRight').addClass('active');
    // }
    // else{
    //   $('.navRight').removeClass('active'); 
    // }
    $('.product-info-main .swatch-option').unbind().click(function(){
      $('.product-add-form').removeClass('active');
      $('.swatch-attribute-selected-option').addClass('active');
      // $('[data-block="minicart"]').unbind().on('contentUpdated', function ()  {
      $('[data-block="minicart"]').on('contentUpdated', function ()  {
        setCartHeight();
        // changeTotalItemsInCart();
        $('.block-minicart').addClass('active');
        $('.bgoverlay').addClass('active');
        // minicartItems();
        // cloneCartItems();
      });
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.block-search').removeClass('active');
        $('.bgoverlay').removeClass('active');

      });
      if($(this).hasClass('selected')){
        $('.swatch-attribute-selected-option.active').html($(this).html());
        event.stopImmediatePropagation();
      }
    });
    $('.product-item-details .action.delete').unbind().click(function(){
      // $('[data-block="minicart"]').unbind().on('contentUpdated', function ()  {
      $('[data-block="minicart"]').on('contentUpdated', function ()  {
        // changeTotalItemsInCart();
        setCartHeight();
        $('.block-minicart').addClass('active');
        $('.bgoverlay').addClass('active');
        // minicartItems();
        // cloneCartItems();
      });
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.block-search').removeClass('active');
        $('.bgoverlay').removeClass('active');
      });
    });
    $('.box-tocart').unbind().click(function(){
      $('[data-block="minicart"]').on('contentUpdated', function ()  {
        setCartHeight();
        // changeTotalItemsInCart();
        $('.block-minicart').addClass('active');
        $('.bgoverlay').addClass('active');
        // minicartItems();
        // cloneCartItems();
      });
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.block-search').removeClass('active');
        $('.bgoverlay').removeClass('active');
      });
    });
  }
  
  // function addToCartFromList(){
  //   $('.products.list .item .swatch-option').unbind().click(function(){
  //     // $(this).parent().parent().parent().parent().find('form').submit();
  //     var parent = $(this).parent().parent().parent().parent(); 
  //     parent.find('.super-attribute-select').val($(this).attr('option-id'));
  //     parent.find('form').submit();

  //     // $('[data-block="minicart"]').unbind().on('contentUpdated', function ()  {
  //     $('[data-block="minicart"]').on('contentUpdated', function ()  {
  //       changeTotalItemsInCart();
  //       $('.block-minicart').addClass('active');
  //       $('.bgoverlay').addClass('active');
  //       minicartItems();
  //       cloneCartItems();
  //     });
  //     $('.bgoverlay, #btn-minicart-close').click(function(){
  //       $('.block-minicart').removeClass('active');
  //       $('.bgoverlay').removeClass('active');
  //     });
  //   });
  // }
  function addZoom(){
      var zoomButtonHtmlCart = '<div class="nextSlick closeSlickCart zoomButton" style="transition: left 200ms ease-out, top 200ms ease-out; -webkit-transition: left 200ms ease-out, top 200ms ease-out; left: 1305px; top: 545px;"><div id="closeZoom" style="/* display: block; *//* position: fixed !important; *//* z-index: 99; */"><span class="">CLOSE</span></div></div>';
      $('body').append(zoomButtonHtmlCart);
      $('.closeSlickCart').css('z-index', '99');
      $('#closeZoom').show();
  }
  function setCartHeight(){
    if(checkForScreenSize() == 'mobile'){
      //$('.minicart-items-wrapper').height($(window).height() - 30 - 60 - 165);
    }
    else{
      $('.minicart-items-wrapper').height($(window).height() - 50 - 30 - 200);
    }
  }
  
  function openCustomCart(){

    setCartHeight();
    $('header a.showcart, header .openCartLink').unbind().click(function(){
      // if(getTotalsInCart() > 1){
      //   $('.navRight').addClass('active');
      // }
      // else{
      //  $('.navRight').removeClass('active'); 
      // }
      //addZoom();
      //var zoomButtonHtmlCart = '<div class="nextSlick closeSlickCart zoomButton" style="transition: left 200ms ease-out, top 200ms ease-out; -webkit-transition: left 200ms ease-out, top 200ms ease-out; left: 1305px; top: 545px;"><div id="closeZoom" style="/* display: block; *//* position: fixed !important; *//* z-index: 99; */"><span class="">CLOSE</span></div></div>';
      event.preventDefault();
      // minicartItems();
      // changeTotalItemsInCart();
      // $('body').append(zoomButtonHtmlCart);
      // $('.closeSlickCart').css('z-index', '99');
      $('.block-minicart').addClass('active');
      $('.bgoverlay').addClass('active');
      // $('#closeZoom').show();

      $('.openCartLink').removeClass('active');
      $('body').css('overflow', 'hidden');
      // cloneCartItems();
      $('.bgoverlay, #btn-minicart-close').click(function(){
        // $('.zoomButton').remove();
        $('.block-minicart').removeClass('active');
        $('.bgoverlay').removeClass('active');
        $('#closeZoom').hide();
        $('body').css('overflow', 'auto');
      });
    });

  }


  function minicartItems(){
    var width = $('header .cartItemNavigation').width();
    if(width != itemsWidth){
      $('header .cartItemNavigation style').remove();
      $('header .cartItemNavigation').append('<style>.minicart-items-wrapper ol li.item{width: '+width+'px !important}</style>');  
    }
    // $('.cartItemNavigation').append('<style>.minicart-items-wrapper ol li.item{width: '+width+'px !important}</style>');
  }


  function cartItemSize(){
    $('.cart.table-wrapper .cart.item').width($('.cart-container .cartItemNavigation').width());
  }

  function cartPage(){
    $('.cart-container .itemCart .totalItem').text(pad(parseInt($('.cart.table-wrapper .cart.item').length), 2));
    if($('.cart.table-wrapper .cart.item').length > 1){
      $('.cart-container .navRight').addClass('active');
    }
    $('.cart-container .navLeft').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.cart-container .itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('.cart-container .cartItemNavigation').width() + 60;
      }
      else{
        var left = $('.cart-container .cartItemNavigation').width() + 30; 
      }
      $(".cart.table-wrapper").animate({
        left: "+="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cart-container .cartItemNavigation .itemCart .currentItem').text()) - 1 );
        $('.cart-container .navRight, .cart-container .navLeft').addClass('active');
        if(number == 1){
          $('.cart-container .navLeft').removeClass('active');
        }
        $('.cart-container .cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });

    $('.cart-container .navRight').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.cart-container .itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('.cart-container .cartItemNavigation').width() + 60;
      }
      else{
        var left = $('.cart-container .cartItemNavigation').width() + 30; 
      }
      $(".cart.table-wrapper").animate({
        left: "-="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cart-container .cartItemNavigation .itemCart .currentItem').text()) + 1 );
        $('.cart-container .navRight, .cart-container .navLeft').addClass('active');
        if(number === totalItem){
          $('.cart-container .navRight').removeClass('active');
        }
        $('.cart-container .cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });
  }

  function cloneCartItems(){
    minicartItems();
    $('.navLeft').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('#minicart-content-wrapper').width() + 60;
      }
      else{
        var left = $('#minicart-content-wrapper').width() + 30; 
      }
      $(".minicart-items-wrapper ol").animate({
        left: "+="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cartItemNavigation .itemCart .currentItem').text()) - 1 );
        $('.navRight, .navLeft').addClass('active');
        if(number == 1){
          $('.navLeft').removeClass('active');
        }
        $('.cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });

    $('.navRight').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('#minicart-content-wrapper').width() + 60;
      }
      else{
        var left = $('#minicart-content-wrapper').width() + 30; 
      }
      $(".minicart-items-wrapper ol").animate({
        left: "-="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cartItemNavigation .itemCart .currentItem').text()) + 1 );
        $('.navRight, .navLeft').addClass('active');
        if(number === totalItem){
          $('.navRight').removeClass('active');
        }
        $('.cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });
    minicartItems();
    
    $('.cartDots').remove();
    var dots = '<div class="cartDots">';
    var i = 1;
    while (i <= getTotalsInCart()) {
      if(i == 1){
        dots += '<span class="innerDot active"></span>';
      }
      else{
        dots += '<span class="innerDot"></span>';
      }
      
      i++;
    }
    dots += '</div>';
    $('.minicart-items-wrapper .cartDots').remove();
    $('.minicart-items-wrapper').prepend(dots);
    cartDotClick();

    if(getTotalsInCart() > 1){
      $('.navRight').addClass('active');
    }
  }
  function cartDotClick(){
    if($(window).width() <= 767){
      $(".minicart-items-wrapper ol").css('left', 0);
    }
    $('.innerDot').click(function(){
      var move = $('#minicart-content-wrapper').width() + 30;
      var activeDot = $('.innerDot.active').index() + 1;
      var newDot = $(this).index() + 1;
      $('.innerDot').removeClass('active');
      $(this).addClass('active');

      if(activeDot >= newDot){
        if(activeDot == newDot){
          return;
        }
        var moveTo = ((activeDot - newDot)*move);

        $(".minicart-items-wrapper ol").animate({
          left: "+="+moveTo,
        }, 500, function() {
          // Animation complete.
          var number = (parseInt(newDot));
          $('.cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
        });
      }
      else{
        var moveTo = ((newDot - activeDot)*move);
        $(".minicart-items-wrapper ol").animate({
          left: "-="+moveTo,
        }, 500, function() {
          // Animation complete.
          var number = (parseInt(newDot));
          $('.cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
        });
      }
      

    });
  }
  function getTotalsInCart(){
    return $('.minicart-items-wrapper ol li.item').length;
  }

  function changeTotalItemsInCart(){
    $('header .itemCart .totalItem').text(pad(parseInt($('header .itemCart .totalItem').text()),2));
    $('header .itemCart .totalItem').unbind();

    $(document).on("change", ".totalItem", function () {
      $('header .itemCart .totalItem').text(pad(parseInt($('header .itemCart .totalItem').text()),2));
    });
  }

  function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }

  var lastScrollTop = 0;

  function headerVisible(){
    var st = $(this).scrollTop();
    // if (st > lastScrollTop){
    //   $('header').css('top', - $("header").outerHeight());
    // } 
    // else {
    //   $('header').css('top', 0);
    // }
    if(!$('.page-header').hasClass('showhead')){
      if (st > lastScrollTop && st > 83 ){
        // $('.mobileToggle').css('top', - 80);
        $('header, .headerContent, .gridChange').addClass('up');
      } 
      else {
        if(checkForScreenSize() == 'mobile'){
          // $('.mobileToggle').css('top', 30);
          $('header, .categoriesMenuToggle, .gridChange').removeClass('up');
        }
        else{
          // $('.mobileToggle').css('top', 50);
          $('header, .categoriesMenuToggle, .gridChange').removeClass('up');
        }
      }
      lastScrollTop = st;
    }

    if(st > 50){
        $(".arrow").fadeOut();
    }
  }

  

  function productPageHeight(){
    if(checkForScreenSize() == 'mobile'){
      $('.product-info-main').height($(window).height() - 70);
      return;
    }
    $('.product-info-main, .productDetails, .paralaxElement').height($(window).height());
  }

   function checkForScreenTouhchSIze(){
    if($(window).width() > 768){
      return 'desktop';
    }
    return 'mobile';
  }

  function checkForScreenSize(){
    if($(window).width() > 767){
      return 'desktop';
    }
    return 'mobile';
  }

  function checkForMobilePage(){
    if(checkForScreenTouhchSIze() == 'mobile'){
      if($('.product-add-form').length > 0){
        $('.product-add-form').appendTo('.productDetails');
      }
      if($('.relatedContainer').length > 0){
        var slickOptions = {
          draggable: true,
          infinite: true,
          variableWidth: true,
        }
        $('.relatedContainer').slick(slickOptions);
      }
    }
  }

  function mediaGallery(){
    var slickOptions = {
      draggable: false,
      dots: true
    }

    // Run on first slider load
    $('#imageGallery').on('init', function(event, slick) {
      $('#imageGallery').css('opacity', 1);
      $('.column.main').addClass('active');
    });
    // Init slider
    var slider = $('#imageGallery').slick(slickOptions);
  }

  // REMOVED 5.12.19
  // function productPageImagePagination(){
  //   $('.imageGallery, .nextSlick').click(function(){
  //     console.log('asdasd');
  //     $('#imageGallery').slick('slickNext');
  //     var count = parseInt($('.paginationText').text()); 
  //     var last = parseInt($('.lastPagination').text());
  //     count += 1;
  //     if(count > last){
  //       count = 1;
  //     }
  //     $('.paginationText').text(pad(count,2));
  //   });
  // }


  function loginFormShow(){
    $('.customer-account-login .column.main, .customer-account-create .column.main').css('min-height', $(window).height());
    $('.showLogin').unbind().click(function(){
      $('.block-new-customer').hide();
      $('.block-customer-login').addClass('active');
    });
  }

  function categoryMenu(){
    $('.categoriesMenuToggle, .categoryOverlay').click(function(){
      $('.categoryOverlay, .categoriesMenu, .categoryOverlay').toggleClass('active');
      $('.categoriesMenuToggle').toggleClass('importantStyle');
      $('header, body').toggleClass('hide');
      $('#maincontent').toggleClass('cat-shown');
      $('footer').toggleClass('cat-shown');
      $('.products.wrapper').toggleClass('blurred');
    });
  }


  function blurOnScroll(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    var blur = ($(this).scrollTop()/$(window).height())*10 > 10 ? 10 : ($(this).scrollTop()/$(window).height())*10; //(($(this).scrollTop()*0.01) > 10 ? 10 : ($(this).scrollTop()*0.01));
    var opacity = 1-((blur/10)*1);
    var opacityMedia = 1-((blur/10)*0.6);


    $('.product.media').css({'filter': 'blur('+blur+'px)', 'opacity': opacityMedia});
    $('.nextSlick, .catalog-product-view .page-wrapper .product-info-main .product-info-price, .catalog-product-view .page-wrapper .product-info-main .product-add-form').css({'filter': 'blur('+blur+'px)', 'opacity': opacity});
    $('.nextSlick.closeSlick').css({'filter': 'blur(0px)', 'opacity': '1'});
    if($(this).scrollTop() >= $(window).height()) {
      $('.product-add-form, .product-info-price').css('opacity', '0');
      // $('.backgroundOverlayMain').addClass('clickBlock');
    }
    else if ($(this).scrollTop() <= $(window).height()) {
      // $('.backgroundOverlayMain').removeClass('clickBlock');
    }

    if($(this).scrollTop() > 20) {
      $('.product-add-form, .product-info-price').css('z-index', '0');

    } else if ($(this).scrollTop() < 20){
        $('.product-add-form, .product-info-price').css('z-index', '9');
       
    }
 
  }

  function fadeOnScroll(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    var blur = ($(this).scrollTop()/$(window).height())*10 > 10 ? 10 : ($(this).scrollTop()/$(window).height())*10; //(($(this).scrollTop()*0.01) > 10 ? 10 : ($(this).scrollTop()*0.01));
    var opacity = 1-((blur/10)*1);
    var opacityMedia = 1-((blur/10)*0.6);


    $('.product.media').css('opacity', opacityMedia);
    $('.nextSlick').css('opacity', opacity);
    $('.nextSlick.closeSlick').css('opacity', '1');
    // if($(this).scrollTop() >= $(window).height()) {
    //   $('.product-add-form, .product-info-price').css('opacity', '0');
    //   // $('.backgroundOverlayMain').addClass('clickBlock');
    // }
    // else if ($(this).scrollTop() <= $(window).height()) {
    //   // $('.backgroundOverlayMain').removeClass('clickBlock');
    // }

    // if($(this).scrollTop() > 20) {
    //   $('.product-add-form, .product-info-price').css('z-index', '0');

    // } else if ($(this).scrollTop() < 20){
    //     $('.product-add-form, .product-info-price').css('z-index', '9');
       
    // }
 
  }
  
  $(window).scroll(function(){

    headerVisible();
    fadeOnScroll();
    // blurOnScroll();
  
  });

  // $(document).on('mousemove', function(e){
  //   if($(window).width() <= 767){
  //     return;
  //   }

  //   var x = e.pageX - 35;
  //   var y = e.pageY - 15 - $(document).scrollTop();
    
  //   if (y < 85 || $(document).scrollTop > 0 ) {
  //     $('.nextSlick').fadeOut();
  //   }

  //   if(y > ($(window).height() - 200)){
  //     $('.nextSlick').fadeOut();
  //   }
  //   else{
  //     $('.nextSlick').fadeIn();
  //   }
    
  //   $('.nextSlick').css({
  //      left:  x-25,
  //      top:   y+25
  //   });

  //   $('.closeSlick').css({
  //      left:  x-25,
  //      top:   y+25
  //   });
  
  // });

   $(document).on('mousemove', function(e){
   if($(window).width() < 767){
     return;
   }
   var x = e.pageX - 35;
   var y = e.pageY - 15 - $(document).scrollTop();

   if(y < 85 ){
     // y = 85;
    
    if($('body.zoomedImage').length == 0){
    // y = $(window).height() - 140 - $(document).scrollTop();
        $('.nextSlick').fadeOut();
    }
   }
   else if(y > ($(window).height() - 150) ){
    if($('body.zoomedImage').length == 0){
     // y = $(window).height() - 140 - $(document).scrollTop();
         $('.nextSlick').fadeOut();
    }
   }
   else{
     $('.nextSlick').fadeIn();
   }


   $('.nextSlick').css({
      left:  x-25,
      top:   y+25
   });

     $('.closeSlick').css({
       left:  x-25,
       top:   y+25
    });

 });

  function closeMenu(){
    $('.bgoverlay').click(function(){
      $('.block-search').removeClass('active');
      $('.bgoverlay').removeClass('active');
      $('body').css('overflow', 'auto');
      // $('header').removeClass('active show');
      // $(this).removeClass('active');
      // $('.menuContainer .anchor').removeClass('active');
      // $('.menuContainer').delay(400).height(50);

      // $('header').addClass('showhead');
      //   el.addClass('active').delay(200).queue(function(){
      //     el.addClass('show').dequeue();
      // });


    });
    $('.closeMenu').click(function(){
      $('header').removeClass('active show');
      // $('.bgoverlay').removeClass('active');
      // $('.menuContainer .anchor').removeClass('active');
      // $('.menuContainer').height(50);
    });
  }
  function getXY(evt, element) {
      var rect = element.getBoundingClientRect();
      var scrollTop = document.documentElement.scrollTop?
                      document.documentElement.scrollTop:document.body.scrollTop;
      var scrollLeft = document.documentElement.scrollLeft?                   
                      document.documentElement.scrollLeft:document.body.scrollLeft;
      var elementLeft = rect.left+scrollLeft;  
      var elementTop = rect.top+scrollTop;

      x = evt.pageX-elementLeft;
      y = evt.pageY-elementTop;

      return {x:x, y:y};
  }
  function productHoverFollow(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    if(supportsTouch == true){
      return;
    }

    jQuery('.product-item .productListOverlay').mousemove(function(e){
        var m=getXY(e, this);
        // if(jQuery('.page-header').hasClass('up')){
        //   if(e.pageY < 110){
        //     jQuery(this).css('opacity', 0);
        //   }else{
        //    jQuery(this).removeAttr('style');
        //   }  
        // }else{
        //   if(e.pageY-pageYOffset < 110){
        //     jQuery(this).css('opacity', 0);
        //   }else{
        //    jQuery(this).removeAttr('style');
        //   }  
        // }
        
        if(m.y < jQuery('.product-item .productListOverlay').height() - ((jQuery(this).find('.product-item-name').height() + jQuery(this).find('.priceContainer').height())/2) ){
          // if((m.y-jQuery(this).find('.priceContainer').height() + 5) < 0){
          //   jQuery(this).find('.product-item-name').css('top', 0); 
          //   jQuery(this).find('.priceContainer').css('top', 0 + jQuery(this).find('.product-item-name').height() + 5);
          // }
          // else{
          //   jQuery(this).find('.product-item-name').css('top', m.y - jQuery(this).find('.product-item-name').height() - 5);
          //   jQuery(this).find('.priceContainer').css('top', m.y);
          // }
            // jQuery(this).find('.product-item-name').css('top', (jQuery(this).height() / 2) -  (jQuery(this).find('.product-item-name').height() - 5)); 
            // jQuery(this).find('.priceContainer').css('top', jQuery(this).height() / 2);


        }
    });

    //jQuery('.relatedProducts .relatedContainer .relatedProduct').mousemove(function(e){
    //    var m=getXY(e, this);
    //    if(m.y < jQuery('.relatedProducts .relatedContainer .relatedProduct').height() - (jQuery(this).find('.inforel').height()/2)){
    //        // jQuery(this).find('.inforel').css('top', (m.y-(jQuery(this).find('.inforel').height()/2)) < 0 ? 0 : (m.y-(jQuery(this).find('.inforel').height()/2)) );
    //        jQuery(this).find('.inforel').css('top', (jQuery(this).height() / 2) - (jQuery(this).find('.inforel').height()/2)) ;
    //    }
    //});
  }


  function menuHover(){
    return;
    if(supportsTouch == true){
      if($(window).width() < 769){
        $('.menuContainer .anchor').click(function(){
          if($(this).hasClass('direct') != true){
            if($(this).hasClass('active') != true){
              event.preventDefault();
            }
            $('.menuContainer .anchor.active').removeClass('active');  
            $(this).addClass('active');  
          }
        });
        return;
      }
      else{
        $('.menuContainer .anchor').click(function(){
          if($(this).hasClass('direct') != true){
            if($(this).hasClass('active') != true){
              event.preventDefault();
            }
            $(this).addClass('active');  
            $('.menuContainer .anchor').removeClass('active full');
            $('.menuContainer').height(50);
            $(this).addClass('active');
            if($(this).find('ul').length > 0){
              var height = $(this).find('ul').outerHeight();
            }
            else{
              var height = 0; 
            }
            $('.menuContainer').height(height + 50);
          }
        });
        return;
      }
    }

    $('.menuContainer .anchor').unbind().mouseover(function(){
      if($(this).hasClass('active')){
        return;
      }

      $('.menuContainer .anchor').removeClass('active full');
      $('.menuContainer').height(50);
      $(this).addClass('active');
      if($(this).find('ul').length > 0){
        var height = $(this).find('ul').outerHeight();
      }
      else{
        var height = 0; 
      }
      $('.menuContainer').height(height + 50);
    });
  }


  function relatedProductSlider(){

    $('.catalog-product-view .relatedContainer .relatedProduct').hover(function(){
      // var pos = parseInt($('.relatedContainer').css('transform').split(',')[4]);
      $('.catalog-product-view .relatedContainer').toggleClass('pause');
      // $('.catalog-product-view .relatedContainer').css('transform', 'translate3d('+pos+'px, 0, 0)');
      // $('.catalog-product-view .relatedContainer').css('animation-play-state', 'paused');
      // $('.catalog-product-view .relatedContainer').css('animation-play-state', 'running');
    })
    // $('.catalog-product-view .relatedContainer').slick({  
    //   dots: false,
    //   infinite: true,
    //   slidesToShow: 5,
    //   slidesToScroll: 1,
    //   centerMode: false,
    //   centerPadding: '60px',
    //   // responsive: [
    //   //   {
    //   //     breakpoint: 1700,
    //   //     settings: {
    //   //       slidesToShow: 5
    //   //     }
    //   //   },
    //   //   {
    //   //     breakpoint: 1500,
    //   //     settings: {
    //   //       slidesToShow: 4
    //   //     }
    //   //   },
    //   //   {
    //   //     breakpoint: 1200,
    //   //     settings: {
    //   //       slidesToShow: 3
    //   //     }
    //   //   }
    //   // ]
    // })
    //if(checkForScreenSize() == 'mobile'){
    //  $('.relatedProducts .relatedContainer .relatedProduct').width(($(window).width()/1.5));
    //}
    //else{
    //  $('.relatedProducts .relatedContainer .relatedProduct').width(($(window).width()/4));
    //}
//
    //var $c = $('.relatedProducts .relatedContainer'),
    //  $w = $(window);
   //
    //$c.carouFredSel({
    //  align: false,
    //  items: 'variable',
    //  scroll: {
    //    items: 1,
    //    duration: 5000,
    //    timeoutDuration: 0,
    //    easing: 'linear',
    //    pauseOnHover: 'immediate'
    //  }
    //});
   //
    //
    //$w.bind('resize.example', function() {
    //  var nw = $w.width();
    //  if (nw < 990) {
    //    nw = 990;
    //  }
   //
    //  $c.width(nw * 3);
    //  $c.parent().width(nw);
   //
    //}).trigger('resize.example');
  }

  function listClicked(){
    if(supportsTouch == true){
      $('ol.product-items li.product-item a.product-item-photo').click(function(){
        if($(this).parent().parent().hasClass('pressed') == true){
          $('ol.product-items li.product-item').removeClass('pressed');
          event.preventDefault();
          return;
        }
        event.preventDefault();
        $('ol.product-items li.product-item').removeClass('pressed');
        $(this).parent().parent().addClass('pressed');
        
      });

      // $('ol.product-items li.product-item a').click(function(){
      //   if($(this).parent().parent().parent().hasClass('pressed') == true){
      //     $('ol.product-items li.product-item').removeClass('pressed');
      //     event.preventDefault();
      //     return;
      //   }
      //   event.preventDefault();
      //   $('ol.product-items li.product-item').removeClass('pressed');
      //   $(this).parent().parent().parent().addClass('pressed');
        
      // });
    }
  }

  function addHoverClass() {

  }

  function hoverList() {

    
      // if($(window).width() <= 768) {
      //   $('ol.product-items li.product-item').removeClass('pressed');

      //   $('ol.product-items li.product-item').hover(function() {
      //     $(this).toggleClass('pressed');
      //   }) 

      // }

    

  }

  function relatedClick(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    $('.relatedProducts .relatedProduct').click(function(){
      if($(this).hasClass('pressed') == true){
        return;
      }
      if(supportsTouch == true){
        event.preventDefault();
        $('.relatedProducts .relatedProduct').removeClass('pressed');
        $(this).addClass('pressed');
      }
    });
  }

  function changeProductGrid(){
    var xy = 1.5003646973;
    $('.gridChange .multiple').click(function(){
      var currentScrollTop = $(window).scrollTop();
      $('.products.list.product-items').addClass('hideElement').delay(400).queue(function(){
        $('.products.list.product-items').removeClass('hideElement').dequeue();
      });
      $('.gridChange .single, .gridChange .multiple').toggleClass('active');
      $('.products-grid ol.product-items').toggleClass('single');
      $(window).scrollTop(currentScrollTop/3);
    });
    $('.gridChange .single').click(function(){
      var currentScrollTop = $(window).scrollTop();
      $('.products.list.product-items').addClass('hideElement').delay(400).queue(function(){
        $('.products.list.product-items').removeClass('hideElement').dequeue();
      });
      $('.gridChange .single, .gridChange .multiple').toggleClass('active');
      $('.products-grid ol.product-items').toggleClass('single');
      $(window).scrollTop(currentScrollTop*3);
    });
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function checkCookie() {
    var first = getCookie("firstvisit");
    if (first != "") {
    } else {
      setCookie("firstvisit", "firstvisit", 186);
      console.log('redirect to Location page');
      window.location.replace("https://www.resumecph.dk/location");
    }
  }



  function secondClickMenuItems(){
    $('header .anchor>.block-category-link>a, header .anchor>.block-cms-link>a').click(function(){
      var el = $(this).parent().parent();
      if(el.hasClass('active') != true){
        $('header nav li').removeClass('active show');
        if(el.find('ul').length > 0){
          event.preventDefault();
          $('header').addClass('showhead');
          if($('header nav').hasClass("open") == true){
              el.addClass('active').delay(200).queue(function(){
                el.addClass('show').dequeue();
            });
          }
          else{
            el.addClass('active').delay(600).queue(function(){
                el.addClass('show').dequeue();
                $('header nav').addClass("open").dequeue();
            });
          }
          // $('header nav').addClass('open');
        }
      }
    });
  }

  function menuOverlay(){
    $('.bgoverlaymenu').click(function(){
      $('.bgoverlaymenu').css('opacity', 0);
      $('header nav li').removeClass('show').delay(400).queue(function(){
        $('header nav li').dequeue();
        $('header nav').removeClass("open");
        $('header nav li').removeClass('active');
        $('header').removeClass('showhead');
        $('.bgoverlaymenu').removeAttr('style');
      });
    });
  }

  function hover() {
    $('.imageGallery .slick-slide img').on("mouseenter", function() {
      $('#pagination').hide();
      $("#zoom").show(); 
    }).on("mouseleave", function() {
      $('#pagination').show();
      $("#zoom").hide(); 
    });

    $('.imageGallery .slick-prev').on("mouseenter", function() {
      $('.slick-prev').css('display', 'block');
      $('.paginationTextRight').hide();
      $('.paginationTextLeft').show();

    }).on("mouseleave", function() {
      // $('.slick-prev').hide();
      // $('.paginationTextLeft').hide();
 
    });

    $('.imageGallery .slick-next').on("mouseenter", function() {
      $('.paginationTextLeft').hide();
      $('.slick-next').css('display', 'block');
      $('.paginationTextRight').show();

    }).on("mouseleave", function() {
      $('slick-next').hide();
      $('.paginationTextRight').hide();
    });
  }

  function zooming() {
    
    var imageSizeUrl;

    $('.imageGallery .slick-slide img').on('click', function() {

      $('.imageZoomContainer').addClass('enlarge');
      $('body').addClass('zoomedImage');
      imageSizeUrl = $(this).parent().data('big-url'); 
      var xy = 1.5003646973;
      var multiplierForMobile = 1;
      if ($(window).width() < 769 ) {
        multiplierForMobile = 2;
      }
      
      // $('.imageZoomContainer img').attr('src', imageSizeUrl);
      var zoomHtml = '<div class="imageZoomContainer"><div class="innerZoom" id="innerZooomz"><img style="width:100%; height:'+($(window).width()*xy*multiplierForMobile)+'px" src="'+imageSizeUrl+'" alt=""></div></div>';
      // var zoomHtmlMobile = '<div class="imageZoomContainer"><div class="innerZoom" id="innerZooomz"><img style="width:100%; height:'+($(window).width()*xy*multiplierForMobile)+'px" src="'+imageSizeUrl+'" alt=""></div></div>';
      var zoomButtonHtml = '<div class="nextSlick closeSlick zoomButton" style="pointer-events: none; left: '+$('.nextSlick').css('left')+'; top: '+$('.nextSlick').css('top')+';"><div id="closeZoom" style="/* display: block; *//* position: fixed !important; *//* z-index: 9999999999999999; */"><span class="">CLOSE</span></div></div>';
      $('body').append(zoomHtml);
      $('body').append(zoomButtonHtml);
      if ($(window).width() < 769 ) {
        $('.imageZoomContainer').css('width', '200%');
      }
      $('.imageZoomContainer').on('click', function() {
        $('.imageZoomContainer').remove();
        $('.zoomButton').remove();
        $('#closeZoom').hide();
        // $('#zoom').show();
        $(window).scrollTop(0);
        $('body').removeClass('zoomedImage');
        $('body').removeAttr('style');
        $('.page-wrapper').removeAttr('style');
      });
      $('#closeZoom').show();
      
      $('#zoom').hide();
      $('body').css('padding-bottom', 0);
      $('body').scrollTop($(window).height()/2);
      $('.page-wrapper').css('overflow', 'hidden');
      $('.page-wrapper').css('height', '100vh');

      var scrollTop;
      var scrollLeft;
      /* Position the scroll on zoom click */
      if($(window).width()*xy*multiplierForMobile > $(window).height() || $(window).width()*xy*multiplierForMobile == $(window).height()){
        scrollTop = (($(window).width()*xy*multiplierForMobile) - $(window).height())/2;
        scrollLeft = (($(window).width()*multiplierForMobile) - $(window).width())/2;
      }
      else{
        // var scrollTop = ($(window).height()-($(window).width()*xy))/2;
        scrollTop = 0;
        scrollLeft = 0;
      }
      $(window).scrollTop(scrollTop);
      $(window).scrollLeft(scrollLeft);
      // $('.closeSlick').css({
      //  left:  $('.nextSlick').css('left'),
      //  top:   $('.nextSlick').css('top')
      // });
    });

    $('.slick-slide').on('click', function(event) {
      event.preventDefault();
    }, false);

    // $('.imageZoomContainer').mousemove(function(e){
    //   $('.zoomContainer img').css({left: -e.pageX + 50, top: -e.pageY });
    // })


  }

  // NEW 
  function checkForTouchDevice(){
    supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;

    if ($('.products.list.items.product-items.show').length > 0) {
      $('.products.list.items.product-items.show').addClass('touchDevice');
    }
  }

  function getXYPrismix(evt, element) {
      var rect = element.getBoundingClientRect();
      var scrollTop = document.documentElement.scrollTop?
                      document.documentElement.scrollTop:document.body.scrollTop;
      var scrollLeft = document.documentElement.scrollLeft?                   
                      document.documentElement.scrollLeft:document.body.scrollLeft;
      var elementLeft = rect.left+scrollLeft;  
      var elementTop = rect.top+scrollTop;

      x = evt.pageX-elementLeft;
      y = evt.pageY-elementTop;
      return {x:x, y:y};
  }

  function prismicHoverPosition(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    if(supportsTouch == true){
      return;
    }
    jQuery('.page-layout-1column-prismic .image-container a').mouseover(function(e){
        var m=getXYPrismix(e, this);
        //var top = $(this).parent().height();
        //var max = $(this).parent().height() - ($(this).parent().height() * 0.1);
        // var bottom = ((m.y-(jQuery(this).find('h2').height()/2)) > max ? max : (m.y-(jQuery(this).find('h2').height()/2)));
        if(m.y < jQuery(this).height() - (jQuery(this).find('h2').height()/2)){
            //jQuery(this).find('h2').css('top', (m.y-(jQuery(this).find('h2').height()/2)) < top ? top : (m.y-(jQuery(this).find('h2').height()/2)) );
            // jQuery(this).find('h2').css('top', ($(this).height() / 2) - (jQuery(this).find('h2').height()/2) );
        }
    });
  }


  function prismicImageClick(){
    if(supportsTouch === true){
      $('.page-layout-1column-prismic .image-container a').click(function(){
        if($(this).hasClass('active') === false){
          event.preventDefault();
        }
        $('.page-layout-1column-prismic .image-container a').removeClass('active');
        $(this).addClass('active');
      });
    }
  }

  var video;
  var canvas;

  function startPlayback()
  {
    if (!video) {
      video = document.createElement('video');
      video.src = $('source').attr('src');
      video.loop = true;
      video.addEventListener('playing', paintVideo);
    }
    video.play();
  }

  function paintVideo()
  {
    if (!canvas) {
      canvas = document.createElement('canvas');
      canvas.width = video.videoWidth;
      canvas.height = video.videoHeight;
      document.body.appendChild(canvas);
    }
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    if (!video.paused)
      requestAnimationFrame(paintVideo);
  }

  function lazyLoadImages(){
    $('.lazy').each(function(){
      if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).addClass('loaded');
      }
    });
  }
  function moveImages(){
    $('.image-gallery-5 .image-container.one').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, +'+( (15*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 15*$(window).scrollTop()/$(this).offset().top ) +'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.two').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (10*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 10*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.three').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (10*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 10*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.four').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (25*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 25*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.five').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (42*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 42*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
  }
  function introScroll(){
    // if($(window).scrollTop() < $('.collection-intro-spacer').height()){
    if(jQuery('.collection-intro .intro-image').height() != null){
      $('.collection-intro .intro-image').width(100-($(window).scrollTop()/$(window).height()*100/2)+"%");
      // }
      if($('.collection-intro .intro-image').height() < $(window).height()){
        $('.collection-intro .intro-image').addClass('fadeout');
        if($('.collection-intro-text').offset().top < ($(document).scrollTop()+$(window).height())){
          $('.title-wrapper').css('top', 'calc(50% + '+($('.collection-intro-text').offset().top-$(window).scrollTop()-$(window).height())+'px)');
        }else{
          $('.title-wrapper').css('top', 'calc(50%)');
        }
      }else{
        $('.collection-intro .intro-image').removeClass('fadeout');
      }
    }

  }




  function elementsHeight(){
    $('.page-layout-1column-prismic-new .heroSection').height($(window).height());
    // $('.page-layout-1column-prismic-new .backgroundImage').height($(window).height()*1.2);
    if(checkForScreenSize() == 'mobile'){
      // $('.page-layout-1column-prismic-new .backgroundImage').height($(window).height()*1.2);
      $('.page-layout-1column-prismic-new .backgroundImage').height($(window).height());
    }
    else{
      $('.page-layout-1column-prismic-new .backgroundImage').height($(window).height()); 
    }
    $('.page-layout-1column-prismic-new main section.instagram .spaceBottom').height($(window).height()/2);

  }

  function smoothScroll(){
    // $('.page-layout-1column-prismic-new .column.main .container').animate({});
    var scroll = $(this).scrollTop() * -1;
    var transform = 'translate3d(0px, '+scroll+'px, 0px)';
    $('.page-layout-1column-prismic-new .column.main .container').animate().css({
      '-webkit-transform': transform,
      '-moz-transform': transform,
      '-ms-transform': transform,
      '-o-transform': transform,
      'transform': transform,
    });
  }

  function scrollCollectionPage(){
      if($('.collectionPageLayout #change').length > 0){
        if ($(this).scrollTop() < $('.collectionPageLayout #change').position().top - $(window).height()) {
          $('body').removeClass('changeColor');
        }
        else{
          $('body').addClass('changeColor');
          if($('.container_collection .full_collection').length > 0){
            if ($(this).scrollTop() >= $('.container_collection .full_collection').position().top - 48) {
              $('body').removeClass('changeColor');     
            }
          }
          else{
            $('body').addClass('changeColor');
          }
        }
      } 
      if($('.collectionPageLayout .durum').length > 0){
        if(($(this).scrollTop()+$(window).height()) > $('.product_inner.full_collection.parallax_pull').position().top + ($('.product_inner.full_collection.parallax_pull').height()/2) ){
          $('.after-collection').addClass('active');
        }
        else{
          $('.after-collection').removeClass('active');
        }
        // if($(window).width() > 1000){
        //   if($(this).scrollTop() >= $('.collectionPageLayout .durum').position().top + $('.collectionPageLayout .durum').outerHeight()+($('.product_inner.full_collection.parallax_pull').outerHeight() - $(window).height()) ){
        //     // $('.full_collection_parallax').height($('.product_inner.full_collection.parallax_pull').outerHeight());
        //     // $('.product_inner.full_collection.parallax_pull').addClass('active');
            
        //   } 
        //   else {
        //     // $('.product_inner.full_collection.parallax_pull').removeClass('active');
        //     // $('.full_collection_parallax').height(0);
        //   }
        // }
        // else if ($(window).width() > 767 && $(window).width() < 1000 ){
        //   if($(this).scrollTop() > $('.collectionPageLayout .durum').position().top + $('.collectionPageLayout .durum').outerHeight() - $(window).height() + $('.product_inner.full_collection.parallax_pull').outerHeight() ){
        //     // $('.full_collection_parallax').height($('.product_inner.full_collection.parallax_pull').outerHeight());
        //     // $('.product_inner.full_collection.parallax_pull').addClass('active');
            
        //   } 
        //   else {
        //     // $('.product_inner.full_collection.parallax_pull').removeClass('active');
        //     // $('.full_collection_parallax').height(0);
        //   }
        // } else {
        //   if($(this).scrollTop() > $('.collectionPageLayout .durum').position().top + $('.collectionPageLayout .durum').outerHeight() - $(window).height() + $('.product_inner.full_collection.parallax_pull').outerHeight() ){
        //     // $('.full_collection_parallax').height($('.product_inner.full_collection.parallax_pull').outerHeight());
        //     // $('.product_inner.full_collection.parallax_pull').addClass('active');
            
        //   } 
        //   else {
        //     // $('.product_inner.full_collection.parallax_pull').removeClass('active');
        //     // $('.full_collection_parallax').height(0);
        //   }
        // }
      }
  }

  function collectionElementHeight(){
    $('.collectionPageLayout .parallaxEl').height($(window).height());
    $('.collectionPageLayout .parallaxEl').height($(window).height()-250); 

    if(checkForScreenSize() == 'mobile'){
      $('.collectionPageLayout .product_inner.full_collection.parallax_pull').width($(window).width());
      $('.collectionPageLayout .product_inner.full_collection.parallax_pull').css('margin-left', -($(window).width()*0.08));
    }
  }



  function accordion() {

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");

        var panel = this.nextElementSibling;

        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }

  }

  $(window).scroll(function(){
    lazyLoadImages();
    moveImages();
    introScroll();
    // smoothScroll();
    if($('.page-layout-1column-prismic-new .product .newsletter').length > 0){
      if ($(this).scrollTop() < $('.page-layout-1column-prismic-new .product .newsletter').position().top + $(window).height()/2) {
        $('body').removeClass('changeColor')
      }
      else{
        $('body').addClass('changeColor')
      }
    }

    if($('.page-layout-1column-prismic-new .instagram').length > 0){
      if ($(this).scrollTop() > $('.page-layout-1column-prismic-new .instagram').position().top) {
        $('.page-layout-1column-prismic-new .backgroundImage, .page-layout-1column-prismic-new .background').css('visibility', 'hidden');
      }
      else{
        $('.page-layout-1column-prismic-new .backgroundImage, .page-layout-1column-prismic-new .background').css('visibility', 'visible')
      }
    }
    scrollCollectionPage();
  });

// NEW



  jQuery(document).ready(function(){

    
    // console.log( $('priceContainer').length() );

    // checkCookie();
    supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
    // console.log(supportsTouch);
    relatedClick();
    listClicked();
    // hoverList();
    // REMOVED 5.12.19
    // productPageImagePagination();
    mobileToggle();
    openSearch();
    openCustomCart();
    productPageHeight();
    mediaGallery();
    coverImg();
    coverImgFullWidth();
    coverImgFullWidthFront();
    coverImgNewsletter();
    loginFormShow();
    categoryMenu();
    closeMenu();
    productHoverFollow();
    menuHover();
    relatedProductSlider();
    changeProductGrid();
    // cartItemSize();
    // cartPage();
    secondClickMenuItems();
    menuOverlay();
    hover();
    $('.page-wrapper .column.main .products ol').addClass('show');
    $('.catalog-category-view .column.main').addClass('active');
    
    $('.preloaderContainer').delay(2000).fadeOut();

    if($(window).width() <= 787 ) {
      $('.product.media .slick-prev, .product.media .slick-next').hide();
      $('.nextSlick').hide();
    }
    $('#product-addtocart-button').on('click', function(event) {
      // console.log("asdad");
      var checkExist = setInterval(function() {
         if ($('.swatch-attribute .mage-error').length) {
            $('#product-addtocart-button').html($.mage.__('Please Choose a Size'));
            $('#product-addtocart-button').prop('disabled', true);
            $('.swatch-option').on('click', function(event) {
              $('#product-addtocart-button').html($.mage.__('Add to Cart'));
              $('#product-addtocart-button').prop('disabled', false);
            });
            clearInterval(checkExist);
         }
      }, 100);
    });
    zooming();
    
    // NEW

    checkForTouchDevice();
    prismicHoverPosition();
    prismicImageClick();
    lazyLoadImages();
    elementsHeight();
    collectionElementHeight();
    sheight = $(window).height();
    // startPlayback();
    // $('#over_video').click(function(){
    //   startPlayback();
    // });
    // $('.page-layout-1column-prismic-new .containerSlideFix').height(
    //   $('.page-layout-1column-prismic-new .column.main .container .heroSection').height() 
    //   +
    //   $('.page-layout-1column-prismic-new .column.main .container .product').height() 
    //   +
    //   $('.page-layout-1column-prismic-new .column.main .container .instagram').height() 
    //   );
    $('body.page-layout-1column-prismic-new').css('opacity', 1);
    $('.collectionPageLayout #change').width($(window).width());
    accordion();

  });


  $(document).on('customer-data-reload', function (event, sectionNames)  {
      if(sectionNames.indexOf('cart') != -1){
          setTimeout(function() {
              var counter = $("#minicart-content-wrapper ol#mini-cart > li").length;
              $('.minicart-wrapper').addClass('show');
              $('.openCartLink').addClass('show');
              setCartHeight();
              openCustomCart();

          }, 1000);
      }
  });

  $(document).on('swatch.initialized', function(){
    checkForMobilePage();
    //addToCartFromList();
    addToCartFromSinglePage();
    // showSwatches();
    // $('.swatch-attribute-options').find('.swatch-option').eq(0).addClass('selected');
    // $('.super-attribute-select').val($('.swatch-attribute-options').find('.swatch-option').eq(0).attr('option-id'));
    // el = $('.swatch-attribute-options .swatch-option.selected');
    // selected = true;
    //$('.page-wrapper .column.main .products ol').addClass('show');
  });

  jQuery(window).resize(function(){
    // NEW
    sheight = $(window).height();
    $('.collectionPageLayout #change').width($(window).width());
    elementsHeight();
    collectionElementHeight();

    coverImgFullWidth();
    coverImgFullWidthFront();
    coverImg();
    coverImgNewsletter();
    // minicartItems();
    loginFormShow();
    productPageHeight();
    setCartHeight();

    // cartItemSize();
  });

  jQuery(window).resize(function(){
      var xy = 1.5003646973;
      var scrollTop;
      var scrollLeft;
      if($('body').hasClass('zoomedImage')){
        if ($(window).width() < 769 ) {
          multiplierForMobile = 2;
          //$('.imageZoomContainer').css('width', '200%');
        }else{
          multiplierForMobile = 1;
          $('.imageZoomContainer').css('width', '100%');
        }
      if ($(window).width() > 768 ){
        $('.imageZoomContainer .innerZoom img').css('height', $(window).width()*xy*multiplierForMobile + 'px');
      }
        /* Position the scroll on zoom click */
        if($(window).width()*xy*multiplierForMobile > $(window).height() || $(window).width()*xy*multiplierForMobile == $(window).height()){
          scrollTop = (($(window).width()*xy*multiplierForMobile) - $(window).height())/2;
          scrollLeft = (($(window).width()*multiplierForMobile) - $(window).width())/2;
        }
        else{
          // var scrollTop = ($(window).height()-($(window).width()*xy))/2;
          scrollTop = 0;
          scrollLeft = 0;
        }
        $(window).scrollTop(scrollTop);
        $(window).scrollLeft(scrollLeft);
      }
  });

});
