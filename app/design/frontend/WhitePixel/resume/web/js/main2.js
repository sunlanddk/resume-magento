require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
  $ = jQuery;
  var supportsTouch = false;
  var itemsWidth = 0;
  function mobileToggle(){
    jQuery('.mobileToggle').click(function(){
      jQuery('header').toggleClass('active');
      $('header .bgoverlay').addClass('active');
    });
  }

  function coverImg(){
    var windowH = $(window).height();
    var pageHeader = $(window).height();
    var minHeight = windowH - pageHeader;
    $("img.cover").each(function (){
      $(this).css("max-width", "none");
      var containerH = $('.imageItem').height();
      var containerW = $('.imageItem').width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log(counter + ": " + (imageXY > containerXY));
      // console.log(counter + ": " + (imageXY < containerXY));

      if(imageXY >= containerXY){
        $(this).height(containerH - 150);
        $(this).width('auto');
  //       $(this).height(containerH);
  //       $(this).width('auto');
        var wWidth = containerW;
        var iWidth = $(this).width();
  //       $(this).css("marginLeft", (wWidth-iWidth)/2 );
  //       $(this).css("marginTop", 75 );
      }
      if(imageXY < containerXY){
  //       $(this).width($(window).width());
  //       $(this).height(containerW / imageXY);
        $(this).width('auto');
        $(this).height(containerH - 200);
        var wHeight = $(this).width();
        var iHeight = containerH - 200;
  //       $(this).css("marginTop", 100 );
  //       $(this).css("marginLeft", (containerW-wHeight)/2 );
      };
    });
  }
  function coverImgFullWidth(){
    $("img.cover-full-width").each(function (){
      $(this).css("max-width", "none");
      var containerH = $('.imageContainer').parent().height();
      var containerW = $('.imageContainer').parent().width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log("containerXY " + containerXY);
      // console.log("imageXY " + imageXY);

      if(imageXY >= containerXY){
        $(this).height(containerH);
        $(this).width('auto');
        var wWidth = $(this).width();;
        $(this).css("marginTop", 0 );
        $(this).css("marginLeft", (containerW-wWidth)/2 );
      }
      if(imageXY < containerXY){
        $(this).height('auto');
        $(this).width(containerW);
        var wHeight = $(this).height();
    //     $(this).css("marginTop", (containerH-wHeight)/2 );
        $(this).css("marginLeft", 0 );

      };
    });
  }
  function coverImgNewsletter(){
    $("img.cover-newsletter").each(function (){
      $(this).css("max-width", "none");
      var containerH = $(this).parent().parent().height();
      var containerW = $(this).parent().parent().width();
      var containerXY = containerW / containerH;
      var imageXY = $(this).width() / $(this).height();
      // console.log("containerXY " + containerXY);
      // console.log("imageXY " + imageXY);

      if(imageXY >= containerXY){
        $(this).height(containerH);
        $(this).width('auto');
        var wWidth = $(this).width();;
        $(this).css("marginTop", 0 );
        $(this).css("marginLeft", (containerW-wWidth)/2 );
      }
      if(imageXY < containerXY){
        $(this).height('auto');
        $(this).width(containerW);
        var wHeight = $(this).height();
    //     $(this).css("marginTop", (containerH-wHeight)/2 );
        $(this).css("marginLeft", 0 );

      };
    });
  }

  function openSearch(){
    // $('.block-search .block-title').click(function(){
    //   $(this).parent().toggleClass('active');
    // });
  }

  function showSwatches(){
    $('.swatch-attribute-label, .swatch-attribute-selected-option').unbind().click(function(){
      $('.product-add-form').addClass('active');
      $('.swatch-attribute-label').addClass('active');
    });
  }

  function addToCartFromSinglePage(){
    if(getTotalsInCart() > 0){
      $('.navRight').addClass('active');
    }
    $('.product-info-main .swatch-option').unbind().click(function(){
      if($('.product-info-main .swatch-option').length == 1){
        if($('.product-info-main .swatch-option.selected').length == 1){
          $('.swatch-attribute-label').removeClass('active');
        }
      }
      $('.product-add-form').removeClass('active');
      $('.swatch-attribute-selected-option').addClass('active');
      // $('[data-block="minicart"]').unbind().on('contentUpdated', function ()  {
      $('[data-block="minicart"]').on('contentUpdated', function ()  {
        changeTotalItemsInCart();
        $('.block-minicart').addClass('active');
        $('.bgoverlay').addClass('active');
        minicartItems();
        cloneCartItems();
      });
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.bgoverlay').removeClass('active');
      });
    });
    $('.product-item-details .action.delete').unbind().click(function(){
      // $('[data-block="minicart"]').unbind().on('contentUpdated', function ()  {
      $('[data-block="minicart"]').on('contentUpdated', function ()  {
        changeTotalItemsInCart();
        $('.block-minicart').addClass('active');
        $('.bgoverlay').addClass('active');
        minicartItems();
        cloneCartItems();
      });
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.bgoverlay').removeClass('active');
      });
    });
    $('.box-tocart').unbind().click(function(){
      $('[data-block="minicart"]').on('contentUpdated', function ()  {
        changeTotalItemsInCart();
        $('.block-minicart').addClass('active');
        $('.bgoverlay').addClass('active');
        minicartItems();
        cloneCartItems();
      });
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.bgoverlay').removeClass('active');
      });
    });
  }
  
  // function addToCartFromList(){
  //   $('.products.list .item .swatch-option').unbind().click(function(){
  //     // $(this).parent().parent().parent().parent().find('form').submit();
  //     var parent = $(this).parent().parent().parent().parent(); 
  //     parent.find('.super-attribute-select').val($(this).attr('option-id'));
  //     parent.find('form').submit();

  //     // $('[data-block="minicart"]').unbind().on('contentUpdated', function ()  {
  //     $('[data-block="minicart"]').on('contentUpdated', function ()  {
  //       changeTotalItemsInCart();
  //       $('.block-minicart').addClass('active');
  //       $('.bgoverlay').addClass('active');
  //       minicartItems();
  //       cloneCartItems();
  //     });
  //     $('.bgoverlay, #btn-minicart-close').click(function(){
  //       $('.block-minicart').removeClass('active');
  //       $('.bgoverlay').removeClass('active');
  //     });
  //   });
  // }

  function openCustomCart(){
    $('header a.showcart, header .openCartLink').unbind().click(function(){
      if(getTotalsInCart() > 0){
        $('.navRight').addClass('active');
      }
      event.preventDefault();
      minicartItems();
      changeTotalItemsInCart();
      $('.block-minicart').addClass('active');
      $('.bgoverlay').addClass('active');
      $('.openCartLink').removeClass('active');
      cloneCartItems();
      $('.bgoverlay, #btn-minicart-close').click(function(){
        $('.block-minicart').removeClass('active');
        $('.bgoverlay').removeClass('active');
      });
    });

  }

  function minicartItems(){
    var width = $('header .cartItemNavigation').width();
    if(width != itemsWidth){
      $('header .cartItemNavigation style').remove();
      $('header .cartItemNavigation').append('<style>.minicart-items-wrapper ol li.item{width: '+width+'px !important}</style>');  
    }
    // $('.cartItemNavigation').append('<style>.minicart-items-wrapper ol li.item{width: '+width+'px !important}</style>');
  }


  function cartItemSize(){
    $('.cart.table-wrapper .cart.item').width($('.cart-container .cartItemNavigation').width());
  }

  function cartPage(){
    $('.cart-container .itemCart .totalItem').text(pad(parseInt($('.cart.table-wrapper .cart.item').length), 2));
    if($('.cart.table-wrapper .cart.item').length > 1){
      $('.cart-container .navRight').addClass('active');
    }
    $('.cart-container .navLeft').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.cart-container .itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('.cart-container .cartItemNavigation').width() + 30;
      }
      else{
        var left = $('.cart-container .cartItemNavigation').width() + 15; 
      }
      $(".cart.table-wrapper").animate({
        left: "+="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cart-container .cartItemNavigation .itemCart .currentItem').text()) - 1 );
        $('.cart-container .navRight, .cart-container .navLeft').addClass('active');
        if(number == 1){
          $('.cart-container .navLeft').removeClass('active');
        }
        $('.cart-container .cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });

    $('.cart-container .navRight').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.cart-container .itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('.cart-container .cartItemNavigation').width() + 30;
      }
      else{
        var left = $('.cart-container .cartItemNavigation').width() + 15; 
      }
      $(".cart.table-wrapper").animate({
        left: "-="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cart-container .cartItemNavigation .itemCart .currentItem').text()) + 1 );
        $('.cart-container .navRight, .cart-container .navLeft').addClass('active');
        if(number === totalItem){
          $('.cart-container .navRight').removeClass('active');
        }
        $('.cart-container .cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });
  }

  function cloneCartItems(){
    minicartItems();
    $('.navLeft').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('#minicart-content-wrapper').width() + 30;
      }
      else{
        var left = $('#minicart-content-wrapper').width() + 15; 
      }
      $(".minicart-items-wrapper ol").animate({
        left: "+="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cartItemNavigation .itemCart .currentItem').text()) - 1 );
        $('.navRight, .navLeft').addClass('active');
        if(number == 1){
          $('.navLeft').removeClass('active');
        }
        $('.cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });

    $('.navRight').unbind().click(function(){
      if($(this).hasClass('active') === false){
        return;
      }
      var totalItem = parseInt($('.itemCart .totalItem').text());
      if($(window).width() > 767){
        var left = $('#minicart-content-wrapper').width() + 30;
      }
      else{
        var left = $('#minicart-content-wrapper').width() + 15; 
      }
      $(".minicart-items-wrapper ol").animate({
        left: "-="+left,
      }, 500, function() {
        // Animation complete.
        var number = (parseInt($('.cartItemNavigation .itemCart .currentItem').text()) + 1 );
        $('.navRight, .navLeft').addClass('active');
        if(number === totalItem){
          $('.navRight').removeClass('active');
        }
        $('.cartItemNavigation .itemCart .currentItem').text(pad(number, 2));
      });
    });
    minicartItems();

    if(getTotalsInCart() > 0){
      $('.navRight').addClass('active');
    }
  }
  function getTotalsInCart(){
    return $('.minicart-items-wrapper ol li.item').length;
  }

  function changeTotalItemsInCart(){
    $('header .itemCart .totalItem').text(pad(parseInt($('header .itemCart .totalItem').text()),2));
    $('header .itemCart .totalItem').unbind();

    $(document).on("change", ".totalItem", function () {
      $('header .itemCart .totalItem').text(pad(parseInt($('header .itemCart .totalItem').text()),2));
    });
  }

  function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }

  var lastScrollTop = 0;

  function headerVisible(){
    var st = $(this).scrollTop();
    // if (st > lastScrollTop){
    //   $('header').css('top', - $("header").outerHeight());
    // } 
    // else {
    //   $('header').css('top', 0);
    // }
    if (st > lastScrollTop && st > 0){
      // $('.mobileToggle').css('top', - 80);
      $('header, .categoriesMenuToggle, .gridChange').addClass('up');
    } 
    else {
      if(checkForScreenSize() == 'mobile'){
        // $('.mobileToggle').css('top', 30);
        $('header, .categoriesMenuToggle, .gridChange').removeClass('up');
      }
      else{
        // $('.mobileToggle').css('top', 50);
        $('header, .categoriesMenuToggle, .gridChange').removeClass('up');
      }
    }
    lastScrollTop = st;
  }

  function productPageHeight(){
    if(checkForScreenSize() == 'mobile'){
      $('.product-info-main').height($(window).height());
      return;
    }
    $('.product-info-main, .productDetails, .paralaxElement').height($(window).height());
  }


  function checkForScreenSize(){
    if($(window).width() > 767){
      return 'desktop';
    }
    return 'mobile';
  }

  function mediaGallery(){
    var slickOptions = {
      draggable: false
    }

    // Run on first slider load
    $('#imageGallery').on('init', function(event, slick) {
      $('#imageGallery').css('opacity', 1);
      $('.column.main').addClass('active');
    });
    // Init slider
    var slider = $('#imageGallery').slick(slickOptions);
  }


  function productPageImagePagination(){
    $('.imageGallery, .nextSlick').click(function(){
      $('#imageGallery').slick('slickNext');
      var count = parseInt($('.paginationText').text()); 
      var last = parseInt($('.lastPagination').text());
      count += 1;
      if(count > last){
        count = 1;
      }
      $('.paginationText').text(pad(count,2));
    });
  }


  function loginFormShow(){
    $('.customer-account-login .column.main, .customer-account-create .column.main').css('min-height', $(window).height());
    $('.showLogin').unbind().click(function(){
      $('.block-new-customer').hide();
      $('.block-customer-login').addClass('active');
    });
  }

 	function categoryMenu(){
 		$('.categoriesMenuToggle, .categoryOverlay').click(function(){
 			$('.categoryOverlay, .categoriesMenu, .categoryOverlay').toggleClass('active');
 			$('header, body').toggleClass('hide');
      $('#maincontent').toggleClass('cat-shown');
      $('footer').toggleClass('cat-shown');
 			$('.products.wrapper').toggleClass('blurred');
 		});
 	}


 	function blurOnScroll(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    var blur = ($(this).scrollTop()/$(window).height())*10 > 10 ? 10 : ($(this).scrollTop()/$(window).height())*10; //(($(this).scrollTop()*0.01) > 10 ? 10 : ($(this).scrollTop()*0.01));
    var opacity = 1-((blur/10)*0.6);
		$('.product.media, .catalog-product-view .page-wrapper .product-info-main .product-info-price, .catalog-product-view .page-wrapper .product-info-main .product-add-form').css({'filter': 'blur('+blur+'px)', 'opacity': opacity});
 	}
  
  $(window).scroll(function(){
    headerVisible();
    blurOnScroll();
  });

  $(document).on('mousemove', function(e){
    if($(window).width() < 767){
      return;
    }
    var x = e.pageX - 35;
    var y = e.pageY - 15 - $(document).scrollTop();
    if(y < 85 || $(document).scrollTop() > 0){
      // y = 85;
      $('.nextSlick').fadeOut();
    }
    else if(y > ($(window).height() - 140)){
      // y = $(window).height() - 140 - $(document).scrollTop();
      $('.nextSlick').fadeOut();
    }
    else{
      $('.nextSlick').fadeIn();
    }
    
    $('.nextSlick').css({
       left:  x-25,
       top:   y+25
    });
  });

  function closeMenu(){
    $('.bgoverlay').click(function(){
      $('header').removeClass('active show');
      $(this).removeClass('active');
      $('.menuContainer .anchor').removeClass('active');
      $('.menuContainer').height(50);
    });
    $('.closeMenu').click(function(){
      $('header').removeClass('active show');
      $('.bgoverlay').removeClass('active');
      $('.menuContainer .anchor').removeClass('active');
      $('.menuContainer').height(50);
    });
  }
  function getXY(evt, element) {
      var rect = element.getBoundingClientRect();
      var scrollTop = document.documentElement.scrollTop?
                      document.documentElement.scrollTop:document.body.scrollTop;
      var scrollLeft = document.documentElement.scrollLeft?                   
                      document.documentElement.scrollLeft:document.body.scrollLeft;
      var elementLeft = rect.left+scrollLeft;  
      var elementTop = rect.top+scrollTop;

      x = evt.pageX-elementLeft;
      y = evt.pageY-elementTop;

      return {x:x, y:y};
  }
  function productHoverFollow(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    if(supportsTouch == true){
      return;
    }

    jQuery('.product-item .productListOverlay').mousemove(function(e){
        var m=getXY(e, this);
        // if(jQuery('.page-header').hasClass('up')){
        //   if(e.pageY < 110){
        //     jQuery(this).css('opacity', 0);
        //   }else{
        //    jQuery(this).removeAttr('style');
        //   }  
        // }else{
        //   if(e.pageY-pageYOffset < 110){
        //     jQuery(this).css('opacity', 0);
        //   }else{
        //    jQuery(this).removeAttr('style');
        //   }  
        // }
        
        if(m.y < jQuery('.product-item .productListOverlay').height() - ((jQuery(this).find('.product-item-name').height() + jQuery(this).find('.priceContainer').height())/2) ){
          // if((m.y-jQuery(this).find('.priceContainer').height() + 5) < 0){
          //   jQuery(this).find('.product-item-name').css('top', 0); 
          //   jQuery(this).find('.priceContainer').css('top', 0 + jQuery(this).find('.product-item-name').height() + 5);
          // }
          // else{
          //   jQuery(this).find('.product-item-name').css('top', m.y - jQuery(this).find('.product-item-name').height() - 5);
          //   jQuery(this).find('.priceContainer').css('top', m.y);
          // }
            jQuery(this).find('.product-item-name').css('top', (jQuery(this).height() / 2) -  (jQuery(this).find('.product-item-name').height() - 5)); 
            jQuery(this).find('.priceContainer').css('top', jQuery(this).height() / 2);
        }
    });

    jQuery('.relatedProducts .relatedContainer .relatedProduct').mousemove(function(e){
        var m=getXY(e, this);
        if(m.y < jQuery('.relatedProducts .relatedContainer .relatedProduct').height() - (jQuery(this).find('.inforel').height()/2)){
            // jQuery(this).find('.inforel').css('top', (m.y-(jQuery(this).find('.inforel').height()/2)) < 0 ? 0 : (m.y-(jQuery(this).find('.inforel').height()/2)) );
            jQuery(this).find('.inforel').css('top', (jQuery(this).height() / 2) - (jQuery(this).find('.inforel').height()/2)) ;
        }
    });
  }


  function menuHover(){
    if(supportsTouch == true){
      if($(window).width() < 769){
        $('.menuContainer .anchor').click(function(){
          if($(this).hasClass('direct') != true){
            if($(this).hasClass('active') != true){
              event.preventDefault();
            }
            $('.menuContainer .anchor.active').removeClass('active');  
            $(this).addClass('active');  
          }
        });
        return;
      }
      else{
        $('.menuContainer .anchor').click(function(){
          if($(this).hasClass('direct') != true){
            if($(this).hasClass('active') != true){
              event.preventDefault();
            }
            $(this).addClass('active');  
            $('.menuContainer .anchor').removeClass('active full');
            $('.menuContainer').height(50);
            $(this).addClass('active');
            if($(this).find('ul').length > 0){
              var height = $(this).find('ul').outerHeight();
            }
            else{
              var height = 0; 
            }
            $('.menuContainer').height(height + 50);
          }
        });
        return;
      }
    }

    $('.menuContainer .anchor').unbind().mouseover(function(){
      if($(this).hasClass('active')){
        return;
      }

      $('.menuContainer .anchor').removeClass('active full');
      $('.menuContainer').height(50);
      $(this).addClass('active');
      if($(this).find('ul').length > 0){
        var height = $(this).find('ul').outerHeight();
      }
      else{
        var height = 0; 
      }
      $('.menuContainer').height(height + 50);
    });
  }


  function relatedProductSlider(){
    if(checkForScreenSize() == 'mobile'){
      $('.relatedProducts .relatedContainer .relatedProduct').width(($(window).width()/1.5));
    }
    else{
      $('.relatedProducts .relatedContainer .relatedProduct').width(($(window).width()/4));
    }

    var $c = $('.relatedProducts .relatedContainer'),
      $w = $(window);
   
    $c.carouFredSel({
      align: false,
      items: 'variable',
      scroll: {
        items: 1,
        duration: 5000,
        timeoutDuration: 0,
        easing: 'linear',
        pauseOnHover: 'immediate'
      }
    });
   
    
    $w.bind('resize.example', function() {
      var nw = $w.width();
      if (nw < 990) {
        nw = 990;
      }
   
      $c.width(nw * 3);
      $c.parent().width(nw);
   
    }).trigger('resize.example');
  }

  function listClicked(){
    if(supportsTouch == true){
      $('ol.product-items li.product-item').click(function(){
        if($(this).hasClass('pressed') == true){
          return;
        }
        event.preventDefault();
        $('ol.product-items li.product-item').removeClass('pressed');
        $(this).addClass('pressed');
        
      });
    }
  }

  function relatedClick(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    $('.relatedProducts .relatedProduct').click(function(){
      if($(this).hasClass('pressed') == true){
        return;
      }
      if(supportsTouch == true){
        event.preventDefault();
        $('.relatedProducts .relatedProduct').removeClass('pressed');
        $(this).addClass('pressed');
      }
    });
  }

  function changeProductGrid(){
    $('.gridChange .multiple').click(function(){
        $('.gridChange .single, .gridChange .multiple').toggleClass('active');
      $('.products-grid ol.product-items').toggleClass('single');
    });
    $('.gridChange .single').click(function(){
      $('.gridChange .single, .gridChange .multiple').toggleClass('active');
      $('.products-grid ol.product-items').toggleClass('single');
    });
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function checkCookie() {
    var first = getCookie("firstvisit");
    if (first != "") {
    } else {
      setCookie("firstvisit", "firstvisit", 186);
      console.log('redirect to Location page');
      window.location.replace("https://www.resumecph.dk/location");
    }
  }


  jQuery(document).ready(function(){
    checkCookie();
    supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
    // console.log(supportsTouch);
    relatedClick();
    listClicked();
    productPageImagePagination();
    mobileToggle();
    openSearch();
    openCustomCart();
    productPageHeight();
    mediaGallery();
    coverImg();
    coverImgFullWidth();
    coverImgNewsletter();
    loginFormShow();
    categoryMenu();
    closeMenu();
    productHoverFollow();
    menuHover();
    relatedProductSlider();
    changeProductGrid();
    cartItemSize();
    cartPage();
    $('.page-wrapper .column.main .products ol').addClass('show');
    $('.catalog-category-view .column.main').addClass('active');
    
    $('.preloaderContainer').hide();
  });


  $(document).on('customer-data-reload', function (event, sectionNames)  {
      if(sectionNames.indexOf('cart') != -1){
          setTimeout(function() {
              var counter = $("#minicart-content-wrapper ol#mini-cart > li").length;
              $('.minicart-wrapper').addClass('show');
              $('.openCartLink').addClass('show');
              openCustomCart();
          }, 1000);
      }
  });

  $(document).on('swatch.initialized', function(){
    //addToCartFromList();
    addToCartFromSinglePage();
    showSwatches();
    $('.swatch-attribute-options').find('.swatch-option').eq(0).addClass('selected');
    $('.super-attribute-select').val($('.swatch-attribute-options').find('.swatch-option').eq(0).attr('option-id'));
    //$('.page-wrapper .column.main .products ol').addClass('show');
  });

  jQuery(window).resize(function(){
    coverImgFullWidth();
    coverImg();
    coverImgNewsletter();
    minicartItems();
    loginFormShow();
    productPageHeight();
    cartItemSize();
  });


});
