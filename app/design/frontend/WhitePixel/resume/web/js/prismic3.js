require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
  $ = jQuery;
  var supportsTouch = false;
  
  function checkForScreenSize(){
    if($(window).width() > 767){
      return 'desktop';
    }
    return 'mobile';
  }

  function checkForTouchDevice(){
    supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
  }

  function getXYPrismix(evt, element) {
      var rect = element.getBoundingClientRect();
      var scrollTop = document.documentElement.scrollTop?
                      document.documentElement.scrollTop:document.body.scrollTop;
      var scrollLeft = document.documentElement.scrollLeft?                   
                      document.documentElement.scrollLeft:document.body.scrollLeft;
      var elementLeft = rect.left+scrollLeft;  
      var elementTop = rect.top+scrollTop;

      x = evt.pageX-elementLeft;
      y = evt.pageY-elementTop;
      return {x:x, y:y};
  }

  function prismicHoverPosition(){
    if(checkForScreenSize() == 'mobile'){
      return;
    }
    if(supportsTouch == true){
      return;
    }
    jQuery('.page-layout-1column-prismic .image-container a').mouseover(function(e){
        var m=getXYPrismix(e, this);
        //var top = $(this).parent().height();
        //var max = $(this).parent().height() - ($(this).parent().height() * 0.1);
        // var bottom = ((m.y-(jQuery(this).find('h2').height()/2)) > max ? max : (m.y-(jQuery(this).find('h2').height()/2)));
        if(m.y < jQuery(this).height() - (jQuery(this).find('h2').height()/2)){
            //jQuery(this).find('h2').css('top', (m.y-(jQuery(this).find('h2').height()/2)) < top ? top : (m.y-(jQuery(this).find('h2').height()/2)) );
            // jQuery(this).find('h2').css('top', ($(this).height() / 2) - (jQuery(this).find('h2').height()/2) );
        }
    });
  }


  function prismicImageClick(){
    if(supportsTouch === true){
      $('.page-layout-1column-prismic .image-container a').click(function(){
        if($(this).hasClass('active') === false){
          event.preventDefault();
        }
        $('.page-layout-1column-prismic .image-container a').removeClass('active');
        $(this).addClass('active');
      });
    }
  }

  var video;
  var canvas;

  function startPlayback()
  {
    if (!video) {
      video = document.createElement('video');
      video.src = $('source').attr('src');
      video.loop = true;
      video.addEventListener('playing', paintVideo);
    }
    video.play();
  }

  function paintVideo()
  {
    if (!canvas) {
      canvas = document.createElement('canvas');
      canvas.width = video.videoWidth;
      canvas.height = video.videoHeight;
      document.body.appendChild(canvas);
    }
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    if (!video.paused)
      requestAnimationFrame(paintVideo);
  }

  function lazyLoadImages(){
    $('.lazy').each(function(){
      if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).addClass('loaded');
      }
    });
  }
  function moveImages(){
    $('.image-gallery-5 .image-container.one').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, +'+( (15*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 15*$(window).scrollTop()/$(this).offset().top ) +'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.two').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (10*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 10*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.three').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (10*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 10*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.four').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (25*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 25*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
    $('.image-gallery-5 .image-container.five').each(function(){
      // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
        $(this).css('transform', 'translate3d(0px, -'+( (42*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 42*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
        // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
        // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
      // }
    });
  }
  function introScroll(){
    // if($(window).scrollTop() < $('.collection-intro-spacer').height()){
    if(jQuery('.collection-intro .intro-image').height() != null){
      $('.collection-intro .intro-image').width(100-($(window).scrollTop()/$(window).height()*100/2)+"%");
      // }
      if($('.collection-intro .intro-image').height() < $(window).height()){
        $('.collection-intro .intro-image').addClass('fadeout');
        if($('.collection-intro-text').offset().top < ($(document).scrollTop()+$(window).height())){
          $('.title-wrapper').css('top', 'calc(50% + '+($('.collection-intro-text').offset().top-$(window).scrollTop()-$(window).height())+'px)');
        }else{
          $('.title-wrapper').css('top', 'calc(50%)');
        }
      }else{
        $('.collection-intro .intro-image').removeClass('fadeout');
      }
    }

  }
  $(window).scroll(function(){
    lazyLoadImages();
    moveImages();
    introScroll();
  });

  jQuery(document).ready(function(){
    checkForTouchDevice();
    prismicHoverPosition();
    prismicImageClick();
    lazyLoadImages();
    // startPlayback();
    // $('#over_video').click(function(){
    //   startPlayback();
    // });
  });

  jQuery(window).resize(function(){
  
  });


});
