// require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
//   $ = jQuery;
//   var supportsTouch = false;
//   var sheight;
  
//   function checkForScreenSize(){
//     if($(window).width() > 767){
//       return 'desktop';
//     }
//     return 'mobile';
//   }

//   function checkForTouchDevice(){
//     supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
//   }

//   function getXYPrismix(evt, element) {
//       var rect = element.getBoundingClientRect();
//       var scrollTop = document.documentElement.scrollTop?
//                       document.documentElement.scrollTop:document.body.scrollTop;
//       var scrollLeft = document.documentElement.scrollLeft?                   
//                       document.documentElement.scrollLeft:document.body.scrollLeft;
//       var elementLeft = rect.left+scrollLeft;  
//       var elementTop = rect.top+scrollTop;

//       x = evt.pageX-elementLeft;
//       y = evt.pageY-elementTop;
//       return {x:x, y:y};
//   }

//   function prismicHoverPosition(){
//     if(checkForScreenSize() == 'mobile'){
//       return;
//     }
//     if(supportsTouch == true){
//       return;
//     }
//     jQuery('.page-layout-1column-prismic .image-container a').mouseover(function(e){
//         var m=getXYPrismix(e, this);
//         //var top = $(this).parent().height();
//         //var max = $(this).parent().height() - ($(this).parent().height() * 0.1);
//         // var bottom = ((m.y-(jQuery(this).find('h2').height()/2)) > max ? max : (m.y-(jQuery(this).find('h2').height()/2)));
//         if(m.y < jQuery(this).height() - (jQuery(this).find('h2').height()/2)){
//             //jQuery(this).find('h2').css('top', (m.y-(jQuery(this).find('h2').height()/2)) < top ? top : (m.y-(jQuery(this).find('h2').height()/2)) );
//             // jQuery(this).find('h2').css('top', ($(this).height() / 2) - (jQuery(this).find('h2').height()/2) );
//         }
//     });
//   }


//   function prismicImageClick(){
//     if(supportsTouch === true){
//       $('.page-layout-1column-prismic .image-container a').click(function(){
//         if($(this).hasClass('active') === false){
//           event.preventDefault();
//         }
//         $('.page-layout-1column-prismic .image-container a').removeClass('active');
//         $(this).addClass('active');
//       });
//     }
//   }

//   var video;
//   var canvas;

//   function startPlayback()
//   {
//     if (!video) {
//       video = document.createElement('video');
//       video.src = $('source').attr('src');
//       video.loop = true;
//       video.addEventListener('playing', paintVideo);
//     }
//     video.play();
//   }

//   function paintVideo()
//   {
//     if (!canvas) {
//       canvas = document.createElement('canvas');
//       canvas.width = video.videoWidth;
//       canvas.height = video.videoHeight;
//       document.body.appendChild(canvas);
//     }
//     canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
//     if (!video.paused)
//       requestAnimationFrame(paintVideo);
//   }

//   function lazyLoadImages(){
//     $('.lazy').each(function(){
//       if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
//         $(this).addClass('loaded');
//       }
//     });
//   }
//   function moveImages(){
//     $('.image-gallery-5 .image-container.one').each(function(){
//       // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
//         $(this).css('transform', 'translate3d(0px, +'+( (15*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 15*$(window).scrollTop()/$(this).offset().top ) +'vw, 0px)');
//         // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
//         // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
//       // }
//     });
//     $('.image-gallery-5 .image-container.two').each(function(){
//       // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
//         $(this).css('transform', 'translate3d(0px, -'+( (10*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 10*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
//         // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
//         // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
//       // }
//     });
//     $('.image-gallery-5 .image-container.three').each(function(){
//       // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
//         $(this).css('transform', 'translate3d(0px, -'+( (10*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 10*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
//         // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
//         // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
//       // }
//     });
//     $('.image-gallery-5 .image-container.four').each(function(){
//       // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
//         $(this).css('transform', 'translate3d(0px, -'+( (25*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 25*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
//         // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
//         // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
//       // }
//     });
//     $('.image-gallery-5 .image-container.five').each(function(){
//       // if($(this).offset().top < ($(document).scrollTop()+$(window).height()-100)){
//         $(this).css('transform', 'translate3d(0px, -'+( (42*$(window).scrollTop()/$(this).offset().top) > 65 ? 65: 42*$(window).scrollTop()/$(this).offset().top )+'vw, 0px)');
//         // jQuery(window).scroll(function(){jQuery('.image-container.one').css('transform', 'translate3d(0px, '+jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top+'px, 0px)')});
//         // console.log(jQuery(window).scrollTop()/jQuery('.image-container.one').offset().top)
//       // }
//     });
//   }
//   function introScroll(){
//     // if($(window).scrollTop() < $('.collection-intro-spacer').height()){
//     if(jQuery('.collection-intro .intro-image').height() != null){
//       $('.collection-intro .intro-image').width(100-($(window).scrollTop()/$(window).height()*100/2)+"%");
//       // }
//       if($('.collection-intro .intro-image').height() < $(window).height()){
//         $('.collection-intro .intro-image').addClass('fadeout');
//         if($('.collection-intro-text').offset().top < ($(document).scrollTop()+$(window).height())){
//           $('.title-wrapper').css('top', 'calc(50% + '+($('.collection-intro-text').offset().top-$(window).scrollTop()-$(window).height())+'px)');
//         }else{
//           $('.title-wrapper').css('top', 'calc(50%)');
//         }
//       }else{
//         $('.collection-intro .intro-image').removeClass('fadeout');
//       }
//     }

//   }


//   function elementsHeight(){
//     $('.page-layout-1column-prismic-new .heroSection').height($(window).height());
//     $('.page-layout-1column-prismic-new .backgroundImage').height($(window).height()*1.2);
//     $('.page-layout-1column-prismic-new main section.instagram .spaceBottom').height($(window).height()/2);

//   }

//   function smoothScroll(){
//     // $('.page-layout-1column-prismic-new .column.main .container').animate({});
//     var scroll = $(this).scrollTop() * -1;
//     var transform = 'translate3d(0px, '+scroll+'px, 0px)';
//     $('.page-layout-1column-prismic-new .column.main .container').animate().css({
//       '-webkit-transform': transform,
//       '-moz-transform': transform,
//       '-ms-transform': transform,
//       '-o-transform': transform,
//       'transform': transform,
//     });
//   }

//   function scrollCollectionPage(){
//       if($('.collectionPageLayout #change').length > 0){
//         if ($(this).scrollTop() < $('.collectionPageLayout #change').position().top - 250) {
//           $('body').removeClass('changeColor')
//         }
//         else{
//           $('body').addClass('changeColor')
//         }
//         } 
//       if($('.collectionPageLayout .durum').length > 0){
//         if(($(this).scrollTop()+$(window).height()) > $('.product_inner.full_collection.parallax_pull').position().top + ($('.product_inner.full_collection.parallax_pull').height()/2) ){
//           $('.after-collection').addClass('active');
//         }
//         else{
//           $('.after-collection').removeClass('active');
//         }
//         // if($(window).width() > 1000){
//         //   if($(this).scrollTop() >= $('.collectionPageLayout .durum').position().top + $('.collectionPageLayout .durum').outerHeight()+($('.product_inner.full_collection.parallax_pull').outerHeight() - $(window).height()) ){
//         //     // $('.full_collection_parallax').height($('.product_inner.full_collection.parallax_pull').outerHeight());
//         //     // $('.product_inner.full_collection.parallax_pull').addClass('active');
            
//         //   } 
//         //   else {
//         //     // $('.product_inner.full_collection.parallax_pull').removeClass('active');
//         //     // $('.full_collection_parallax').height(0);
//         //   }
//         // }
//         // else if ($(window).width() > 767 && $(window).width() < 1000 ){
//         //   if($(this).scrollTop() > $('.collectionPageLayout .durum').position().top + $('.collectionPageLayout .durum').outerHeight() - $(window).height() + $('.product_inner.full_collection.parallax_pull').outerHeight() ){
//         //     // $('.full_collection_parallax').height($('.product_inner.full_collection.parallax_pull').outerHeight());
//         //     // $('.product_inner.full_collection.parallax_pull').addClass('active');
            
//         //   } 
//         //   else {
//         //     // $('.product_inner.full_collection.parallax_pull').removeClass('active');
//         //     // $('.full_collection_parallax').height(0);
//         //   }
//         // } else {
//         //   if($(this).scrollTop() > $('.collectionPageLayout .durum').position().top + $('.collectionPageLayout .durum').outerHeight() - $(window).height() + $('.product_inner.full_collection.parallax_pull').outerHeight() ){
//         //     // $('.full_collection_parallax').height($('.product_inner.full_collection.parallax_pull').outerHeight());
//         //     // $('.product_inner.full_collection.parallax_pull').addClass('active');
            
//         //   } 
//         //   else {
//         //     // $('.product_inner.full_collection.parallax_pull').removeClass('active');
//         //     // $('.full_collection_parallax').height(0);
//         //   }
//         // }
//       }
//   }

//   function collectionElementHeight(){
//     $('.collectionPageLayout .parallaxEl').height($(window).height());
//     $('.collectionPageLayout .parallaxEl').height($(window).height()-250); 

//     if(checkForScreenSize() == 'mobile'){
//       $('.collectionPageLayout .product_inner.full_collection.parallax_pull').width($(window).width());
//       $('.collectionPageLayout .product_inner.full_collection.parallax_pull').css('margin-left', -($(window).width()*0.08));
//     }
//   }



//   function accordion() {

//     var acc = document.getElementsByClassName("accordion");
//     var i;

//     for (i = 0; i < acc.length; i++) {
//       acc[i].addEventListener("click", function() {
//         this.classList.toggle("active");

//         var panel = this.nextElementSibling;

//         if (panel.style.maxHeight) {
//           panel.style.maxHeight = null;
//         } else {
//           panel.style.maxHeight = panel.scrollHeight + "px";
//         } 
//       });
//     }

//   }

 

//   $(window).scroll(function(){
//     lazyLoadImages();
//     moveImages();
//     introScroll();
//     // smoothScroll();
//     if($('.page-layout-1column-prismic-new .product .newsletter').length > 0){
//       if ($(this).scrollTop() < $('.page-layout-1column-prismic-new .product .newsletter').position().top + $(window).height()/2) {
//         $('body').removeClass('changeColor')
//       }
//       else{
//         $('body').addClass('changeColor')
//       }
//     }

//     if($('.page-layout-1column-prismic-new .instagram').length > 0){
//       if ($(this).scrollTop() > $('.page-layout-1column-prismic-new .instagram').position().top) {
//         $('.page-layout-1column-prismic-new .backgroundImage, .page-layout-1column-prismic-new .background').css('visibility', 'hidden');
//       }
//       else{
//         $('.page-layout-1column-prismic-new .backgroundImage, .page-layout-1column-prismic-new .background').css('visibility', 'visible')
//       }
//     }
//     scrollCollectionPage();
//   });

//   jQuery(document).ready(function(){
//     checkForTouchDevice();
//     prismicHoverPosition();
//     prismicImageClick();
//     lazyLoadImages();
//     elementsHeight();
//     collectionElementHeight();
//     sheight = $(window).height();
//     // startPlayback();
//     // $('#over_video').click(function(){
//     //   startPlayback();
//     // });
//     // $('.page-layout-1column-prismic-new .containerSlideFix').height(
//     //   $('.page-layout-1column-prismic-new .column.main .container .heroSection').height() 
//     //   +
//     //   $('.page-layout-1column-prismic-new .column.main .container .product').height() 
//     //   +
//     //   $('.page-layout-1column-prismic-new .column.main .container .instagram').height() 
//     //   );
//     $('body.page-layout-1column-prismic-new').css('opacity', 1);
//     $('.collectionPageLayout #change').width($(window).width());
//     accordion();

//   });

//   jQuery(window).resize(function(){
//     sheight = $(window).height();
//     elementsHeight();
//     collectionElementHeight();
//   });
//   // window.onorientationchange = function() {
//   //   console.log("adsasd");
//   //   sheight = $(window).height();
//   //   elementsHeight();
//   //   collectionElementHeight();
//   // }

// });
