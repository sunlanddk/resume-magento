require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
  $ = jQuery;
  var supportsTouch = false;

  function checkForScreenSize(){
    if($(window).width() > 767){
      return 'desktop';
    }
    return 'mobile';
  }


  var lastScrollTop = 0;

  function headerVisible(){
    var st = $(this).scrollTop();
    // if (st > lastScrollTop){
    //   $('header').css('top', - $("header").outerHeight());
    // } 
    // else {
    //   $('header').css('top', 0);
    // }
    if (st > lastScrollTop && st > 0 ){
      // $('.mobileToggle').css('top', - 80);
      $('header, .categoriesMenuToggle, .gridChange').addClass('up');
    } 
    else {
      if(checkForScreenSize() == 'mobile'){
        // $('.mobileToggle').css('top', 30);
        $('header, .categoriesMenuToggle, .gridChange').removeClass('up');
      }
      else{
        // $('.mobileToggle').css('top', 50);
        $('header, .categoriesMenuToggle, .gridChange').removeClass('up');
      }
    }
    lastScrollTop = st;
  }

  
  $(window).scroll(function(){
    headerVisible();
  });


  jQuery(document).ready(function(){

  });



  // $(document).on('swatch.initialized', function(){
  //   //addToCartFromList();
  //   addToCartFromSinglePage();
  //   showSwatches();
  //   $('.swatch-attribute-options').find('.swatch-option').eq(0).addClass('selected');
  //   $('.super-attribute-select').val($('.swatch-attribute-options').find('.swatch-option').eq(0).attr('option-id'));
  //   el = $('.swatch-attribute-options .swatch-option.selected');
  //   selected = true;
  //   //$('.page-wrapper .column.main .products ol').addClass('show');
  // });

  jQuery(window).resize(function(){
  });


});
