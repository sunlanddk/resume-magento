// require(['jquery', 'jqueryui', 'slick', 'carouFredSel'], function($){
// $ = jQuery;
require([
        'jquery'
],
function ($) {
  function resizingImg() {
  let elWidth = $('.fullWidth-image_text .full-image').outerWidth();
  let elHeight = $('.fullWidth-image_text .full-image').height();
  let elRatio = elWidth/elHeight;

  let wHeight = $(window).height();
  let wWidth = $(window).width();

  if (wWidth > 767 ) {
    $('.fullWidth-image_text .full-image').css('left', ((elWidth-wWidth)*-1) );

    if( wWidth > elWidth ) {
      $('.fullWidth-image_text .full-image').css('width', '100%');
      $('.fullWidth-image_text .full-image').css('height', 'auto');
      $('.fullWidth-image_text .full-image').css('left', '0');
    }
  }
  
}

$(document).on('mousemove', function(e){
  if($(window).width() < 767){
    return;
  }
  var y = e.pageY - $(document).scrollTop();
  // var y = e.pageY;
  if(y < 45 ){
    $('.customMouse').fadeOut();
  }
  else{
    $('.customMouse').fadeIn();
  }

});

function playVideo() {
  document.getElementById("vid").play();
}

$(document).ready(function(){

    $( ".custom-cursor" ).mouseover(function() {
      if ( $(this).data('name') == '' ) {
        // $('#tracer').fadeOut();
        $('#tracer').hide();
        $('.customMouse').addClass('dax');
      } else {
        // $('#tracer').fadeIn();
        $('#tracer').show();
         if ( $( this ).hasClass('sale') ) {
            $('.customMouse').addClass('sale');
            $('.customMouse').removeClass('dax');
          } else {
            $('.customMouse').removeClass('sale');
            $('.customMouse').removeClass('dax');
          }
        $(".tracerText").text($(this).data('name'));  
      }
    });

    $('.instagram-feed').mouseover(function(e){
      var eWidth = $('.instagram-feed').width();
      var percent = 0;
      percent = e.pageX/eWidth;
      console.log(percent);
      if($(window).width() < 767){
        return;
      }
      $('.split-box.products').bind('mousewheel', function(e){
        $('.split-box.products').stop();
        return;
      });

      if(percent > 0.6){
        $('.split-box.products').stop();
        $('.split-box.products').animate( { scrollLeft: '+='+$('.split-box.products').width() }, $('.split-box.products').width()*4, 'linear');        
      }else if(percent < 0.4){
        $('.split-box.products').stop();
        $('.split-box.products').animate( { scrollLeft: '-='+$('.split-box.products').width() }, $('.split-box.products').width()*4, 'linear'); 
      }else{
        $('.split-box.products').stop();
      }
    });
    $('.instagram-feed').mouseleave(function(e){
      $('.split-box.products').stop();
    });

    $('.sliding-products').mouseover(function(e){
      var eWidth = $('.sliding-products').width();
      var percent = 0;
      percent = e.pageX/eWidth;
      console.log(percent);
      if($(window).width() < 767){
        return;
      }
      $('.split-box.sale').bind('mousewheel', function(e){
        $('.split-box.sale').stop();
        return;
      });

      if(percent > 0.6){
        $('.split-box.sale').stop();
        $('.split-box.sale').animate( { scrollLeft: '+='+$('.split-box.sale').width() }, $('.split-box.sale').width()*4, 'linear');        
      }else if(percent < 0.4){
        $('.split-box.sale').stop();
        $('.split-box.sale').animate( { scrollLeft: '-='+$('.split-box.sale').width() }, $('.split-box.sale').width()*4, 'linear'); 
      }else{
        $('.split-box.sale').stop();
      }
    });
    $('.sliding-products').mouseleave(function(e){
      $('.split-box.sale').stop();
    });
    // var instagramAnimation;
    // var instagramAnimationPos = 0;
    // var instagramAnimationDirection = 1;
    // var instagramAnimationWidth = 0;
    // var instagramAnimationpercent = 0;
    // var instagramAnimationSliderWidth = 0;
    // var instagramAnimationTotalWidth = 0;

    // $('.split-box.products').bind('mousewheel', function(e){
    //   // $('.split-box.products').stop();
    //   clearTimeout(instagramAnimation);
    // });

    // $('.instagram-feed').mousemove(function(e){
    //   instagramAnimationSliderWidth = 0;
    //   var instagramAnimationWidth = $('.instagram-feed').width();
    //   $('.split-box.products li').each(function(){
    //     instagramAnimationSliderWidth += $(this).width();
    //   });
    //   instagramAnimationTotalWidth = (instagramAnimationWidth*0.1) + instagramAnimationSliderWidth - $(window).width();
    //   instagramAnimationpercent = 0.5-e.pageX/instagramAnimationWidth;
    //   instagramAnimationPos = $('.split-box.products').scrollLeft();

    //   if($(window).width() < 767){
    //     return;
    //   }
      

    //   if(instagramAnimationpercent > 0.5){
    //     instagramAnimationDirection = 1;
    //     // $('.split-box.products').stop();
    //     // $('.split-box.products').animate( { scrollLeft: '+='+$('.split-box.products').width() }, $('.split-box.products').width()*4, 'linear');        
    //   }else if(instagramAnimationpercent < 0.5){
    //     instagramAnimationDirection = -1;
    //     // $('.split-box.products').stop();
    //     // $('.split-box.products').animate( { scrollLeft: '-='+$('.split-box.products').width() }, $('.split-box.products').width()*4, 'linear'); 
    //   }else{
    //     // $('.split-box.products').stop();
    //   }
    // });
    // $('.instagram-feed').mouseenter(function(e){
      
    //   instagramAnimation = setInterval(function(){ 
        
    //     instagramAnimationPos-= 55*instagramAnimationpercent; 
    //     if(instagramAnimationPos > instagramAnimationTotalWidth){
    //       instagramAnimationPos = instagramAnimationTotalWidth;
    //     }
    //     if(instagramAnimationPos < 0){
    //       instagramAnimationPos = 0;
    //     }
    //     $('.split-box.products').scrollLeft(instagramAnimationPos); 
    //   }, 100);
    // });

    // $('.instagram-feed').mouseleave(function(e){
    //   clearTimeout(instagramAnimation);
    // });
      
    resizingImg();  
    playVideo();
      
    window.onresize = function() {
      resizingImg();
    }

  });

// $(document).on('mousemove', function(e){

//   if($(window).width() < 767){
//     return;
//   }
//   let cursorWidth = $('.tracerText').outerWidth();
//   // var x = e.pageX - cursorWidth/2;
//   var x = e.pageX;
//   var y = e.pageY;
//   if(y < 85 ){
//     // y = 85;
//     $('.customMouse').fadeOut();
//   }
//   else{
//     $('.customMouse').fadeIn();
//   }
  
//   $('.customMouse').css({
//      left:  x,
//      top:   y
//   });

// });

$(document).on('mousemove', function(e){
    if($(window).width() < 767){
      return;
    }
    let cursorWidth = $('.tracerText').outerWidth();
    var x = e.pageX - cursorWidth/2;
    var y = e.pageY - $(document).scrollTop();
    $('.customMouse').css({
       left:  x,
       top:   y
    });



  });
});

