<?php 
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$appState = $objectManager->get("Magento\Framework\App\State");
$appState->setAreaCode('frontend');

?>
<div class="bg">
<!-- 	<picture>
		<source media="(max-width: 767px)" srcset="http://turndigital.prod41.magentohotel.dk/resumecph/bgc3.jpg 1x, http://turndigital.prod41.magentohotel.dk/resumecph/bgc3.jpg 2x, http://turndigital.prod41.magentohotel.dk/resumecph/bgc3.jpg 3x">
		<source  media="(max-width: 768px)" srcset="http://turndigital.prod41.magentohotel.dk/resumecph/bgc2.jpg 1x, http://turndigital.prod41.magentohotel.dk/resumecph/bgc2.jpg 2x, http://turndigital.prod41.magentohotel.dk/resumecph/bgc2.jpg 3x">
		<source srcset="http://turndigital.prod41.magentohotel.dk/resumecph/bgc1.jpg 1x, http://turndigital.prod41.magentohotel.dk/resumecph/bgc1.jpg 2x, http://turndigital.prod41.magentohotel.dk/resumecph/bgc1.jpg 3x">
		<img class="move" src="http://turndigital.prod41.magentohotel.dk/resumecph/bgc1.jpg" alt="">
	</picture> -->
</div>

<div class="locationChooser">
	<div class="logoContainer">
		<img src="http://turndigital.prod41.magentohotel.dk/resumecph/logo_white.png" alt="">
		<p>Please select your location</p>
	</div>
	<div class="locations">
		<a href="http://turndigital.prod41.magentohotel.dk/resumecph/da">Denmark</a>
		<a href="http://turndigital.prod41.magentohotel.dk/resumecph/eu">Europe</a>
		<a href="http://turndigital.prod41.magentohotel.dk/resumecph/us">Global</a>
		<a href="http://turndigital.prod41.magentohotel.dk/resumecph/no">Norway</a>
		<a href="http://turndigital.prod41.magentohotel.dk/resumecph/se">Sweden</a>
		<a href="http://turndigital.prod41.magentohotel.dk/resumecph/uk">United Kingdom</a>
	</div>
</div>

<style>
	html, body{
		padding: 0;
		margin: 0;
		height: auto;
	}
	.bg1, .bg2, .bg3{
		position: fixed;
		left: 0;
		top: 0;
		height: 100%;
		width: 100%;
  		background: url('../images/bgc1.jpg') no-repeat center center fixed; 
  		-webkit-background-size: cover;
  		-moz-background-size: cover;
  		-o-background-size: cover;
	  	background-size: cover;
	}
	.bg3, .bg2{
			display: none;
	}
	
	@media all and (max-width: 768px){
		.bg1{
			display: none;
		}
		.bg2{
	  		background: url(bgc2.jpg) no-repeat center center fixed; 
	  		-webkit-background-size: cover;
	  		-moz-background-size: cover;
	  		-o-background-size: cover;
		  	background-size: cover;
		}
	}

	@media all and (max-width: 767px){
		.bg2{
			display: none;
		}
		.bg3{
	  		background: url(bgc3.jpg) no-repeat center center fixed; 
	  		-webkit-background-size: cover;
	  		-moz-background-size: cover;
	  		-o-background-size: cover;
		  	background-size: cover;
		}
	}

</style>
<?php 

// $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
// $storeManager = $objectManager->Create('\Magento\Store\Model\StoreRepository');    
// $cateinstance = $objectManager->create('Magento\Catalog\Model\CategoryFactory');
// $category = $objectManager->create('Magento\Catalog\Model\Category')->load(3)->setStore(1);
// $subCats = $category->getChildrenCategories();

// foreach($subCats as $category){
// 	$allcategoryproduct = $cateinstance->create()->load($category->getId())->getProductCollection()->addAttributeToSelect('*');
//     echo $category->getId().'--'.$category->getName().' --- '.$allcategoryproduct->count();
//     echo '<br>';

// }


// print_r(randomnumbers(20, 7));



// function randomnumbers($_max, $_amount){
// 	$array = array();
// 	while(count($array) !== $_amount){
// 		$rand = rand(1,$_max);
// 		$array[$rand] = $rand;
// 	}
// 	return $array;
// }

?>
<div class="bg3" style='background: url("{{media url=&quot;tempprodimg/bgc3.jpg&quot;}}") no-repeat center center fixed; '></div>
<div class="bg2" style='background: url("{{media url=&quot;tempprodimg/bgc2.jpg&quot;}}") no-repeat center center fixed; '></div>
<div class="bg1" style='background: url("{{media url=&quot;tempprodimg/bgc1.jpg&quot;}}") no-repeat center center fixed; '></div>
<div class="locationChooser">
<div class="logoContainer"><img src="{{media url=&quot;tempprodimg/logo_white.png&quot;}}" width="650" height="100">
<p>Please select your location</p>
</div>
<div class="locations">
<a href="http://turndigital.prod41.magentohotel.dk/resumecph/da">Denmark</a> <a href="http://turndigital.prod41.magentohotel.dk/resumecph/eu">Europe</a> <a href="http://turndigital.prod41.magentohotel.dk/resumecph/us">Global</a> <a href="http://turndigital.prod41.magentohotel.dk/resumecph/no">Norway</a> <a href="http://turndigital.prod41.magentohotel.dk/resumecph/se">Sweden</a> <a href="http://turndigital.prod41.magentohotel.dk/resumecph/uk">United Kingdom</a></div>
