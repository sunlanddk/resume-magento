  <html>
<head>
	<meta charset="UTF-8">
	<title>RÉSUMÉ</title>
	<link rel="stylesheet" href="//fast.fonts.net/cssapi/77def3ea-aec9-4716-8d2f-d6161ec4cffa.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<style>
		
		.bg2{
			display: none;
		}
		.bg3{
			display: none;
		}
		header{
			width: 100%;
			max-width: 1250px;
			padding-left: 3.5%;
    		padding-right: 3.5%;
    		margin: auto;
    		overflow: hidden;
    		margin-bottom: 35px;
		}
		header img{
			width: 300px;
			float: left;
    		height: auto;
		}
		.container{
			width: 100%;
			max-width: 500px;
			padding-left: 3.5%;
    		padding-right: 3.5%;
    		margin: auto;
    		overflow: hidden;
    		position: relative;
    		z-index: 99999;
    		display: table;
    		height: 100%;
    		text-align: center;
		}
		.left{
			width: 50%;
			float: left;
			z-index: 999;
			position: relative;
		}
		.left img{
			width: 100%;
			height: auto;
		}
		.right{
			float: left;
			width: calc(50% - 3.5%);
			background: #e1d0c0;
			position: absolute;
			right: 3.5%;
			top: 0;
			bottom: 0;
			z-index: 1;
			color: #2a3b8f;
			padding: 3.5%;
		}
		h2{
			margin-top: 0;
			text-transform: uppercase;
			/*font-family: 'Georgia W01 Regular';*/
			font-family: 'Avenir LT W01_45 Book1475508' !important;
			display: table-cell;
			vertical-align: middle;
			color: white;
			font-size: 18px;
    		line-height: 25px;
		}
		h3{
			margin-top: 0;
			text-transform: uppercase;
			font-family: 'Avenir LT W01_45 Book1475508' !important;
		}
		*{
			box-sizing: border-box;
		}
		@media all and (max-width: 768px){
			.left{
				display: none;
			}
			.right{
				position: relative;
				height: 500px;
				width: 100%;
				right: 0;
				left: 0;
				padding: 10%;
			}
			.bg1{
			display: none;
			}
			.bg2{
				display: block;
			}
			.bg3{
				display: none;
			}
		}

		@media all and (max-width: 767px){
			.bg1{
			display: none;
			}
			.bg2{
				display: none;
			}
			.bg3{
				display: block;
			}
		}
	</style>

	<!-- <header><img src="https://www.resumecph.dk/pub/static/frontend/WhitePixel/resume/da_DK/images/logo.png" alt="Magento Commerce" width="500" height="75"></header> -->
	<section class="container">
		<h2>New features launching soon. Follow us on instagram @resumecph and we will keep you posted. Stay tuned</h2>
	</section>

	<div class="bg1" style="position: fixed;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    background: url(https://www.resumecph.dk/bgc1.jpg) no-repeat center center fixed;
    background-size: cover;"></div>
	<div class="bg2" style="position: fixed;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    background: url(https://www.resumecph.dk/bgc2.jpg) no-repeat center center fixed;
    background-size: cover;"></div>
	<div class="bg3" style="position: fixed;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    background: url(https://www.resumecph.dk/bgc3.jpg) no-repeat center center fixed;
    background-size: cover;"></div>
	

</body>
</html>

<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// require_once 'processorFactory.php';

// $processorFactory = new \Magento\Framework\Error\ProcessorFactory();
// $processor = $processorFactory->createProcessor();
// $response = $processor->process503();
// $response->sendResponse();

