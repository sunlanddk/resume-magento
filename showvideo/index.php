<?php
  date_default_timezone_set('Europe/Copenhagen');
  $date = strtotime("February 3, 2021 9:30 AM");
  // $date = strtotime("January 26, 2021 10:25 PM");
  $remaining = $date - time();
  $days_remaining = floor($remaining / 86400);
  $hours_remaining = floor(($remaining % 86400) / 3600);
  $minuttes_remaining = floor(($remaining % 3600) / 60);
  $sec_remaining = floor(($remaining % 60));
  $now = $remaining <= 0 ? true:false;
  $now = isset($_GET['snyd']) ? true:$now;
?>

<html>
<head>
  <title>Résumé Video</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" type="image/x-icon" href="https://www.resumecph.dk/pub/media/favicon/default/fav.png">
  <style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,700|Roboto:300,400,500&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Libre+Baskerville&display=swap');
    body{
      background-color: #041fe2;
      height: 100vh;
      margin: 0;
      padding: 0;
    }
    h1{
      color: white;
      font-family: "Oswald",sans-serif;
      font-weight: 700 !important;
      font-size: 60px;
      margin: 0;
      text-align: center;
    }
    h2{
      color: white;
      text-align: center;
      font-size: 20px;
      font-family: "Libre Baskerville",serif;
      margin-bottom: 10px;
      font-weight: 400;
    }
    p{
      color: white;
      text-align: center;
      font-size: 31px;
      font-family: "Libre Baskerville",serif;
      margin: 0;
      font-weight: 400;
    }
    .desktop{display: block;}
    .mobile{display: none;}
    @media (max-width: 767px){
      .desktop{display: none;}
      .mobile{display: block; width: auto;}
    }
    .wrapper{
      display: flex;
      height: 100%;
    }
    .inner{
      margin: auto;
    }
  </style>
</head>
<body>
<div class="wrapper">
  <div class="inner">
    <?php if ($now): ?>
      <iframe class="mobile" src="https://player.vimeo.com/video/505443418" width="640" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
      <iframe class="desktop" src="https://player.vimeo.com/video/505443604" width="1000" height="600" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
    <?php else: ?>
      <h1>RÉSUMÉ</h1>
      <h2>Come back in</h2>
      <p><?php echo $days_remaining . ' days ' . sprintf('%02d', $hours_remaining) .':'. sprintf('%02d', $minuttes_remaining) . ' hours'; ?></p>
    <?php endif; ?>
  </div>
</div>
</body>
</html>