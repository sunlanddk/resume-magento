<?php 

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$appState = $objectManager->get("Magento\Framework\App\State");
$appState->setAreaCode('frontend');

$mediaDir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList')->getPath('media');// Im Magento 2
$fpath = '/var/www/resumecph.dk/public_html/td_inc/image_upload/';// path to your file
$tempcatimg = $mediaDir.'/tempprodimg';
// $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');

// $productId = 19;
// $sku = '12345_650';

// $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
// $productRepository = $objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
// $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
// foreach ($existingMediaGalleryEntries as $key => $entry) {
//     unset($existingMediaGalleryEntries[$key]);
// }
// $product->setMediaGalleryEntries($existingMediaGalleryEntries);
// $productRepository->save($product);


	$StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
	$productRepository = $objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
  $stockRegistry = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');
	$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
	// $collection = $productCollection->setFlag('has_stock_status_filter', true)->load();
	$collection = $productCollection->addAttributeToSelect('season_id')->addAttributeToFilter('season_id', ['eq'=>'32'])->addAttributeToFilter('type_id', ['eq'=>'configurable']);



$products = array();
foreach ($collection as $key => $value) {
	array_push($products, array("id" => $value->getId(), "sku" => $value->getSku(), "productObj" => $value));
}

if ($handle = opendir($fpath)) {
    while (false !== ($file = readdir($handle))) {
        if ('.' === $file) continue;
        if ('..' === $file) continue;

        $fileParts = explode("_", $file);
        $fileName = ltrim($fileParts[0],'010')."-".$fileParts[1];
        echo $fileName;
        echo PHP_EOL;
				echo PHP_EOL;
        $productKey = array_search($fileName,  array_column($products, 'sku'));
        if($productKey != FALSE){
					$imagePath = save_image( $file, $objectManager, $tempcatimg, $fpath); // path of the image
					$products[$productKey]['productObj']->addImageToMediaGallery($imagePath, null, false, false);
					$products[$productKey]['productObj']->save();
					$productUpdated = $objectManager->create('Magento\Catalog\Model\Product')->load($products[$productKey]['id']);
	    		$images = $productUpdated->getMediaGalleryImages();
      		foreach ($images as $key => $image) {
						if(strpos($image->getFile(), '_1.') !== false){
							$productUpdated->setImage($image->getFile());
							$productUpdated->setSmallImage($image->getFile());
							$productUpdated->setBigImageThird($image->getFile());
						}
						if(strpos($image->getFile(), '_2.') !== false){
							$productUpdated->setThumbnail($image->getFile());
							$productUpdated->setBigImage($image->getFile());
						}
						if(strpos($image->getFile(), '_3.') !== false){
							$productUpdated->setBigImageSecond($image->getFile());
						}
					}
					$productUpdated->save();
					unlink($fpath.$file);
					unlink($mediaDir.$file);
					echo "delete file: " . $fpath.$file . "& " . $mediaDir.'/'.$file;
					echo PHP_EOL;
					echo PHP_EOL;
        }else{
        	continue;
        }
    }
    closedir($handle);
}

//// OLD ///
die();

	foreach ($collection as $key => $value) {
		echo $_productId = $value->getId();
	  	if($value->getTypeId() == 'configurable' ){
	    	$_productId = $value->getId();
	    	$_productSku = str_replace('-','_',$value->getSku());

	    	if(@file_get_contents($fpath.$_productSku.'_1.jpg') !== false){
	    		// Add Images To The Product
				$imagePath = save_image( $_productSku.'_1.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
				$value->addImageToMediaGallery($imagePath, null, false, false);
				$value->save();
	    	}
	    	if(@file_get_contents($fpath.$_productSku.'_2.jpg') !== false){
	    		// Add Images To The Product
				$imagePath = save_image( $_productSku.'_2.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
				$value->addImageToMediaGallery($imagePath, null, false, false);
				$value->save();
	    	}
	    	if(@file_get_contents($fpath.$_productSku.'_3.jpg') !== false){
	    		// Add Images To The Product
				$imagePath = save_image( $_productSku.'_3.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
				$value->addImageToMediaGallery($imagePath, null, false, false);
				$value->save();
	    	}
	   //  	if(@file_get_contents($fpath.$_productSku.'_4.jpg') !== false){
	   //  		// Add Images To The Product
				// $imagePath = save_image( $_productSku.'_4.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
				// $value->addImageToMediaGallery($imagePath, null, false, false);
				// $value->save();
	   //  	}
	   //  	if(@file_get_contents($fpath.$_productSku.'_5.jpg') !== false){
	   //  		// Add Images To The Product
				// $imagePath = save_image( $_productSku.'_5.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
				// $value->addImageToMediaGallery($imagePath, null, false, false);
				// $value->save();
	   //  	}
	   //  	if(@file_get_contents($fpath.$_productSku.'_6.jpg') !== false){
	   //  		// Add Images To The Product
				// $imagePath = save_image( $_productSku.'_6.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
				// $value->addImageToMediaGallery($imagePath, null, false, false);
				// $value->save();
	    	// }

	    	$productUpdated = $objectManager->create('Magento\Catalog\Model\Product')->load($_productId);
	    	$images = $productUpdated->getMediaGalleryImages();
			foreach ($images as $key => $image) {
				if(strpos($image->getFile(), '_1.') !== false){
					$productUpdated->setImage($image->getFile());
					$productUpdated->setSmallImage($image->getFile());
				}
				if(strpos($image->getFile(), '_2.') !== false){
					$productUpdated->setThumbnail($image->getFile());
					$productUpdated->setBigImage($image->getFile());
					// $productUpdated->setImageBehind($image->getFile());
				}
				if(strpos($image->getFile(), '_3.') !== false){
					$productUpdated->setBigImageSecond($image->getFile());
				}
				if(strpos($image->getFile(), '_4.') !== false){
					$productUpdated->setBigImageThird($image->getFile());
				}
				if(strpos($image->getFile(), '_5.') !== false){
					$productUpdated->setDetailImage($image->getFile());
				}
				if(strpos($image->getFile(), '_6.') !== false){
					
				}
			}
			$productUpdated->save();

			echo $_productSku;
			echo PHP_EOL;
			echo PHP_EOL;
		}

	}


function save_image($img,$objectManager,$mediaDir, $fpath) {
	// $imageFilename = basename($img);
	$imageFilename = $img;
	$image_type = substr(strrchr($imageFilename,'.'),1); //find the image extension
	echo $filepath = $mediaDir . '/'. $imageFilename; //path for temp storage folder: pub/media
	echo PHP_EOL;
	echo PHP_EOL;
	$ggh = file_get_contents($fpath.$img);
	$fp = fopen($filepath, 'w');
	fwrite($fp, $ggh);
	fclose($fp);
	// file_put_contents($filepath, file_get_contents(trim($img))); //store the image from external url to the temp storage folder
	return $filepath;
}



die();

// Add Images To The Product
// $mediaAttribute = array('thumbnail','small_image','image');
// $imagePath = save_image( $sku.'_3.jpg', $objectManager, $tempcatimg, $fpath); // path of the image
// $product->addImageToMediaGallery($imagePath, array('small_image'), false, false);
// $product->save();

// $images = $product->getMediaGalleryImages();
// foreach ($images as $key => $image) {
// 	if(strpos($image->getFile(), '_1.') === true){
// 		$product->setImage($image->getFile());
// 		$product->setSmallImage($image->getFile());
// 		$product->setThumbnail($image->getFile());	
// 	}
// 	if(strpos($image->getFile(), '_2.') === true){
// 		$product->setImageBehind($image->getFile());
// 	}
// 	if(strpos($image->getFile(), '_3.') === true){
// 		$product->setDetailImage($image->getFile());
// 	}
// 	if(strpos($image->getFile(), '_4.') === true){
// 		$product->setBigImage($image->getFile());
// 	}
// 	if(strpos($image->getFile(), '_5.') === true){
// 		$product->setBigImageSecond($image->getFile());
// 	}
// 	if(strpos($image->getFile(), '_6.') === true){
// 		$product->setBigImageThird($image->getFile());
// 	}
// }

// $test = $product->save();


// echo 'imageSaved';
// echo PHP_EOL;


// function saveAsMain($_product){

// }


