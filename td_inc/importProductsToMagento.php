<?php
$apiUrl = "https://www.resumecph.dk";
// $apiUrl = "http://turndigital.prod41.magentohotel.dk/resumecph";
$serverPath = '/home/resumecph_dk/public_html/td_inc';

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$appState = $objectManager->get("Magento\Framework\App\State");
$appState->setAreaCode('frontend');


if(!file_exists($serverPath.'/cronRunning.flag')){ 
	// Create / Update season
	if(file_exists($serverPath.'/seasoncreate.json')){
		$productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
		$productAction = $objectManager->get('\Magento\Catalog\Model\Product\Action');
		$myfile = fopen($serverPath."/cronRunning.flag", "w");
		fwrite($myfile, 'Cron kører');
		fclose($myfile);

		$time = time();
		$dateTime =  date("d/m/y - h:i:s",$time);
		$starttime = microtime(true);
	    $time = time();
	    $dateTime =  date("d/m/y - h:i:s",$time);

		$token = 'ylb0q43hoygoehyxf1oyquhjpb1kdh7x';

	    $fromPotJson = file_get_contents($serverPath."/seasoncreate.json");
	    $productArray = json_decode($fromPotJson);
	    $statusReturn = array(
        	'success' => array(),
        	'error'   => array(),
      	);

		// Get list of attributes from magento
		$ch = curl_init($apiUrl."/rest/all/V1/products/attribute-sets/9/attributes");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
		$attrListJson = curl_exec($ch);
		$attributeList = json_decode($attrListJson);
		curl_close($ch);

		$sizesArray = array();
		$colorsArray = array();

		// Loop through and get sizes and colors that are not in magento yet.
		foreach ($productArray as $key => $value){
		    $colors = $value->colors;
		    foreach ($colors as $color => $arrayvalue){
		      	$colorsArray[$color] = array('id'=>$color, 'name'=>$arrayvalue->colorName->default);
		      	$sizes = $arrayvalue->sizes;
		      	foreach ($sizes as $sizeId => $sizevalue){
		        	$sizesArray[$sizeId] = array('id'=>$sizeId, 'name'=>$sizeId);
		      	}
		    }
		}

		/// Create size if not in magento
		foreach ($sizesArray as $ckey => $cvalue) {
		    // Get size from pot and magento 
		    $sizeLabel = strtolower($ckey);
		    // check if color exists if not create color in magento
		    foreach($attributeList as $attrGroup){
		     	if($attrGroup->attribute_code == 'size'){
		       		$attributes = $attrGroup->options;
		       		$exists = 'false';
		       		foreach ($attributes as $key => $val) {
		         		if ((string) $val->label == (string) $sizeLabel) {
		          			$exists = 'true';
		          			// echo 'Denne størrelse findes allerede: '. $sizeLabel;
		          			// echo PHP_EOL;
		          			// echo PHP_EOL;
		        		}
		       		}
		       		if($exists == 'false'){
		        		// echo 'Denne størrelse findes ikke: '. $sizeLabel;
		        		// echo PHP_EOL;
		         		$attrOption = array(
		           		'label' => $sizeLabel,
		         		);
		         		$attrOption = json_encode(array('option' => $attrOption));              
		         		$ch = curl_init($apiUrl."/index.php/rest/V1/products/attributes/size/options");
		         		curl_setopt($ch, CURLOPT_POSTFIELDS, $attrOption);
		         		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		         		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		         		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
		         		$result = curl_exec($ch);
		         		$result = json_decode($result);
		         		curl_close($ch);
				        if($result == 1){
				           // echo $sizeLabel . ' has been created succesfully';
		           		// 	echo PHP_EOL;
		           		// 	echo PHP_EOL;
		         		} 
		        	}
		      	}
		    }    
		}

		/// Create color if not in magento
		foreach ($colorsArray as $ckey => $cvalue) {
		    $colorId = $ckey;

		    // check if color exists if not create color in magento
		    foreach($attributeList as $attrGroup){
		    	if($attrGroup->attribute_code == 'color'){
		       		$attributes = $attrGroup->options;

		       		$exists = 'false';
		       		foreach ($attributes as $key => $val) {
		        		// if ((string) $val->label == (string) $colorId) {
		        		if ((string) $val->label == (string) $cvalue['name']) {
		           			$exists = 'true';
		           			// echo 'Denne farve findes allerede: '. $cvalue['name'];
		           			// echo PHP_EOL;
		           			// echo PHP_EOL;
		         		}
		       		}
		       		if($exists == 'false'){
		        		$colArr = array();
		        		array_push($colArr, array('store_id' => 0, 'label' => $cvalue['name'] ) );

		         		$attrOption = array(
		           			'label' => utf8_decode($colorId),
		           			'store_labels'   => $colArr,
		        		 );
				        // echo 'Denne farve findes ikke: '. $cvalue['name'];
				        // echo PHP_EOL;

		         		$attrOption = json_encode(array('option' => $attrOption));              
		         		$ch = curl_init($apiUrl."/index.php/rest/all/V1/products/attributes/color/options");
		         		curl_setopt($ch, CURLOPT_POSTFIELDS, $attrOption);
		         		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		         		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		         		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
		         		$result = curl_exec($ch);
				        $result = json_decode($result);
		         		curl_close($ch);
		         		if($result == 1){
		           			// echo $cvalue['name'] . ' has been created succesfully';
		           			// echo PHP_EOL;
		           			// echo PHP_EOL;
		         		} 
		       		}
		     	}
		    } 
		}

		// Get new list of attributes from magento, with the newly added attributes
		$ch = curl_init($apiUrl."/rest/all/V1/products/attribute-sets/9/attributes");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
		$attrListJson = curl_exec($ch);
		$attributeList = json_decode($attrListJson);
		curl_close($ch);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		function createSimpleProduct($_storeCode, $_name, $_sku, $_currency, $_size, $_datetime, $_qty, $_isinstock, $_apiUrl, $_token, $_seasonId, $_collectionmemberid, $_status){
 			$simpleProduct = array(
            	'sku'               => $_sku,
             	'name'              => $_name,
             	'visibility'        => 1,
             	'type_id'           => 'simple',
             	'price'             => $_currency,
             	'status'            => $_status,
             	'attribute_set_id'  => 4,
             	'custom_attributes' => array(
                	array( 'attribute_code' => 'size', 'value' => $_size ),
                	array( 'attribute_code' => 'new_product', 'value' => $_datetime ),
                	array( 'attribute_code' => 'season_id', 'value' => $_seasonId ),
                	array( 'attribute_code' => 'colmemberid', 'value' =>  $_collectionmemberid),
               	),
             	'extensionAttributes' => array(
               		'stockItem'   => array('qty' => $_qty, "isInStock" => $_isinstock, "manageStock" => true,)
             	),
         	);

         	$simpleProduct = json_encode(array('product' => $simpleProduct));

          	$ch = curl_init($_apiUrl."/index.php/rest/".$_storeCode."/V1/products");
          	curl_setopt($ch, CURLOPT_POSTFIELDS, $simpleProduct);
          	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $_token));
          	$result = curl_exec($ch);
          	$result = json_decode($result);
          	return $result->id;
          	// print_r($result);
          	// die();
          	
 		}

 		function createSimpleProgmatically($_object_Manager, $_name, $_sku, $_price, $_size, $_qty, $_isinstock, $_seasonId, $_collectionmemberid, $_dateTime, $_collectionName){
 			$set_product = $_object_Manager->create('\Magento\Catalog\Model\Product');
	 			$set_product->setWebsiteIds(array(1,2,3,4,5,6));
			    $set_product->setAttributeSetId(4);
			    $set_product->setTypeId('simple');
			    $set_product->setCreatedAt(strtotime('now')); 
			    $set_product->setName($_name); 
			    $set_product->setSku($_sku);
			    $set_product->setStatus(1);
			    $set_product->setVisibility(1); 
			    $set_product->setSeasonId($_seasonId); 
			    $set_product->setColmemberid($_collectionmemberid);
			    $set_product->setNewProduct($_dateTime);
			    $set_product->setCollectionName($_collectionName);
			    $set_product->setSize($_size);
			    $set_product->setPrice($_price) ;
			    $set_product->setStockData(
			        array(
			        'use_config_manage_stock' => 0, 
			        // checkbox for 'Use config settings' 
			        'manage_stock' => 1, // manage stock
			        'is_in_stock' => $_isinstock, // Stock Availability of product
			        'qty' => $_qty // qty of product
			        )
			    );
			    $set_product->save();
			    // get id of product
			    return $get_product_id = $set_product->getId();
 		}


 		function createConfigurableProuct($_sku, $_name, $_currency, $_url, $_color, $_dateTime, $_sizeOption, $_simpleProductIds, $_apiUrl, $_token, $_seasonId, $_description, $_storeCode, $_collectionmemberid, $_collectionName, $_status = 1){

 			$confProductArray = array(
                 'sku'               => $_sku,
                 'name'              => $_name,
                 // 'description'       => $_description,
                 'visibility'        => 1, /*'catalog',*/
                 'type_id'           => 'configurable',
                 'price'             => $_currency,
                 'status'            => $_status,
                 'attribute_set_id'  => 9,
                 'custom_attributes' => array(
                     array( 'attribute_code' => 'has_options', 'value' => 1 ),
                     array( 'attribute_code' => 'url_key', 'value' => $_url ),
                     array( 'attribute_code' => 'color', 'value' => $_color ),
                     array( 'attribute_code' => 'new_product', 'value' => $_dateTime ),
                     array( 'attribute_code' => 'category_ids', 'value' => [3] ),
                     array( 'attribute_code' => 'season_id', 'value' =>  $_seasonId),
                     array( 'attribute_code' => 'colmemberid', 'value' =>  $_collectionmemberid),
                     array( 'attribute_code' => 'description', 'value' => $_description ),
                     array( 'attribute_code' => 'collection_name', 'value' => $_collectionName ),
                     
                   ),
                 'extensionAttributes' => array(
                   'stockItem'   => array("isInStock" => true),
                   'configurable_product_options'   => array(
                     'values' => array(
                       'attribute_id'  => 138,
                       'label'         => 'Size',
                       'position'      => 0,
                       'values'        => $_sizeOption,
                     ),
                   ),
                   'configurable_product_links'    => $_simpleProductIds,
                 ),
               );
               $confProductArray = json_encode(array('product' => $confProductArray));
               
                $ch = curl_init($_apiUrl."/index.php/rest/".$_storeCode."/V1/products");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $confProductArray);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $_token));
                $result = curl_exec($ch);
                $res = json_decode($result);
                return $res->id;
 		}

		foreach ($productArray as $key => $value) {
            $confSku = (int)$key;
            $colors = $value->colors;
            $name = $value->name->default;
            $description = '';
            foreach ($colors as $color => $arrayvalue) {
                $colorKey = $color;
                $colorName = $arrayvalue->colorName->default;
                $seasonId = $arrayvalue->season_id;
                $statusReturn['success']['season'] = $seasonId;
                $collectionmemberid = $arrayvalue->collection_member_id;
                $collectionName = $arrayvalue->collectionName;
                $ch = curl_init($apiUrl."/rest/V1/products/".$confSku."-".$colorKey."");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
                $result = curl_exec($ch);
                $result = json_decode($result);
                // curl_close($ch);
                if(isset($result->sku)){
                // if(false){
                    // echo 'Denne findes allerede: '. $confSku . '-' . $colorKey;
                    // echo PHP_EOL;
                }
                else{
                	//echo "'".$confSku . '-' . $colorKey."', ";
	           		// echo 'Denne findes ikke allerede: '. $confSku . '-' . $colorKey;
	           		// echo PHP_EOL;
	           		
	           		$simpleProductIds = array();
	           		$sizes = $arrayvalue->sizes;
	           		$lastPrices = '';
	           		foreach ($sizes as $sizeId => $sizevalue) {
	           			$lastPrices = $sizevalue;
	                    // Get the id of the size from magento
				        foreach($attributeList as $attrGroup){
				         	if($attrGroup->attribute_code == 'size'){
				           		$attributes = $attrGroup->options;
				           		foreach ($attributes as $key => $val) {
				             		if($key !== 0){
				             			$sizeId = strtolower($sizeId);
				               			if ((string) $val->label == (string) $sizeId) {
				                 			$size = $val->value;
				               			}
				             		}
				           		}
				         	}
				        }
	             		$qty = $sizevalue->QTY;
	             		$isInStock = false;
	             		if($qty > 0){
	               			$isInStock = true;
	             		}

	             		// $resultId = createSimpleProduct('all', $name, $sizevalue->sku, $sizevalue->DKK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             		// 	array_push($simpleProductIds, $resultId);
             			$resultId = createSimpleProgmatically($objectManager, $name, $sizevalue->sku, $sizevalue->DKK, $size, $sizevalue->QTY, $isInStock, $seasonId, $collectionmemberid, $dateTime, $collectionName);
             			array_push($simpleProductIds, $resultId);

	             		if(isset($sizevalue->DKK)){
	             			$productIds = array($resultId);
							$data = ['price'=>$sizevalue->DKK];
							$productAction->updateAttributes($productIds, $data, 1);
	             			// createSimpleProduct('da', $name, $sizevalue->sku, $sizevalue->DKK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             			
	             		}
	             		else{
	             			$productIds = array($resultId);
							$data = ['price'=>9999, 'status' => 0];
							$productAction->updateAttributes($productIds, $data, 1);
	             			// createSimpleProduct('da', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
	             		}
	             		if(isset($sizevalue->EUR)){
	             			$productIds = array($resultId);
							$data = ['price'=>$sizevalue->EUR];
							$productAction->updateAttributes($productIds, $data, 2);
	             			// createSimpleProduct('eu', $name, $sizevalue->sku, $sizevalue->EUR, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             		}
	             		else{
	             			$productIds = array($resultId);
							$data = ['price'=>9999, 'status' => 0];
							$productAction->updateAttributes($productIds, $data, 2);
	             			// createSimpleProduct('eu', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
	             		}
	             		if(isset($sizevalue->NOK)){
	             			$productIds = array($resultId);
							$data = ['price'=>$sizevalue->NOK];
							$productAction->updateAttributes($productIds, $data, 4);
	             			// createSimpleProduct('no', $name, $sizevalue->sku, $sizevalue->NOK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             		}
	             		else{
	             			$productIds = array($resultId);
							$data = ['price'=>9999, 'status' => 0];
							$productAction->updateAttributes($productIds, $data, 4);
	             			// createSimpleProduct('no', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
	             		}
	             		if(isset($sizevalue->USD)){
	             			$productIds = array($resultId);
							$data = ['price'=>$sizevalue->USD];
							$productAction->updateAttributes($productIds, $data, 5);
	             			// createSimpleProduct('us', $name, $sizevalue->sku, $sizevalue->USD, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             		}
	             		else{
	             			$productIds = array($resultId);
							$data = ['price'=>9999, 'status' => 0];
							$productAction->updateAttributes($productIds, $data, 5);
	             			// createSimpleProduct('us', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
	             		}
	             		if(isset($sizevalue->SEK)){
	             			$productIds = array($resultId);
							$data = ['price'=>$sizevalue->SEK];
							$productAction->updateAttributes($productIds, $data, 6);
	             			// createSimpleProduct('se', $name, $sizevalue->sku, $sizevalue->SEK, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             		}
	             		else{
	             			$productIds = array($resultId);
							$data = ['price'=>9999, 'status' => 0];
							$productAction->updateAttributes($productIds, $data, 6);
	             			// createSimpleProduct('se', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);
	             		}
	             		if(isset($sizevalue->GBP)){
	             			$productIds = array($resultId);
							$data = ['price'=>$sizevalue->GBP];
							$productAction->updateAttributes($productIds, $data, 3);
	             			// createSimpleProduct('uk', $name, $sizevalue->sku, $sizevalue->GBP, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 1);
	             		}
	             		else{
	             			$productIds = array($resultId);
							$data = ['price'=>9999, 'status' => 0];
							$productAction->updateAttributes($productIds, $data, 3);
	             			// createSimpleProduct('uk', $name, $sizevalue->sku, 9999, $size, $dateTime, $sizevalue->QTY, $isInStock, $apiUrl, $token, $seasonId, $collectionmemberid, 0);	
	             		}
	             		
	            	}

	                $testArray = array($colorKey);
	                foreach($testArray as $array){
	                   $colorId = $array;
	                   foreach($attributeList as $attrGroup){
	                     	if($attrGroup->attribute_code == 'color'){
	                       		$attributes = $attrGroup->options;
	                       		foreach ($attributes as $key => $val) {
	                         		if($key !== 0){
	                         			// if ($val->label == $colorId) {
	                           			if ($val->label == $colorName) {
	                             			$color = $val->value;
	                           			}
	                         		}
	                       		}
	                     	}
	                   	}
	                }

	                $sizeOption = array();
				    foreach($attributeList as $attrGroup){
				        if($attrGroup->attribute_code == 'size'){
				          	$options = $attrGroup->options;
				          	foreach ($options as $option) {
				            	if($option->value != ''){
				              		array_push($sizeOption, array('value_index' => $option->value));
				            	} 
				          	}
				        }
				    }
				    if(isset($value->description->uk) === true){
				    	$description = $value->description->uk;
				    }
	                $confId = createConfigurableProuct($confSku.'-'.$colorKey, $name, $sizevalue->DKK, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'all', $collectionmemberid, $collectionName);
	                echo $confId .' - Conf created';
	                echo PHP_EOL;
	                $daName = $name;
	                $dadescription = $description;
	                if(isset($value->name->da)){
	                	$daName = $value->name->da;
	                }
	                if(isset($value->description->da)){
	                	$dadescription = $value->description->da;
	                }

	                if(isset($lastPrices->DKK)){
	                	$product = $productFactory->create()->setStoreId(1)->load($confId);
                    $product->setStatus(null); // Use default
                    $product->setTaxClassId(null); // Use default
                    $product->setVisibility(null); // Use default
                    $product->setCountryOfManufacture(null); // Use default
        						$product->setName(null);
        						$product->setDescription($dadescription);
        						$product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $daName, $sizevalue->DKK, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $dadescription, 'da', $collectionmemberid);
	                }
	                else{
	                	$product = $productFactory->create()->setStoreId(1)->load($confId);
        						$product->setStatus(0);
                    $product->setTaxClassId(null); // Use default
                    $product->setVisibility(null); // Use default
                    $product->setCountryOfManufacture(null); // Use default
                    $product->setName(null);
                    $product->setDescription(null);
						        $product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $daName, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $dadescription, 'da', $collectionmemberid, 0);
	                }

	                if(isset($lastPrices->EUR) === false){
	                	$product = $productFactory->create()->setStoreId(2)->load($confId);
						$product->setStatus(0);
						$product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'eu', $collectionmemberid, 0);
	                }
	                if(isset($lastPrices->NOK) === false){
	                	$product = $productFactory->create()->setStoreId(4)->load($confId);
						$product->setStatus(0);
						$product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'no', $collectionmemberid, 0);
	                }
	                if(isset($lastPrices->USD) === false){
	                	$product = $productFactory->create()->setStoreId(5)->load($confId);
						$product->setStatus(0);
						$product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'us', $collectionmemberid, 0);
	                }
	                if(isset($lastPrices->SEK) === false){
	                	$product = $productFactory->create()->setStoreId(6)->load($confId);
						$product->setStatus(0);
						$product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'se', $collectionmemberid, 0);
	                }
	                if(isset($lastPrices->GBP) === false){
	                	$product = $productFactory->create()->setStoreId(3)->load($confId);
						$product->setStatus(0);
						$product->save();
	                	// createConfigurableProuct($confSku.'-'.$colorKey, $name, 9999, $name.'-'.$confSku.'-'.$colorKey, $color, $dateTime, $sizeOption, $simpleProductIds, $apiUrl, $token, $seasonId, $description, 'uk', $collectionmemberid, 0);
	                }


	                $statusReturn['success']['collectionmembers'][$collectionmemberid] = 1;
	                // echo $confSku.'-'.$colorKey .' Has been craeted !!!! ####';
	                // echo PHP_EOL;
	                // echo PHP_EOL;
	                // echo PHP_EOL;

	                // die('1 oprettet');
                }
            }
        }


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$endtime = microtime(true);
		$timediff = $endtime - $starttime;
		// unlink('all.json');

		$newStatus = json_encode($statusReturn);
		// $ch = curl_init("http://b2b.resumecph.dk/resume/sfmodule/sw/api/update/status/season");
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $newStatus);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		// $result = curl_exec($ch);
		// curl_close($ch);
		
		unlink($serverPath.'/seasoncreate.json');
		unlink($serverPath.'/cronRunning.flag');
		$myfile = fopen($serverPath."/cronImport.log", "a");
		fwrite($myfile, 'Ran create/update season at: '. $dateTime . ' it took : ' . $timediff . ' 
				');
	 	fclose($myfile);

	}
	if(file_exists($serverPath.'/deleteseason.json')){
		//$myfile = fopen("/var/www/resumecph.dk/public_html/td_inc/cronRunning.flag", "w");
		//fwrite($myfile, 'Cron kører');
		//fclose($myfile);


		//unlink('/var/www/resumecph.dk/public_html/td_inc/cronRunning.flag');
	}

}



?>