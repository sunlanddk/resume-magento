<?php 
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);


	$ch = curl_init("http://b2b.resumecph.dk/resume/sfmodule/sw/api/season/list/all");
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
  $result = curl_exec($ch);
  $result = json_decode($result);
  
  $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
  $appState = $objectManager->get("Magento\Framework\App\State");
	$appState->setAreaCode('frontend');
  $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');

      $updatePriceArray = array();
      //get prices and map them to array with product sku and correct storeview code
      foreach ($result as $conf => $confValue) {
         // $cont = $key
         //$confValue = $value
        $colors = $confValue->colors;
          foreach ($colors as $colorKey => $colorValue) {
            // $colorKey = $key
            //$colorValue = $value
            $sizes = $colorValue->sizes;
            foreach ($sizes as $sizeKey => $sizeValue) {
                // $sizeKey = $key
                //$sizeValue = $value
                $isInStock = 1;
                if($sizeValue->QTY == 0){
                    $isInStock = 0;
                }
                if($sizeValue->QTY < 0){
                  $sizeValue->QTY = 0;
                }
                $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/stock.log", "a");
                fwrite($myfile, date("d-m-Y h:i:s") .' '. $sizeValue->sku . ': ' . $sizeValue->QTY . ' -  is in stock: '. $isInStock . '
                ');
                fclose($myfile);

                  try {
                    $productObj = $productRepository->get($sizeValue->sku);
                    $productSimpID = $productObj->getId();
                    $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productSimpID);
                    $_product->setStockData(['qty' => $sizeValue->QTY, 'is_in_stock' => $isInStock]);
                    $_product->setQuantityAndStockStatus(['qty' => $sizeValue->QTY, 'is_in_stock' => $isInStock]);
                    $_product->save();
                    // echo $productSimpID.' - '.$sizeValue->QTY;
                    // echo PHP_EOL;
                  } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
                      continue;
                  }
            }
          }
      }