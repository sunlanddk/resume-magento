<?php 
    if(isset($_GET['code'])){
		$code = $_GET['code'];

		$apiData = array(
		  'client_id'       => $insta_client_id,
		  'client_secret'   => $insta_client_secret,
		  'grant_type'      => 'authorization_code',
		  'redirect_uri'    => $insta_redirect_uri,
		  'code'            => $code
		);

		$apiHost = 'https://api.instagram.com/oauth/access_token';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apiHost);
		curl_setopt($ch, CURLOPT_POST, count($apiData));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($apiData));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$jsonData = curl_exec($ch);
		curl_close($ch);

		
		$user = @json_decode($jsonData); 
		
		echo $access_token = $user->access_token; //this is your access token
	}
?>