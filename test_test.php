<?php 
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$appState = $objectManager->get("Magento\Framework\App\State");
$appState->setAreaCode('frontend');

die();
// $stockinterface = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
// // $_product = $objectManager->create('Magento\Catalog\Model\Product')->load(2775);
// // $_product->setStockData(['qty' => 9, 'is_in_stock' => true]);
// // $_product->setQuantityAndStockStatus(['qty' => 9, 'is_in_stock' => true]);
// // $_product->save();
// echo (int)$stockinterface->getStockQty(2775);
// echo PHP_EOL;
// echo (int)$stockinterface->getStockQty(2781);
// echo PHP_EOL;

// die();
$ch = curl_init("http://b2b.resumecph.dk/resume/sfmodule/sw/api/season/list/all");
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
          $result = curl_exec($ch);
          $result = json_decode($result);
          
          $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
          $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
          $stockinterface = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');

          $updatePriceArray = array();
          //get prices and map them to array with product sku and correct storeview code
          foreach ($result as $conf => $confValue) {
             // $cont = $key
             //$confValue = $value
            $colors = $confValue->colors;
              foreach ($colors as $colorKey => $colorValue) {
                // $colorKey = $key
                //$colorValue = $value
                $sizes = $colorValue->sizes;
                foreach ($sizes as $sizeKey => $sizeValue) {
                    // $sizeKey = $key
                    //$sizeValue = $value
                    $isInStock = 1;
                    if($sizeValue->QTY == 0){
                        $isInStock = 0;
                    }
                    if($sizeValue->QTY < 0){
                      $sizeValue->QTY = 0;
                    }
                    
                    // \Magento\Framework\Exception\NoSuchEntityException
                      try {
                        $productObj = $productRepository->get($sizeValue->sku);
                        $productSimpID = $productObj->getId();
                        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productSimpID);
                        if((int)$stockinterface->getStockQty($_product->getId()) != (int)$sizeValue->QTY){
                          $_product->setStockData(['qty' => $sizeValue->QTY, 'is_in_stock' => $isInStock]);
                          $_product->setQuantityAndStockStatus(['qty' => $sizeValue->QTY, 'is_in_stock' => $isInStock]);
                          $_product->save();
                          echo $productSimpID.' - '.$sizeValue->QTY;
                          echo PHP_EOL;
                          $myfile = fopen("/var/www/resumecph.dk/public_html/var/log/stock.log", "a");
                    fwrite($myfile, date("d-m-Y H:i:s") .' '. $sizeValue->sku . ': ' . $sizeValue->QTY . ' -  is in stock: '. $isInStock . '
');
                      fclose($myfile);
                        }
                      } catch (Throwable $e){
                          	continue;
                      }
                }
              }
          }


          $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
          $stockRepo = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');

          $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
          $collection = $productCollection->setFlag('has_stock_status_filter', true)->load();

          foreach ($collection as $_product) {
            try {
              $product = $objectManager->create('Magento\Catalog\Model\Product')->load($_product->getId());  
            } catch (Exception $e) {
              print_r($e);
              continue;
            }
            
            if($product->getTypeId() == 'configurable' ){
              $isInStock = $stockRepo->get($product->getId())->getIsInStock();
              $_children = $product->getTypeInstance()->getUsedProducts($product);
              $stock = 0;
              foreach ($_children as $child){
                //echo "Here are your child Product Ids ".$child->getID() . " | " . $StockState->getStockQty($child->getId(), $child->getStore()->getWebsiteId());;
                $stock += $StockState->getStockQty($child->getId(), $child->getStore()->getWebsiteId());
              }
              if ($stock == 0 && $isInStock == true){
                try {
                  $_product->setUrlKey($product->getUrlKey());
                  $_product->setStockData(['qty' => 0, 'is_in_stock' => false]);
                  $_product->save();
                } catch (Exception $e) {
                  // echo $e;
                  //echo "set out of stock " . $product->getUrlKey() . " | " . $product->getName() . " | " . $product->getSku() . " | " . $isInStock;
                  //echo PHP_EOL;  
                }
              }
              if($stock > 0 && $isInStock == false){
                try {
                  $_product->setUrlKey($product->getUrlKey());
                  $_product->setStockData(['qty' => 0, 'is_in_stock' => true]);
                  $_product->save();
                } catch (Exception $e) {
                  // echo $e;
                  //echo "set out of stock " . $product->getUrlKey() . " | " . $product->getName() . " | " . $product->getSku() . " | " . $isInStock;
                  //echo PHP_EOL;  
                }
              }
            }
          }

die();
